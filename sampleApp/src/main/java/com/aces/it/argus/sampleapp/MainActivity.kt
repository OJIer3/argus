package com.aces.it.argus.sampleapp

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.net.toFile
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aces.it.argus.Argus
import com.aces.it.argus.model.Transformations
import com.aces.it.argus.sampleapp.databinding.ActMainBinding
import com.aces.it.argus.sampleapp.databinding.LiImageBinding
import com.bumptech.glide.Glide
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : ComponentActivity() {

    private var imagesAdapter = ImageAdapter()

    private lateinit var argusCamera: ActivityResultLauncher<Intent>
    private lateinit var argusQR: ActivityResultLauncher<Intent>
    private lateinit var argusBarcode: ActivityResultLauncher<Intent>
    private lateinit var argusDraw: ActivityResultLauncher<Intent>
    private lateinit var argusCrop: ActivityResultLauncher<Intent>

    private lateinit var binding: ActMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.act_main)

        if (savedInstanceState == null)
            Argus.initializeLog(this.application, true)

        argusCamera = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            result.data?.let {
                //Argus.parseImageIntent(it)
                fillAdapter()
            }
        }

        argusQR = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            result.data?.let {
                Argus.parseQRIntent(it)?.let{ message ->
                    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
                }
            }
        }

        argusBarcode = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            result.data?.let {
                Argus.parseBarcodeIntent(it)?.let{ message ->
                    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
                }
            }
        }

        argusDraw = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            result.data?.let {
                //Argus.parseImageIntent(it)
                fillAdapter()
            }
        }

        argusCrop = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            result.data?.let {
                //Argus.parseImageIntent(it)
                fillAdapter()
            }
        }

        checkPermissions()
    }

    private fun checkPermissions() {
        val filePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        if (filePermission != PackageManager.PERMISSION_GRANTED
            || cameraPermission != PackageManager.PERMISSION_GRANTED)
        {
            val contract = ActivityResultContracts.RequestMultiplePermissions()
            val callback = ActivityResultCallback<Map<String, Boolean>> {
                if ( it.values.contains(false))
                    finish()
                else
                    init()
            }

            registerForActivityResult(contract, callback)
                .launch(
                    arrayOf(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA
                    )
                )
        } else {
            init()
        }
    }

    private fun init() {
        binding.btnAdd.setOnClickListener {
            FilesUtils.getArgusDir(this)?.toUri()?.let { uri ->
                argusCamera.launch(Argus.getFromCameraIntent(this, uri, false, false))
                //argusCamera.launch(Argus.getTakePhotoIntent(this, uri, false, false))
            }
        }

        binding.btnAdd.setOnLongClickListener {
            FilesUtils.getArgusDir(this)?.toUri()?.let { uri ->
                argusCamera.launch(Argus.getFromCameraIntent(this, uri, true, true, 1))
            }
            true
        }

        binding.btnScan.setOnClickListener {
            //argusQR.launch(Argus.scanQRIntent(this))
            argusBarcode.launch(Argus.scanBarcodeIntent(this))
        }

        binding.recycler.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = imagesAdapter

            val itemTouchHelper = ItemTouchHelper(SwipeToDeleteCallback())
            itemTouchHelper.attachToRecyclerView(this)

            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    val offset = binding.recycler.computeVerticalScrollOffset()
                    if (offset == 0) {
                        binding.btnScan.show()
                        binding.btnAdd.extend()
                    } else {
                        binding.btnScan.hide()
                        binding.btnAdd.shrink()
                    }
                }
            })
        }

        fillAdapter()
    }

    private fun fillAdapter() {
        imagesAdapter.setItems(
            FilesUtils.getArgusDir(this)
                ?.listFiles { file -> file.name.endsWith(".png") || file.name.endsWith(".jpg") }
                ?.map { file -> file.toUri() }
        )
        if (imagesAdapter.isEmpty()) {
            binding.recycler.visibility = View.GONE
            binding.empty.visibility = View.VISIBLE
        } else {
            binding.recycler.visibility = View.VISIBLE
            binding.empty.visibility = View.GONE
        }
    }

    inner class ImageAdapter: RecyclerView.Adapter<ImageAdapter.Holder>() {

        private val items = mutableListOf<Uri>()

        fun deleteItem(pos: Int) {
            items.elementAtOrNull(pos)?.let { uri ->
                try {
                    val file = uri.toFile()
                    if (file.delete()) {
                        items.removeAt(pos)
                        notifyItemRemoved(pos)
                    }
                } catch (e: Exception) {

                }
            }
        }

        @SuppressLint("NotifyDataSetChanged")
        fun setItems(uris: Collection<Uri>?) {
            items.clear()
            if (!uris.isNullOrEmpty())
                items.addAll(uris)
            notifyDataSetChanged()
        }

        fun isEmpty() = items.isEmpty()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            return Holder (
                LiImageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            )
        }

        override fun onBindViewHolder(holder: Holder, position: Int) {
            holder.bind(items.elementAtOrNull(position))
        }

        override fun getItemCount(): Int {
            return items.size
        }

        inner class Holder(private val binding: LiImageBinding): RecyclerView.ViewHolder(binding.root) {

            init {
                itemView.setOnClickListener {
                    uri?.let {
                        val uris = ArrayList<Uri>()
                        uris.add(it)
                        argusDraw.launch(
                            Argus.drawIntent (
                                this@MainActivity,
                                Transformations(it),
                                FilesUtils.getArgusDir(this@MainActivity)?.toUri(),
                                null,
                                false
                            )


                            /*Argus.getEditPhotoIntent(
                                this@MainActivity,
                                uris,
                                FilesUtils.getArgusDir(this@MainActivity)?.toUri(),
                                false,
                                false
                            )*/
                        )
                    }

                }

                itemView.setOnLongClickListener {
                    uri?.let {
                        argusCrop.launch(
                            Argus.cropIntent (
                                this@MainActivity,
                                Transformations(it),
                                FilesUtils.getArgusDir(this@MainActivity)?.toUri(),
                                null
                            )
                        )
                    }
                    true
                }
            }

            private var uri: Uri? = null

            fun bind(uri: Uri?) {
                this.uri = uri
                Glide.with(itemView).load(uri).into(binding.image)
            }
        }
    }

    inner class SwipeToDeleteCallback: ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            return true
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            imagesAdapter.deleteItem(viewHolder.adapterPosition)
        }
    }
}
