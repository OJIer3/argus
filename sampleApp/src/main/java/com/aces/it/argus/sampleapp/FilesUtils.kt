package com.aces.it.argus.sampleapp

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by O.Zakharov on 11.02.2020
 */
@Suppress("unused", "MemberVisibilityCanBePrivate")
class FilesUtils {

    companion object {

        private const val TAG = "FilesUtils"
        private val fileNameFormat = SimpleDateFormat("yyyy-MM-dd_HH-mm-ss", Locale.ENGLISH)

        fun getArgusDir(context: Context): File? {
            try {
                val parent: File =
                    if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                        context.externalCacheDir!!
                    else
                        context.cacheDir!!

                val imagePath = File(parent, "argus")
                if (!imagePath.exists())
                    if (!imagePath.mkdirs())
                        return null
                return imagePath
            } catch (e: Exception) {
                return null
            }
        }

    }
}