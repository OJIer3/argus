package com.aces.it.argus;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.LocaleList;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.aces.it.argus.fragment.BarcodeFragment;
import com.aces.it.argus.fragment.BaseFragment;
import com.aces.it.argus.fragment.DocumentCropFragment;
import com.aces.it.argus.fragment.DocumentDrawFragment;
import com.aces.it.argus.fragment.DocumentScanFragment;
import com.aces.it.argus.fragment.EditPhotoFragment;
import com.aces.it.argus.fragment.GalleryFragment;
import com.aces.it.argus.fragment.QrFragment;
import com.aces.it.argus.fragment.TakePhotoFragment;
import com.aces.it.argus.model.Transformations;

import java.util.ArrayList;
import java.util.Locale;


public class ArgusActivity extends AppCompatActivity implements Callback {

    private static final int REQUEST_CAMERA_PERMISSION = 20001;
    private static final int REQUEST_WRITE_EXTERNAL_PERMISSION = 20002;

    static {
        System.loadLibrary("opencv_java4");
    }

    private WindowInsets insets;
    private boolean showTutorial = false;
    private boolean showDrawTutorial = false;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(updateResources(newBase));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.argus_activity);
        findViewById(R.id.fragment).setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        );

        if (getCurrentFragment() == null) {
            int type = Argus.TYPE_CAMERA;
            Intent intent = getIntent();
            int maxPhotos = 0;
            if (intent != null) {
                type = intent.getIntExtra(Argus.TYPE, type);
                showTutorial = intent.getBooleanExtra(Argus.SHOW_TUTORIAL, false);
                showDrawTutorial = intent.getBooleanExtra(Argus.SHOW_DRAW_TUTORIAL, false);
                maxPhotos = intent.getIntExtra(Argus.MAX_PHOTOS, 0);
            }

            switch (type) {

                case Argus.TYPE_CAMERA:
                    replaceFragment(DocumentScanFragment.newInstance(null, maxPhotos), false);
                    break;

                case Argus.TYPE_GALLERY: {
                    ArrayList<Uri> uris = intent.getParcelableArrayListExtra(Argus.SOURCE_URI);
                    ArrayList<Transformations> transformations = new ArrayList<>();
                    if (uris != null) {
                        for (Uri uri : uris) {
                            transformations.add(new Transformations(uri));
                        }
                    }
                    replaceFragment(
                            GalleryFragment.newInstance(
                                    transformations,
                                    0,
                                    false,
                                    0
                            ),
                            false
                    );
                    break;
                }

                case Argus.TYPE_DRAW: {
                    Transformations transformations =
                            (Transformations) getIntent().getSerializableExtra(Argus.TRANSFORMATIONS);
                    replaceFragment(
                            DocumentDrawFragment.newInstance(transformations),
                            false
                    );
                    break;
                }

                case Argus.TYPE_CROP: {
                    Transformations transformations =
                            (Transformations) getIntent().getSerializableExtra(Argus.TRANSFORMATIONS);
                    replaceFragment(
                            DocumentCropFragment.newInstance(transformations),
                            false
                    );
                    break;
                }

                case Argus.TYPE_QR:
                    replaceFragment(QrFragment.newInstance(), false);
                    break;

                case Argus.TYPE_BARCODE:
                    replaceFragment(BarcodeFragment.newInstance(), false);
                    break;

                case Argus.TYPE_TAKE_PHOTO:
                    replaceFragment(TakePhotoFragment.newInstance(null), false);
                    break;

                case Argus.TYPE_EDIT_PHOTO: {
                    ArrayList<Uri> uris = intent.getParcelableArrayListExtra(Argus.SOURCE_URI);
                    if (uris != null && !uris.isEmpty()) {
                        Transformations transformations = new Transformations(uris.get(0));
                        replaceFragment(EditPhotoFragment.newInstance(transformations, false), false);
                    } else {
                        finish();
                    }
                    break;
                }
            }
        }

        if (savedInstanceState != null) {
            showTutorial = savedInstanceState.getBoolean(Argus.SHOW_TUTORIAL, false);
            showDrawTutorial = savedInstanceState.getBoolean(Argus.SHOW_DRAW_TUTORIAL, false);
        }

        findViewById(R.id.fragment).setOnApplyWindowInsetsListener((view, insets) -> {
            ViewGroup viewGroup = (ViewGroup)view;
            int childCount = viewGroup.getChildCount();
            boolean consumed = false;
            for (int i = 0; i< childCount; i++) {
                View child = viewGroup.getChildAt(i);
                if (child.dispatchApplyWindowInsets(insets).isConsumed())
                    consumed = true;
            }

            this.insets = insets;

            if (consumed)
                return insets.consumeSystemWindowInsets();
            else
                return insets;
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(Argus.SHOW_TUTORIAL, showTutorial);
        outState.putBoolean(Argus.SHOW_DRAW_TUTORIAL, showDrawTutorial);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!hasCameraPermission()) {
            ActivityCompat.requestPermissions(this,
                    new String[] { Manifest.permission.CAMERA },
                    REQUEST_CAMERA_PERMISSION);
        } else if (!hasWriteExternalPermission()) {
            ActivityCompat.requestPermissions(this,
                    new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE },
                    REQUEST_WRITE_EXTERNAL_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Fragment fragment = getCurrentFragment();
                if (fragment instanceof BaseFragment)
                    ((BaseFragment) fragment).onCameraPermissionGranted();

                if (!hasWriteExternalPermission()) {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_WRITE_EXTERNAL_PERMISSION);
                }
            } else
                finish();
        } else if (requestCode == REQUEST_WRITE_EXTERNAL_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED)
                finish();
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getCurrentFragment();
        if (fragment instanceof BaseFragment) {
            if (((BaseFragment) fragment).onBackPressedHandled())
                return;
        }
        setResult(
                Activity.RESULT_CANCELED,
                new Intent()
                        .putExtra(Argus.SHOW_TUTORIAL, showTutorial)
                        .putExtra(Argus.SHOW_DRAW_TUTORIAL, showDrawTutorial)
        );
        super.onBackPressed();
    }

    private boolean hasCameraPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean hasWriteExternalPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    public void replaceFragment (Fragment fragment, boolean addToBackStack) {
        final FragmentManager manager = getSupportFragmentManager();
        if (!addToBackStack)
            manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        final String tag = fragment.getClass().getCanonicalName();
        final FragmentTransaction transaction = manager.beginTransaction();
        if (addToBackStack)
            transaction.addToBackStack(tag);
        transaction.replace(R.id.fragment, fragment, tag);
        transaction.commitAllowingStateLoss();
    }


    public Fragment getCurrentFragment () {
        return getSupportFragmentManager().findFragmentById(R.id.fragment);
    }

    @Override
    public void onQrRead(String value) {
        setResult(Activity.RESULT_OK, new Intent().putExtra(Argus.QR, value));
        finish();
    }

    @Override
    public void onBarcodeRead(String value) {
        setResult(Activity.RESULT_OK, new Intent().putExtra(Argus.BARCODE, value));
        finish();
    }

    @Override
    public WindowInsets getInsets() {
        return insets;
    }

    @Override
    public boolean isShowTutorial() {
        return showTutorial;
    }

    @Override
    public void setShowTutorialNextTime(boolean show) {
        showTutorial = show;
    }

    @Override
    public boolean isShowDrawTutorial() {
        return showDrawTutorial;
    }

    @Override
    public void setShowDrawTutorialNextTime(boolean show) {
        showDrawTutorial = show;
    }

    @Override
    public void returnResult(ArrayList<Uri> images) {
        Intent intent = new Intent();

        if (getIntent() != null) {
            Bundle args = getIntent().getExtras();
            if (args != null)
                intent.putExtras(args);
        }

        intent
            .putExtra(Argus.SAVED_URIS, images)
            .putExtra(Argus.SHOW_TUTORIAL, showTutorial)
            .putExtra(Argus.SHOW_DRAW_TUTORIAL, showDrawTutorial);

        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void returnResultTransformations(Transformations transformations) {
        Intent intent = new Intent();

        if (getIntent() != null) {
            Bundle args = getIntent().getExtras();
            if (args != null)
                intent.putExtras(args);
        }

        intent
                .putExtra(Argus.TRANSFORMATIONS, transformations)
                .putExtra(Argus.SHOW_TUTORIAL, showTutorial)
                .putExtra(Argus.SHOW_DRAW_TUTORIAL, showDrawTutorial);

        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public Uri getSaveUri() {
        if (getIntent() != null)
            return getIntent().getParcelableExtra(Argus.SAVE_DIR_URI);
        else
            return null;
    }

    private Context updateResources(Context context) {
        Locale locale = Argus.locale;
        if (locale != null) {
            Locale.setDefault(locale);
            Resources res = context.getResources();
            Configuration config = new Configuration(res.getConfiguration());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                config.setLocale(locale);
                LocaleList localeList = new LocaleList(locale);
                LocaleList.setDefault(localeList);
                config.setLocales(localeList);
                return context.createConfigurationContext(config);
            } else {
                config.setLocale(locale);
                return context.createConfigurationContext(config);
            }
        }
        return context;
    }
}

