package com.aces.it.argus.processor;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;

import androidx.camera.core.ImageProxy;

import com.aces.it.argus.Log;
import com.aces.it.argus.model.Correction;
import com.aces.it.argus.model.Quad;
import com.aces.it.argus.model.Transformations;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.CLAHE;
import org.opencv.imgproc.Imgproc;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by O.Zakharov on 03.09.2019
 */
public class ImageProcessor {

    public static boolean DEBUG = false;
    public static boolean SHOW_SEEKBARS = false;

    public static int PARAMETER1 = 0;
    final public static int PARAMETER1MAX = 1;
    public static int PARAMETER2 = 1;
    final public static int PARAMETER2MAX = 10;
    public static int PARAMETER3 = 85;
    final public static int PARAMETER3MAX = 500;

    private final static float MAX_SIDE_TO_DETECT = 1000f;

    public static Bitmap applyTransformations(Bitmap src, Transformations transformations) {
        int width = src.getWidth();
        int height = src.getHeight();

        Bitmap result = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(result);

        if (transformations.isGrayScale()) {
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setFilterBitmap(true);
            paint.setDither(true);

            ColorMatrix cm = new ColorMatrix();
            cm.setSaturation(0);
            ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
            paint.setColorFilter(f);
            canvas.drawBitmap(src, 0, 0, paint);
        } else {
            canvas.drawBitmap(src, 0, 0, null);
        }

        if (!transformations.getCorrections().isEmpty()) {
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setStyle(Paint.Style.FILL);

            ColorMatrix grayColor = new ColorMatrix();
            grayColor.setSaturation(0);
            ColorFilter grayColorFilter = new ColorMatrixColorFilter(grayColor);

            ColorMatrix normalColor = new ColorMatrix();
            normalColor.setSaturation(1);
            ColorFilter normalColorFilter = new ColorMatrixColorFilter(normalColor);
            for (Correction correction: transformations.getCorrections()) {
                paint.setColor(correction.color);
                paint.setColorFilter(correction.isGrayScale ? grayColorFilter : normalColorFilter);
                canvas.drawRect(correction.startX, correction.startY, correction.endX, correction.endY, paint);
            }
        }

        int orientation = transformations.getOrientation();
        int rotation = orientation == SubsamplingScaleImageView.ORIENTATION_90 ? 90 :
                        orientation == SubsamplingScaleImageView.ORIENTATION_180 ? 180 :
                        orientation == SubsamplingScaleImageView.ORIENTATION_270 ? 270 : 0;
        if (rotation != 0) {
            Bitmap temp = getRotatedBitmap(result, rotation);
            result.recycle();
            result = temp;
        }

        return result;
    }

    public static Bitmap autoCropAndRotate (ImageProxy image) {
        Bitmap bitmap = YUV420toBitmap (image);
        if (image.getImageInfo().getRotationDegrees() != 0) {
            Bitmap temp = ImageProcessor.getRotatedBitmap(bitmap, image.getImageInfo().getRotationDegrees());
            bitmap.recycle();
            bitmap = temp;
        }

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        float ratio = Math.min(1f, Math.min (MAX_SIDE_TO_DETECT / width, MAX_SIDE_TO_DETECT / height));
        Bitmap scaledBitmap = ImageProcessor.getScaledBitmap(
                bitmap,
                (int) (width * ratio),
                (int) (height * ratio));
        Quad quad = detectLargestQuad(scaledBitmap);
        scaledBitmap.recycle();

        if (Quad.isValid(quad)) {
            List<Point> points = new ArrayList<>();
            for (Point point : quad.getPoints()) {
                points.add(new Point(point.x / (ratio * quad.getDetectionScale()), point.y / (ratio * quad.getDetectionScale())));
            }

            Bitmap temp = getCroppedBitmap(bitmap, points);
            bitmap.recycle();
            bitmap = temp;
        }

        return bitmap;
    }

    public static Bitmap YUV420toBitmap (ImageProxy image) {

        if (image.getFormat() == ImageFormat.JPEG) {
            ByteBuffer buffer = image.getPlanes()[0].getBuffer();
            buffer.rewind();
            byte[] bytes = new byte[buffer.capacity()];
            buffer.get(bytes);
            return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        }

        /*Rect crop = image.getCropRect();
        int width = crop.width();
        int height = crop.height();
        YuvImage yuvImage = new YuvImage(YUV420toNV21(image), ImageFormat.NV21, width, height, null);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        yuvImage.compressToJpeg(new Rect(0, 0, yuvImage.getWidth(), yuvImage.getHeight()), 100, out);

        byte[] imageBytes = out.toByteArray();
        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);*/

        Rect crop = image.getCropRect();
        int width = crop.width();
        int height = crop.height();

        Mat yuvMat = new Mat(height + height / 2, width, CvType.CV_8UC1);
        yuvMat.put(0, 0, YUV420toNV21(image));
        Mat rgbMat = new Mat(image.getHeight(), image.getWidth(),CvType.CV_8UC4);
        Imgproc.cvtColor(yuvMat, rgbMat, Imgproc.COLOR_YUV2RGB_NV21, 0);
        yuvMat.release();

        final Bitmap bitmap = Bitmap.createBitmap(rgbMat.cols(), rgbMat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(rgbMat, bitmap);
        rgbMat.release();

        return bitmap;
    }

    public static byte[] YUV420toNV21 (ImageProxy image) {
        Rect crop = image.getCropRect();
        int format = image.getFormat();
        int width = crop.width();
        int height = crop.height();
        ImageProxy.PlaneProxy[] planes = image.getPlanes();
        byte[] data = new byte[width * height * ImageFormat.getBitsPerPixel(format) / 8];
        byte[] rowData = new byte[planes[0].getRowStride()];

        int channelOffset = 0;
        int outputStride = 1;
        for (int i = 0; i < planes.length; i++) {
            switch (i) {
                case 0:
                    channelOffset = 0;
                    outputStride = 1;
                    break;
                case 1:
                    channelOffset = width * height + 1;
                    outputStride = 2;
                    break;
                case 2:
                    channelOffset = width * height;
                    outputStride = 2;
                    break;
            }

            ByteBuffer buffer = planes[i].getBuffer();
            int rowStride = planes[i].getRowStride();
            int pixelStride = planes[i].getPixelStride();

            int shift = (i == 0) ? 0 : 1;
            int w = width >> shift;
            int h = height >> shift;
            buffer.position(rowStride * (crop.top >> shift) + pixelStride * (crop.left >> shift));
            for (int row = 0; row < h; row++) {
                int length;
                if (pixelStride == 1 && outputStride == 1) {
                    length = w;
                    buffer.get(data, channelOffset, length);
                    channelOffset += length;
                } else {
                    length = (w - 1) * pixelStride + 1;
                    buffer.get(rowData, 0, length);
                    for (int col = 0; col < w; col++) {
                        data[channelOffset] = rowData[col * pixelStride];
                        channelOffset += outputStride;
                    }
                }
                if (row < h - 1) {
                    buffer.position(buffer.position() + rowStride - length);
                }
            }
        }
        return data;
    }

    public static Bitmap getCroppedBitmap(Bitmap bitmap, List<Point> points) {

        Mat picture = bitmapToMat(bitmap);

        Point[] pts = sortPoints(points);
        Point tl = pts[0];
        Point tr = pts[1];
        Point br = pts[2];
        Point bl = pts[3];

        double widthA = Math.sqrt(Math.pow(br.x - bl.x, 2.0) + Math.pow(br.y - bl.y, 2.0));
        double widthB = Math.sqrt(Math.pow(tr.x - tl.x, 2.0) + Math.pow(tr.y - tl.y, 2.0));
        double dw = (widthA + widthB) / 2.0;
        int maxWidth = (int) dw;


        double heightA = Math.sqrt(Math.pow(tr.x - br.x, 2.0) + Math.pow(tr.y - br.y, 2.0));
        double heightB = Math.sqrt(Math.pow(tl.x - bl.x, 2.0) + Math.pow(tl.y - bl.y, 2.0));
        double dh = (heightA + heightB) / 2.0;
        int maxHeight = (int) dh;

        Mat croppedPic = new Mat(maxHeight, maxWidth, CvType.CV_8UC4);

        Mat srcMat = new Mat(4, 1, CvType.CV_32FC2);
        Mat dstMat = new Mat(4, 1, CvType.CV_32FC2);

        srcMat.put(0, 0, tl.x, tl.y, tr.x, tr.y, br.x, br.y, bl.x, bl.y);
        dstMat.put(0, 0, 0.0, 0.0, dw, 0.0, dw, dh, 0.0, dh);

        Mat transform = Imgproc.getPerspectiveTransform(srcMat, dstMat);

        Imgproc.warpPerspective(picture, croppedPic, transform, croppedPic.size());
        picture.release();
        transform.release();
        srcMat.release();
        dstMat.release();
        Bitmap result = matToBitmap(croppedPic);
        croppedPic.release();
        return result;
    }

    public static Bitmap getRotatedBitmap(Bitmap original, int angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(original, 0, 0, original.getWidth(), original.getHeight(), matrix, true);
    }

    public static Bitmap getFlippedBitmap(Bitmap source, boolean xFlip, boolean yFlip) {
        Matrix matrix = new Matrix();
        matrix.postScale(xFlip ? -1 : 1, yFlip ? -1 : 1, source.getWidth() / 2f, source.getHeight() / 2f);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public static Bitmap getScaledBitmap(Bitmap bitmap, int width, int height) {
        Matrix matrix = new Matrix();
        matrix.setRectToRect(
                new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()),
                new RectF(0, 0, width, height),
                Matrix.ScaleToFit.CENTER);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static Bitmap toBlackAndWhiteBitmap(Bitmap bitmap) {
        Mat mat = bitmapToMat(bitmap);
        Mat gray = new Mat(mat.rows(), mat.cols(), CvType.CV_8UC1);
        Imgproc.cvtColor(mat, gray, Imgproc.COLOR_BGR2GRAY, 4);
        mat.release();
        Imgproc.adaptiveThreshold(gray, gray, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 45, 10);
        Bitmap result = matToBitmap(gray);
        gray.release();
        return result;
    }

    public static Bitmap toGrayScaleBitmap(Bitmap bitmap) {
        Mat mat = bitmapToMat(bitmap);
        Mat gray = new Mat(mat.rows(), mat.cols(), CvType.CV_8UC1);
        Imgproc.cvtColor(mat, gray, Imgproc.COLOR_BGR2GRAY, 4);
        mat.release();
        Bitmap result = matToBitmap(gray);
        gray.release();
        return result;
    }

    public static Bitmap adjustBrightnessAndContrast(Bitmap bitmap) {
        Mat mat = bitmapToMat(bitmap);
        Mat res = brightnessAndContrastAuto(mat, 1);
        Bitmap result = matToBitmap(res);
        mat.release();
        res.release();
        return result;
    }

    public static Bitmap applyCLAHE(Bitmap bitmap) {
        Mat src = bitmapToMat(bitmap);
        Mat dst = new Mat();
        Mat channel = new Mat();
        Imgproc.cvtColor(src, dst, Imgproc.COLOR_BGR2Lab);
        src.release();

        Core.extractChannel(dst, channel, 0);

        CLAHE clahe = Imgproc.createCLAHE();
        clahe.setClipLimit(4);
        clahe.apply(channel, channel);

        Core.insertChannel(channel, dst, 0);
        channel.release();

        Imgproc.cvtColor(dst, dst, Imgproc.COLOR_Lab2BGR);

        Bitmap res = matToBitmap(dst);
        dst.release();
        return res;
    }

    public static int getMedian(Mat mat) {
        ArrayList<Mat> listOfMat = new ArrayList<>();
        listOfMat.add(mat);
        MatOfInt channels = new MatOfInt(0);
        Mat mask = new Mat();
        Mat hist = new Mat(256, 1, CvType.CV_8UC1);
        MatOfInt histSize = new MatOfInt(256);
        MatOfFloat ranges = new MatOfFloat(0, 256);

        Imgproc.calcHist(listOfMat, channels, mask, hist, histSize, ranges);

        double t = mat.rows() * mat.cols() / 2.0;
        double total = 0;
        int med = -1;
        for (int row = 0; row < hist.rows(); row++) {
            double val = hist.get(row, 0)[0];
            if ((total <= t) && (total + val >= t)) {
                med = row;
                break;
            }
            total += val;
        }
        return med;
    }

    @SuppressLint("UseSparseArrays")
    public static Map<Integer, PointF> getPolygonPoints(Bitmap bitmap) {
        Map<Integer, PointF> outlinePoints = null;
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        Quad quad = detectLargestQuad(bitmap);
        if (Quad.isValid(quad))
            outlinePoints =  quad.toPolygonPoints();

        if (outlinePoints == null || outlinePoints.size() != 4) {
            outlinePoints = new HashMap<>();
            outlinePoints.put(0, new PointF(0, 0));
            outlinePoints.put(1, new PointF(width, 0));
            outlinePoints.put(2, new PointF(0, height));
            outlinePoints.put(3, new PointF(width, height));
        }

        return outlinePoints;
    }

    public static Bitmap debug (ImageProxy image) {

        Mat gray = getMatForFindContoursDebug(image);

        /*List<MatOfPoint> contours = findContours(gray);
        Imgproc.cvtColor(gray, gray, Imgproc.COLOR_GRAY2RGB, 4);
        Imgproc.drawContours(gray, contours, 0,  new Scalar(0, 255, 0), 4);

        float scale = (float) gray.width() / image.getWidth();
        Quad quad = findLargestQuad(contours, scale);
        if (Quad.isValid(quad)) {
            Scalar color = new Scalar(0, 0, 255);
            Imgproc.line(gray,
                    new Point(quad.getPoints()[0].x, quad.getPoints()[0].y),
                    new Point(quad.getPoints()[1].x, quad.getPoints()[1].y),
                    color,
                    10, Imgproc.LINE_AA,
                    0);
            Imgproc.line(gray,
                    new Point(quad.getPoints()[1].x, quad.getPoints()[1].y),
                    new Point(quad.getPoints()[2].x, quad.getPoints()[2].y),
                    color,
                    10, Imgproc.LINE_AA,
                    0);
            Imgproc.line(gray,
                    new Point(quad.getPoints()[2].x, quad.getPoints()[2].y),
                    new Point(quad.getPoints()[3].x, quad.getPoints()[3].y),
                    color,
                    10, Imgproc.LINE_AA,
                    0);
            Imgproc.line(gray,
                    new Point(quad.getPoints()[3].x, quad.getPoints()[3].y),
                    new Point(quad.getPoints()[0].x, quad.getPoints()[0].y),
                    color,
                    10, Imgproc.LINE_AA,
                    0);
        }*/

        //Imgproc.resize(gray, gray, new Size(gray.cols() / 5.0, gray.rows() / 5.0));
        //Mat linesP = new Mat(); // will hold the results of the detection
        //Imgproc.HoughLinesP(gray, linesP, 1, Math.PI/180, PARAMETER1, PARAMETER2, PARAMETER3); // runs the actual detection
        // Draw the lines
        //for (int x = 0; x < linesP.rows(); x++) {
        //    double[] l = linesP.get(x, 0);
        //    Imgproc.line(gray, new Point(l[0], l[1]), new Point(l[2], l[3]), new Scalar(255, 255, 255), 3, Imgproc.LINE_AA, 0);
        //}
        //linesP.release();

        //Mat lines = new Mat(); // will hold the results of the detection
        //Imgproc.HoughLines(gray, lines, 1, Math.PI/180, PARAMETER3);
        //for (int x = 0; x < lines.rows(); x++) {
        //    double rho = lines.get(x, 0)[0],
        //            theta = lines.get(x, 0)[1];
        //    double a = Math.cos(theta), b = Math.sin(theta);
        //    double x0 = a*rho, y0 = b*rho;
        //    Point pt1 = new Point(Math.round(x0 + 1000*(-b)), Math.round(y0 + 1000*(a)));
        //    Point pt2 = new Point(Math.round(x0 - 1000*(-b)), Math.round(y0 - 1000*(a)));
        //    Imgproc.line(gray, pt1, pt2, new Scalar(255, 255, 255), 3, Imgproc.LINE_AA, 0);
        //}
        //lines.release();

        final Bitmap bitmap = matToBitmap(gray);
        gray.release();
        return bitmap;
    }

    public static Quad detectLargestQuad (ImageProxy image) {
        //long start = System.currentTimeMillis();
        Rect crop = image.getCropRect();
        int width = crop.width();
        int height = crop.height();
        float scale = Math.min(1f, Math.min(MAX_SIDE_TO_DETECT / width, MAX_SIDE_TO_DETECT / height));

        Mat yuvMat = new Mat(height + height / 2, width, CvType.CV_8UC1);
        yuvMat.put(0, 0, YUV420toNV21(image));
        Mat rgbMat = new Mat(image.getHeight(), image.getWidth(),CvType.CV_8UC4);
        Imgproc.cvtColor(yuvMat, rgbMat, Imgproc.COLOR_YUV2RGB_NV21);
        yuvMat.release();

        if (scale < 1f)
            Imgproc.resize(rgbMat, rgbMat, new Size(width * scale, height * scale));
        LinesProcessor.Quadrilateral quadrilateral = getQuadrilateral (rgbMat);
        rgbMat.release();

        Quad quad = null;
        if (quadrilateral != null) {
            quad = new Quad (
                    new MatOfPoint2f( quadrilateral.getContour().toArray() ),
                    sortPoints(Arrays.asList(quadrilateral.getPoints())),
                    scale
            );
        }
        //Log.d("Computation time: " + (System.currentTimeMillis() - start) + "; scale: " + scale );
        return quad;
    }

    private static Quad detectLargestQuad (Bitmap bitmap) {
        Mat mat = bitmapToMat(bitmap);
        int width = mat.width();
        int height = mat.height();
        float scale = Math.min(1f, Math.min(MAX_SIDE_TO_DETECT / width, MAX_SIDE_TO_DETECT / height));


        if (scale < 1f)
            Imgproc.resize(mat, mat, new Size(width * scale, height * scale), 1, 1, Imgproc.INTER_LANCZOS4);
        LinesProcessor.Quadrilateral quadrilateral = getQuadrilateral (mat);
        mat.release();

        Quad quad = null;
        if (quadrilateral != null) {
            quad = new Quad (
                    new MatOfPoint2f( quadrilateral.getContour().toArray() ),
                    sortPoints(Arrays.asList(quadrilateral.getPoints())),
                    scale
            );
        }
        return quad;
    }

    private static LinesProcessor.Quadrilateral getQuadrilateral (Mat rgbMat) {
        double imageWidth = rgbMat.cols();
        double imageHeight = rgbMat.rows();

        Mat hsv = new Mat();
        Imgproc.cvtColor(rgbMat, hsv, Imgproc.COLOR_RGB2HSV);

        ArrayList<Mat> hsvPlanes = new ArrayList<>();
        Core.split(hsv, hsvPlanes);
        rgbMat.release();

        Mat v = hsvPlanes.get(2);
        CLAHE clahe = Imgproc.createCLAHE(0.1, new Size(8, 8));
        clahe.apply(v, v);

        Core.merge(hsvPlanes, hsv);
        Imgproc.cvtColor(hsv, hsv, Imgproc.COLOR_HSV2RGB);
        for (Mat plane: hsvPlanes) {
            plane.release();
        }

        ArrayList<Mat> rgbPlanes = new ArrayList<>();
        Core.split(hsv, rgbPlanes);
        hsv.release();

        Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));

        for (Mat plane: rgbPlanes) {
            Mat adjust = new Mat();
            Imgproc.medianBlur(plane, adjust, 21);
            Imgproc.erode(adjust, adjust, kernel, new Point(-1, -1), 10);
            Core.absdiff(plane, adjust, plane);
            Core.normalize(plane, plane, 0, 255, Core.NORM_MINMAX, CvType.CV_8UC1);
        }

        rgbMat = new Mat();
        Core.merge(rgbPlanes, rgbMat);
        for (Mat plane: rgbPlanes) {
            plane.release();
        }

        Mat gray = new Mat();
        Imgproc.cvtColor(rgbMat, gray, Imgproc.COLOR_RGB2GRAY);
        rgbMat.release();

        Imgproc.erode(gray, gray, kernel, new Point(-1,-1), 2);
        kernel.release();

        double median = Imgproc.threshold(gray, gray, 30, 255, Imgproc.THRESH_BINARY);
        Imgproc.Canny(gray, gray, 0.5 * median, median);

        Mat lines = new Mat();
        Imgproc.HoughLines(gray, lines, 1, 1 * Math.PI/180, 85);
        gray.release();

        LinesProcessor.Quadrilateral quad = LinesProcessor.getQuad(lines, imageWidth, imageHeight);
        lines.release();
        return quad;
    }

    private static Mat getMatForFindContoursDebug(ImageProxy image) {
        long start = System.currentTimeMillis();
        Rect crop = image.getCropRect();
        int width = crop.width();
        int height = crop.height();
        float scale = Math.min(1f, Math.min(MAX_SIDE_TO_DETECT / width, MAX_SIDE_TO_DETECT / height));

        Mat yuvMat = new Mat(height + height / 2, width, CvType.CV_8UC1);
        yuvMat.put(0, 0, YUV420toNV21(image));
        Mat rgbMat = new Mat(image.getHeight(), image.getWidth(),CvType.CV_8UC4);
        Imgproc.cvtColor(yuvMat, rgbMat, Imgproc.COLOR_YUV2RGB_NV21);
        yuvMat.release();

        if (scale < 1f)
            Imgproc.resize(rgbMat, rgbMat, new Size(width * scale, height * scale));

        Mat hsv = new Mat();
        Imgproc.cvtColor(rgbMat, hsv, Imgproc.COLOR_RGB2HSV);

        ArrayList<Mat> hsvPlanes = new ArrayList<>();
        Core.split(hsv, hsvPlanes);
        rgbMat.release();

        Mat v = hsvPlanes.get(2);
        CLAHE clahe = Imgproc.createCLAHE(0.1, new Size(8, 8));
        clahe.apply(v, v);

        Core.merge(hsvPlanes, hsv);
        Imgproc.cvtColor(hsv, hsv, Imgproc.COLOR_HSV2RGB);
        for (Mat plane: hsvPlanes) {
            plane.release();
        }

        ArrayList<Mat> rgbPlanes = new ArrayList<>();
        Core.split(hsv, rgbPlanes);
        hsv.release();

        Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));

        for (Mat plane: rgbPlanes) {
            Mat adjust = new Mat();
            Imgproc.medianBlur(plane, adjust, 21);
            Imgproc.erode(adjust, adjust, kernel, new Point(-1, -1), 10);
            Core.absdiff(plane, adjust, plane);
            Core.normalize(plane, plane, 0, 255, Core.NORM_MINMAX, CvType.CV_8UC1);
        }

        rgbMat = new Mat();
        Core.merge(rgbPlanes, rgbMat);
        for (Mat plane: rgbPlanes) {
            plane.release();
        }

        Mat gray = new Mat();
        Imgproc.cvtColor(rgbMat, gray, Imgproc.COLOR_RGB2GRAY);
        rgbMat.release();

        Imgproc.erode(gray, gray, kernel, new Point(-1,-1), 2);
        kernel.release();

        double median = Imgproc.threshold(gray, gray, 30, 255, Imgproc.THRESH_BINARY);
        Imgproc.Canny(gray, gray, 0.5 * median, median);

        Mat lines = new Mat();
        Imgproc.HoughLines(gray, lines, 1, 1 * Math.PI/180, 85);
        Imgproc.cvtColor(gray, gray, Imgproc.COLOR_GRAY2RGB);

        LinesProcessor.Quadrilateral quad = LinesProcessor.getQuad(lines, image.getWidth(), image.getHeight());
        lines.release();
        if (quad != null)
            Imgproc.drawContours(gray, Collections.singletonList(quad.getContour()), 0,  new Scalar(255, 0, 0), 4);
        Log.d("Calculations time: " + (System.currentTimeMillis() - start));
        return gray;
    }

    public static Bitmap matToBitmap (Mat mat) {
        Bitmap bitmap = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(mat, bitmap);
        return bitmap;
    }

    public static Bitmap getScaledBitmap (Uri uri, int width, int height) {
        try {
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(uri.getPath(), options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize (options, width, height);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(uri.getPath(), options);

        } catch (Exception e) {
            return null;
        }
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {

        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private static Point[] sortPoints (List<Point> src) {
        Point[] result = new Point[4];
        // top-left corner = minimal sum
        result[0] = Collections.min(src, COMPARATOR_BY_COORDINATES_SUM);
        // bottom-right corner = maximal sum
        result[2] = Collections.max(src, COMPARATOR_BY_COORDINATES_SUM);
        // top-right corner = minimal difference
        result[1] = Collections.min(src, COMPARATOR_BY_COORDINATES_DIFF);
        // bottom-left corner = maximal difference
        result[3] = Collections.max(src, COMPARATOR_BY_COORDINATES_DIFF);

        return result;
    }

    private static Mat bitmapToMat (Bitmap bitmap) {
        Mat mat = new Mat(bitmap.getHeight(), bitmap.getWidth(), CvType.CV_8U, new Scalar(4));
        Bitmap bitmap32 = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        Utils.bitmapToMat(bitmap32, mat);
        return mat;
    }

    private static Mat brightnessAndContrastAuto(Mat src, float clipHistPercent) {
        //clipHistPercent cut wings of histogram at given percent typical=>1, 0=>Disabled
        final int histSize = 256;
        float alpha;
        float beta;
        double minGray;
        double maxGray;

        Mat gray = null;
        if (src.type() == CvType.CV_8UC1)
            gray = src;
        else if (src.type() == CvType.CV_8UC3) {
            gray = new Mat(src.rows(), src.cols(), CvType.CV_8UC1);
            Imgproc.cvtColor(src, gray, Imgproc.COLOR_BGR2GRAY);
        } else if (src.type() == CvType.CV_8UC4) {
            gray = new Mat(src.rows(), src.cols(), CvType.CV_8UC1);
            Imgproc.cvtColor(src, gray, Imgproc.COLOR_BGRA2GRAY);
        }

        if (gray == null)
            return src;

        if (clipHistPercent == 0) {
            // keep full available range
            Core.MinMaxLocResult res = Core.minMaxLoc(gray);
            minGray = res.minVal;
            maxGray = res.maxVal;
        } else {
            ArrayList<Mat> listOfMat = new ArrayList<>();
            listOfMat.add(src);
            MatOfInt channels = new MatOfInt(0); //try 1
            Mat mask = new Mat();
            Mat hist = new Mat(histSize, 1, CvType.CV_8UC1);
            MatOfInt hSize = new MatOfInt(histSize);
            MatOfFloat ranges = new MatOfFloat(0, 256);

            Imgproc.calcHist(listOfMat, channels, mask, hist, hSize, ranges, false);
            mask.release();

            // calculate cumulative distribution from the histogram
            float[] accumulator = new float[histSize];
            float[] value = new float[1];
            hist.get(0, 0, value);
            accumulator[0] = value[0];
            for (int i = 1; i < histSize; i++) {
                hist.get(i, 0, value);
                accumulator[i] = accumulator[i - 1] + value[0];
            }
            hist.release();

            // locate points that cuts at required value
            float max = accumulator[histSize - 1];
            clipHistPercent *= (max / 100.0); //make percent as absolute
            clipHistPercent /= 2.0; // left and right wings
            // locate left cut
            minGray = 0;
            while (accumulator[(int) minGray] < clipHistPercent && minGray < histSize)
                minGray++;

            // locate right cut
            maxGray = histSize - 1;
            while (accumulator[(int) maxGray] >= (max - clipHistPercent) && maxGray >= 0)
                maxGray--;
        }

        // current range
        float inputRange = (float) (maxGray - minGray);

        alpha = (histSize - 1) / inputRange;   // alpha expands current range to histsize range
        beta = (float) (- minGray * alpha);             // beta shifts current range so that minGray will go to 0

        // Apply brightness and contrast normalization
        // convertTo operates with saurate_cast
        Mat dst = new Mat();
        src.convertTo(dst, -1, alpha, beta);

        // restore alpha channel from source
        if (dst.type() == CvType.CV_8UC4)
            Core.mixChannels(Collections.singletonList(src), Collections.singletonList(dst), new MatOfInt(3, 3));
        return dst;
    }

    private static final Comparator<Point> COMPARATOR_BY_COORDINATES_SUM =
            (lhs, rhs) -> Double.compare(lhs.y + lhs.x, rhs.y + rhs.x);

    private static final Comparator<Point> COMPARATOR_BY_COORDINATES_DIFF =
            (lhs, rhs) -> Double.compare(lhs.y - lhs.x, rhs.y - rhs.x);
}
