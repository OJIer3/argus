package com.aces.it.argus.model;

import android.net.Uri;

import androidx.annotation.IntDef;

import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by O.Zakharov on 12.11.2020
 */
public class Transformations implements Serializable {

    @IntDef({
            SubsamplingScaleImageView.ORIENTATION_0,
            SubsamplingScaleImageView.ORIENTATION_90,
            SubsamplingScaleImageView.ORIENTATION_180,
            SubsamplingScaleImageView.ORIENTATION_270
    })
    public @interface OrientationDef {}

    public Transformations (Uri uri) {
        setUri(uri);
    }

    @OrientationDef
    private int orientation = SubsamplingScaleImageView.ORIENTATION_0;

    private boolean grayScale = false;

    private final ArrayList<Correction> corrections = new ArrayList<>();

    private transient Uri uri = null;

    private String uriString = null;

    public void setUri(Uri uri) {
        this.uri = uri;
        uriString = uri != null ? uri.toString() : null;
    }

    public Uri getUri () {
        if (uri == null && uriString != null) {
            uri = Uri.parse(uriString);
        }
        return uri;
    }

    public int getOrientation() {
        return orientation;
    }

    public boolean isGrayScale() {
        return grayScale;
    }

    public ArrayList<Correction> getCorrections() {
        return corrections;
    }

    public void setCorrections(Collection<Correction> corrections) {
        this.corrections.clear();
        if (corrections != null && !corrections.isEmpty())
            this.corrections.addAll(corrections);
    }

    public void reset() {
        orientation = SubsamplingScaleImageView.ORIENTATION_0;
        grayScale = false;
        corrections.clear();
    }

    public void rotate() {
        switch (orientation) {
            case SubsamplingScaleImageView.ORIENTATION_0:
                orientation = SubsamplingScaleImageView.ORIENTATION_90;
                break;
            case SubsamplingScaleImageView.ORIENTATION_90:
                orientation = SubsamplingScaleImageView.ORIENTATION_180;
                break;
            case SubsamplingScaleImageView.ORIENTATION_180:
                orientation = SubsamplingScaleImageView.ORIENTATION_270;
                break;
            case SubsamplingScaleImageView.ORIENTATION_270:
                orientation = SubsamplingScaleImageView.ORIENTATION_0;
                break;
        }
    }

    public void toggleBW() {
        grayScale = !grayScale;
        for (Correction correction: corrections) {
            correction.isGrayScale = grayScale;
        }
    }

    public void setBW(boolean bw) {
        grayScale = bw;
        for (Correction correction: corrections) {
            correction.isGrayScale = bw;
        }
    }

    public boolean hasChanges() {
        return grayScale || orientation != SubsamplingScaleImageView.ORIENTATION_0 || !corrections.isEmpty();
    }

    public Transformations copy() {
        Transformations transformations = new Transformations(getUri());
        transformations.grayScale = grayScale;
        transformations.orientation = orientation;
        for (Correction correction: corrections) {
            transformations.corrections.add(correction.copy());
        }
        return transformations;
    }
}
