package com.aces.it.argus.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.display.DisplayManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.camera.core.AspectRatio;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraInfoUnavailableException;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.Preview;
import androidx.camera.core.TorchState;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.Insets;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;

import com.aces.it.argus.Argus;
import com.aces.it.argus.Log;
import com.aces.it.argus.R;
import com.aces.it.argus.model.Transformations;
import com.aces.it.argus.processor.ImageProcessor;
import com.aces.it.argus.utils.FileUtils;
import com.google.common.util.concurrent.ListenableFuture;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by O.Zakharov on 19.02.2020
 */
public class TakePhotoFragment extends BaseFragment {

    public static TakePhotoFragment newInstance(Uri rePhotoUri) {
        Bundle args = new Bundle();
        if (rePhotoUri != null)
            args.putParcelable (Argus.RE_PHOTO_URI, rePhotoUri);
        TakePhotoFragment fragment = new TakePhotoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private PreviewView mPreviewView;
    private ImageView mFlashButton;
    private View mFlipCameraButton;
    private View mShutterButton;
    private View mCloseButton;
    private View mProgress;
    private ConstraintLayout mTopLayout;
    private ConstraintLayout mBottomLayout;

    private Disposable mDisposable;

    private Preview cameraPreview;
    private ImageCapture imageCapture;
    private ExecutorService cameraExecutor;
    private LiveData<Integer> torchState = null;

    private ProcessCameraProvider cameraProvider;
    private int lensFacing = CameraSelector.LENS_FACING_BACK;

    final private DisplayManager.DisplayListener displayListener = new DisplayManager.DisplayListener() {
        @Override
        public void onDisplayAdded(int displayId) { }

        @Override
        public void onDisplayRemoved(int displayId) { }

        @Override
        public void onDisplayChanged(int displayId) {
            if (cameraPreview != null && displayId == mPreviewView.getDisplay().getDisplayId()) {
                int rotation = mPreviewView.getDisplay().getRotation();
                Log.d("Rotation changed: " + rotation);
                if (imageCapture != null)
                    imageCapture.setTargetRotation(rotation);
            }
        }
    };

    private boolean mProgressVisible = false;

    private Uri rePhotoUri = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            rePhotoUri = args.getParcelable(Argus.RE_PHOTO_URI);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.argus_fmt_take_photo, container, false);
        mPreviewView = view.findViewById(R.id.preview_view);
        mCloseButton = view.findViewById(R.id.btn_close);
        mShutterButton = view.findViewById(R.id.btn_take_picture);
        mFlashButton = view.findViewById(R.id.btn_flash);
        mFlipCameraButton  = view.findViewById(R.id.btn_flip);
        mTopLayout = view.findViewById(R.id.top_layout);
        mBottomLayout = view.findViewById(R.id.bottom_layout);
        mProgress = view.findViewById(R.id.progress);

        view.setOnApplyWindowInsetsListener((v, insets) -> {
            handleInsets(insets);
            return insets;
        });

        Activity activity = getActivity();
        if (activity != null ) {
            DisplayManager manager = (DisplayManager) activity.getSystemService(Context.DISPLAY_SERVICE);
            if (manager != null)
                manager.registerDisplayListener(displayListener, null);
        }

        handleInsets(callback.getInsets());

        return view;
    }

    private void handleInsets(WindowInsets windowInsets) {
        if (windowInsets != null) {
            Insets insets = WindowInsetsCompat.toWindowInsetsCompat(windowInsets)
                .getInsets(WindowInsetsCompat.Type.systemBars());

            mTopLayout.setPadding(
                0, Math.max(mTopLayout.getPaddingTop(), insets.top), 0, 0
            );
            mBottomLayout.setPadding(
                0, mBottomLayout.getPaddingTop(),
                0, Math.max(mBottomLayout.getPaddingBottom(), insets.bottom)
            );

            mTopLayout.requestLayout();
            mBottomLayout.requestLayout();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (hasCameraPermission())
            initCamera(view.getContext());

        bindTopLayout();

        bindProgress(mProgressVisible);
    }

    private void initCamera(Context context) {
        final ListenableFuture<ProcessCameraProvider> cameraProviderFuture
                = ProcessCameraProvider.getInstance(context);
        cameraProviderFuture.addListener(() ->
            mPreviewView.post(() -> {
                try {
                    cameraProvider = cameraProviderFuture.get();
                    if (cameraProvider.hasCamera(CameraSelector.DEFAULT_BACK_CAMERA))
                        lensFacing = CameraSelector.LENS_FACING_BACK;
                    bindCamera();
                } catch (InterruptedException | ExecutionException | CameraInfoUnavailableException e ) {
                    Log.e(e);
                }
            }),
            ContextCompat.getMainExecutor(context)
        );
    }

    private void bindCamera() {
        if (cameraProvider == null)
            return;

        cameraProvider.unbindAll();

        if (cameraExecutor == null || cameraExecutor.isShutdown())
            cameraExecutor = Executors.newSingleThreadExecutor();

        initPreview();
        initImageCapture();

        CameraSelector cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(lensFacing)
                .build();

        Camera camera = cameraProvider.bindToLifecycle(
                getViewLifecycleOwner(),
                cameraSelector,
                cameraPreview, imageCapture
        );

        cameraPreview.setSurfaceProvider( mPreviewView.getSurfaceProvider() );

        //bind flash button
        if (camera.getCameraInfo().hasFlashUnit()) {
            torchState = camera.getCameraInfo().getTorchState();
            torchState.removeObservers(TakePhotoFragment.this);
            torchState.observe(
                    getViewLifecycleOwner(),
                    integer -> mFlashButton.setImageResource(TorchState.ON == integer ?
                            R.drawable.ic_argus_flash_on :
                            R.drawable.ic_argus_flash_off
                    )
            );

            mFlashButton.setVisibility(View.VISIBLE);
            mFlashButton.setOnClickListener(v -> {
                if (torchState != null && torchState.getValue() != null)
                    camera.getCameraControl().enableTorch(TorchState.ON != torchState.getValue());
            });
            if (torchState != null && torchState.getValue() != null)
                mFlashButton.setImageResource(TorchState.ON == torchState.getValue() ?
                        R.drawable.ic_argus_flash_on :
                        R.drawable.ic_argus_flash_off);
        } else {
            mFlashButton.setVisibility(View.GONE);
        }

        //bind switch cameras button
        try {
            if (cameraProvider.hasCamera(CameraSelector.DEFAULT_BACK_CAMERA)
                    && cameraProvider.hasCamera(CameraSelector.DEFAULT_FRONT_CAMERA)) {
                mFlipCameraButton.setVisibility(View.VISIBLE);
                mFlipCameraButton.setOnClickListener(v -> {
                    if (lensFacing == CameraSelector.LENS_FACING_BACK)
                        lensFacing = CameraSelector.LENS_FACING_FRONT;
                    else
                        lensFacing = CameraSelector.LENS_FACING_BACK;
                    bindCamera();
                });
            } else {
                mFlipCameraButton.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e(e);
            mFlipCameraButton.setVisibility(View.GONE);
        }
    }

    private void initPreview() {
        cameraPreview = new Preview.Builder()
                .setTargetAspectRatio(AspectRatio.RATIO_16_9)
                .setTargetRotation(Surface.ROTATION_0)
                .build();
    }

    private void initImageCapture() {
        imageCapture = new ImageCapture.Builder()
                .setCaptureMode(ImageCapture.CAPTURE_MODE_MAXIMIZE_QUALITY)
                .setTargetAspectRatio(AspectRatio.RATIO_16_9)
                .setTargetRotation(Surface.ROTATION_0)
                .build();

        mShutterButton.setOnClickListener(v -> {

            //show progress and disable buttons while saving picture
            bindProgress(true);

            cameraProvider.unbind(cameraPreview);

            final Uri saveUri = callback.getSaveUri();

            ImageCapture.Metadata metadata = new ImageCapture.Metadata();
            metadata.setReversedHorizontal(lensFacing == CameraSelector.LENS_FACING_FRONT);

            imageCapture.takePicture(cameraExecutor, new ImageCapture.OnImageCapturedCallback() {
                @Override
                public void onCaptureSuccess(@NonNull ImageProxy image) {

                    mDisposable = Single.fromCallable(() -> {
                        int rotationDegrees = image.getImageInfo().getRotationDegrees();
                        Bitmap bitmap = ImageProcessor.YUV420toBitmap(image);
                        image.close();

                        if (rotationDegrees != 0) {
                            Bitmap temp = ImageProcessor.getRotatedBitmap(bitmap, image.getImageInfo().getRotationDegrees());
                            bitmap.recycle();
                            bitmap = temp;
                        }

                        if (lensFacing == CameraSelector.LENS_FACING_FRONT) {
                            Bitmap flipped = ImageProcessor.getFlippedBitmap(bitmap, true, false);
                            bitmap.recycle();
                            bitmap = flipped;
                        }

                        Uri uri = FileUtils.saveBitmap(bitmap, saveUri);
                        bitmap.recycle();
                        return uri;
                    })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                            res -> {
                                if (rePhotoUri != null)
                                    returnResult(res);
                                else
                                    replaceFragment(
                                            EditPhotoFragment.newInstance(
                                                new Transformations(res),
                                                true
                                            ),
                                            false
                                    );
                            },

                            error -> {
                                Log.e(error);
                                bindProgress(false);
                                bindCamera();
                            }
                        );
                }

                @Override
                public void onError(@NonNull ImageCaptureException exception) {
                    Log.e(exception);
                    bindCamera();
                    bindProgress(false);
                }
            });
        });
    }

    private void bindTopLayout() {
        mCloseButton.setOnClickListener(v -> onBackPressed());
    }

    private void bindProgress(boolean show) {
        if (!mProgress.hasOnClickListeners())
            mProgress.setOnClickListener(v -> {});
        mProgressVisible = show;
        mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void returnResult(Uri uri) {
        FragmentManager manager = getFragmentManager();
        Fragment target = getTargetFragment();
        int requestCode = getTargetRequestCode();

        if (target != null && manager != null) {
            manager.popBackStack();
            target.onActivityResult(
                    requestCode,
                    Activity.RESULT_OK,
                    new Intent()
                            .putExtra(Argus.RE_PHOTO_URI, rePhotoUri)
                            .putExtra(Argus.NEW_PHOTO_URI, uri)
            );
        }
    }

    @Override
    public void onDestroyView() {
        if (cameraExecutor != null)
            cameraExecutor.shutdown();
        if (getActivity() != null) {
            DisplayManager manager = (DisplayManager)getActivity().getSystemService(Context.DISPLAY_SERVICE);
            if (manager != null)
                manager.unregisterDisplayListener(displayListener);
        }
        if (mDisposable != null)
            mDisposable.dispose();
        super.onDestroyView();
    }

    @Override
    public void onCameraPermissionGranted() {
        Activity activity = getActivity();
        if (activity != null)
            initCamera(activity);
    }
}
