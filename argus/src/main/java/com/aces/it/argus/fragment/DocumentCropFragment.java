package com.aces.it.argus.fragment;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.aces.it.argus.Argus;
import com.aces.it.argus.R;
import com.aces.it.argus.model.Transformations;
import com.aces.it.argus.processor.ImageProcessor;
import com.aces.it.argus.utils.FileUtils;
import com.aces.it.argus.view.PolygonView;

import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by O.Zakharov on 19.02.2020
 */
public class DocumentCropFragment extends BaseFragment {

    public static DocumentCropFragment newInstance(Transformations transformations) {
        DocumentCropFragment fragment = new DocumentCropFragment();
        //noinspection ConstantConditions
        fragment.getArguments().putSerializable(Argus.TRANSFORMATIONS, transformations);
        return fragment;
    }

    private PolygonView mPolygonView;
    private TextView mBtnCancel;
    private TextView mBtnDone;
    private View mProgress;
    private Disposable mWorking;

    private View mContent;
    private View mNavBarBg;

    private boolean mProgressVisible = false;
    private Transformations transformations;
    private Transformations newTransformations;
    private Bitmap bitmap;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            transformations = (Transformations) args.getSerializable(Argus.TRANSFORMATIONS);
            if (transformations != null) {
                newTransformations = transformations.copy();
                newTransformations.setBW (false);
            }
        }
        initBitmap();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.argus_fmt_document_crop, container, false);
        mPolygonView = view.findViewById(R.id.polygonView);
        mBtnCancel = view.findViewById(R.id.btn_cancel);
        mBtnDone = view.findViewById(R.id.btn_done);
        mProgress = view.findViewById(R.id.progress);
        mContent = view.findViewById(R.id.content);
        mNavBarBg = view.findViewById(R.id.navBarBg);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContent.setOnApplyWindowInsetsListener((v, insets) -> {
            handleInsets(insets);
            return insets;
        });

        mPolygonView.post(this::initializeCropping);
        bindCancelButton();
        bindContinueButton();
        bindProgress();

        handleInsets(callback.getInsets());
    }

    private void handleInsets(WindowInsets windowInsets) {
        if (windowInsets != null) {
            Insets insets = WindowInsetsCompat.toWindowInsetsCompat(windowInsets)
                .getInsets(WindowInsetsCompat.Type.systemBars());

            mContent.setPadding (
                0, Math.max(mContent.getPaddingTop(), insets.top),
                0, Math.max(mContent.getPaddingBottom(), insets.bottom)
            );

            ViewGroup.LayoutParams params = mNavBarBg.getLayoutParams();
            params.height = Math.max(params.height, insets.bottom);

            mContent.requestLayout();
            mNavBarBg.requestLayout();
        }
    }

    private void bindCancelButton() {
        mBtnCancel.setOnClickListener(v -> {
                Activity activity = getActivity();
                if (activity != null)
                    activity.onBackPressed();
        });
    }

    private void bindProgress() {
        if (mProgress != null) {
            if (!mProgress.hasOnClickListeners())
                mProgress.setOnClickListener(v -> {});
            mProgress.setVisibility(mProgressVisible ? View.VISIBLE : View.GONE);
        }

    }

    private void bindContinueButton() {
        mBtnDone.setOnClickListener(v -> {
            if (mWorking == null || mWorking.isDisposed()) {
                Bitmap scaled = mPolygonView.getBitmap();
                if (scaled == null)
                    return;

                mProgressVisible = true;
                bindProgress();

                float xRatio = (float) bitmap.getWidth() / scaled.getWidth();
                float yRatio = (float) bitmap.getHeight() / scaled.getHeight();

                List<Point> points = new ArrayList<>();
                for (PointF point : mPolygonView.getBitmapPoints().values()) {
                    points.add(new Point(point.x * xRatio, point.y * yRatio));
                }
                final Uri savePath = callback.getSaveUri();

                mWorking = Single.fromCallable(() -> {
                    Bitmap temp = ImageProcessor.getCroppedBitmap(bitmap, points);
                    Uri uri = FileUtils.saveBitmap(temp, savePath);
                    temp.recycle();
                    return uri;
                })

                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                uri -> {
                                    Fragment target = getTargetFragment();
                                    int requestCode = getTargetRequestCode();
                                    newTransformations.setUri(uri);
                                    if (target != null) {
                                        FragmentManager manager = getFragmentManager();
                                        if (manager != null ) {
                                            manager.popBackStack();
                                            target.onActivityResult(
                                                    requestCode,
                                                    Activity.RESULT_OK,
                                                    new Intent()
                                                            .putExtra(Argus.CROP_TRANSFORMATIONS, newTransformations)
                                                            .putExtra(Argus.TRANSFORMATIONS, transformations)
                                            );
                                        } else {
                                            onBackPressed();
                                        }
                                    } else {
                                        callback.returnResultTransformations(newTransformations);
                                        /*if (uri != null) {
                                            ArrayList<Uri> uris = new ArrayList<>();
                                            uris.add(uri);
                                            callback.returnResult(uris);
                                        } else  {
                                            onBackPressed();
                                        }*/
                                    }
                                    mProgressVisible = false;
                                    bindProgress();
                                },

                                error -> {
                                    error.printStackTrace();
                                    if (bitmap == null || bitmap.isRecycled()) {
                                        initBitmap();
                                        mPolygonView.post(this::initializeCropping);
                                    }
                                    mProgressVisible = false;
                                    bindProgress();
                                }
                        );


            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (bitmap != null && !bitmap.isRecycled())
            bitmap.recycle();
    }

    private void initBitmap() {
        if (transformations.getUri() != null && (mWorking == null || mWorking.isDisposed())) {
            Activity activity = getActivity();
            if (activity != null) {
                mProgressVisible = true;
                bindProgress();
                ContentResolver resolver = activity.getContentResolver();

                mWorking = Single.fromCallable(() -> {
                    Bitmap bm = MediaStore.Images.Media.getBitmap(resolver, transformations.getUri());
                    if (newTransformations.hasChanges()) {
                        Bitmap transformed = ImageProcessor.applyTransformations(bm, newTransformations);
                        bm.recycle();
                        bm = transformed;
                    }
                    return bm;
                })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        bm -> {
                            if (bitmap != null && !bitmap.isRecycled())
                                bitmap.recycle();

                            bitmap = bm;
                            newTransformations.reset();
                            newTransformations.setBW(transformations.isGrayScale());

                            initializeCropping();
                            mProgressVisible = false;
                            bindProgress();
                        },

                        error -> {
                            error.printStackTrace();
                            mProgressVisible = false;
                            bindProgress();
                            onBackPressed();
                        }
                    );
            }
        }
    }

    private void initializeCropping() {
        int width = mPolygonView.getWidth();
        int height = mPolygonView.getHeight();
        if (width > 0 && height > 0 && bitmap != null) {
            Bitmap scaledBitmap = ImageProcessor.getScaledBitmap(bitmap,
                    mPolygonView.getWidth(), mPolygonView.getHeight());
            mPolygonView.setBitmap(scaledBitmap);
            mPolygonView.setBitmapPoints(ImageProcessor.getPolygonPoints(scaledBitmap));
            mPolygonView.setGrayscale(transformations.isGrayScale());
        } else if (bitmap != null) {
            mPolygonView.post(this::initializeCropping);
        }
    }

}
