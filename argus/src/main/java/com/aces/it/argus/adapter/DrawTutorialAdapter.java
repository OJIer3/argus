package com.aces.it.argus.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aces.it.argus.Log;
import com.aces.it.argus.R;
import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.button.MaterialButton;

import org.jetbrains.annotations.NotNull;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by O.Zakharov on 05.11.2020
 */
public class DrawTutorialAdapter extends RecyclerView.Adapter<DrawTutorialAdapter.Holder> {

    private Listener listener;
    private final PublishSubject<Integer> restartAnimation = PublishSubject.create();

    @NonNull
    @Override
    public DrawTutorialAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.argus_li_draw_tutorial, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DrawTutorialAdapter.Holder holder, int position) {
        holder.bind(position);
    }

    @Override
    public void onViewRecycled(@NonNull @NotNull Holder holder) {
        super.onViewRecycled(holder);
        holder.dispose();
    }

    @Override
    public int getItemCount() {
        return 3;
    }


    public int measureHeight(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.argus_li_draw_tutorial, parent, false);
        DrawTutorialAdapter.Holder holder = new Holder(view);

        int wSpec = View.MeasureSpec.makeMeasureSpec(parent.getWidth(), View.MeasureSpec.EXACTLY);
        int hSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        int maxHeight = 0;

        for (int i = 0; i < getItemCount(); i++) {
            holder.bind(i);
            holder.itemView.measure(wSpec, hSpec);
            maxHeight = Math.max(maxHeight, holder.itemView.getMeasuredHeight());
        }

        return maxHeight;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void restartAnimation(Integer page) {
        if (page != null)
            restartAnimation.onNext(page);
        else
            restartAnimation.onComplete();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private final LottieAnimationView imageView;
        private final ImageView handView;
        private final TextView textView;
        private final MaterialButton button;
        private Disposable disposable = null;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);
            handView = itemView.findViewById(R.id.handImage);
            textView = itemView.findViewById(R.id.hintText);
            button = itemView.findViewById(R.id.btnNext);
            button.setOnClickListener(v -> {
                if (listener != null)
                    listener.onNextButtonClick();
            });

        }

        public void bind (int position) {
            if (position == 0) {
                imageView.setAnimation(R.raw.argus_anim_draw_tutorial_fill);
                handView.setImageResource(R.drawable.ic_argus_hands_select);
                textView.setText(R.string.argus_hint_drawing_1);
                button.setText(R.string.argus_btn_next);
            } else if (position == 1) {
                imageView.setAnimation(R.raw.argus_anim_draw_tutorial_zoom);
                handView.setImageResource(R.drawable.ic_argus_hands_zoom);
                textView.setText(R.string.argus_hint_drawing_2);
                button.setText(R.string.argus_btn_next);
            } else {
                imageView.setAnimation(R.raw.argus_anim_draw_tutorial_move);
                handView.setImageResource(R.drawable.ic_argus_hands_dclick);
                textView.setText(R.string.argus_hint_drawing_3);
                button.setText(R.string.argus_btn_good);
            }

            dispose();
            disposable = restartAnimation
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            integer -> {
                                if (integer == position)
                                    imageView.setProgress(0f);
                            },
                            Log::e
                    );
        }

        public void dispose() {
            if (disposable != null && !disposable.isDisposed())
                disposable.dispose();
        }
    }

    public interface Listener {
        void onNextButtonClick();
    }
}
