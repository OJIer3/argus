package com.aces.it.argus.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.aces.it.argus.R;

/**
 * Created by O.Zakharov on 04.09.2019
 */
public class DragDotView extends View {

    private final Paint mPaint;
    private final int mSize;
    private final Path mCenterPath;
    private final Path mBorderPath;
    private final int mStrokeWidth;

    private final int mDotColor;
    private final int mStrokeColor;

    @SuppressWarnings("SuspiciousNameCombination")
    public DragDotView(Context context) {
        super(context);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mSize = 2 * context.getResources().getDimensionPixelSize(R.dimen.argus_drag_dot_r);
        mStrokeWidth = context.getResources().getDimensionPixelSize(R.dimen.argus_drag_dot_border_width);

        mDotColor = ContextCompat.getColor(context, R.color.argusColorDragDot);
        mStrokeColor = ContextCompat.getColor(context, R.color.argusColorDragDotStroke);

        mCenterPath = new Path();
        mCenterPath.addOval(new RectF(mStrokeWidth, mStrokeWidth, mSize - mStrokeWidth, mSize - mStrokeWidth), Path.Direction.CW);

        mBorderPath = new Path();
        mBorderPath.addOval(new RectF(mStrokeWidth / 2f + 1, mStrokeWidth / 2f + 1, mSize - mStrokeWidth / 2f - 1 , mSize - mStrokeWidth / 2f - 1), Path.Direction.CW);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(mSize, mSize);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mPaint.setColor(mDotColor);
        mPaint.setStyle(Paint.Style.FILL);
        canvas.drawPath(mCenterPath, mPaint);

        mPaint.setColor(mStrokeColor);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(mStrokeWidth);
        canvas.drawPath(mBorderPath, mPaint);
    }

    @Override
    public void setX(float x) {
        super.setX(x - mSize / 2f);
    }

    @Override
    public void setY(float y) {
        super.setY(y- mSize / 2f);
    }

    @Override
    public float getX() {
        return super.getX() + mSize / 2f;
    }

    @Override
    public float getY() {
        return super.getY()+ mSize / 2f;
    }
}
