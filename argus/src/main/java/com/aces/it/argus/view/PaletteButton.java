package com.aces.it.argus.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

/**
 * Created by O.Zakharov on 12.11.2020
 */
public class PaletteButton extends View {

    private static final float SWEEP_R = 12f;
    private static final float CENTER_R = 8f;
    private static final float BORDER = 1f;

    private Paint centerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint sweepPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    @SuppressWarnings("FieldCanBeLocal")
    private int borderColor = Color.BLACK;
    private int centerColor = Color.parseColor("#FF97A0AF");

    private int [] colors = {
            Color.parseColor("#FFFFDB00"),
            Color.parseColor("#FF10FF00"),
            Color.parseColor("#FF00FFD7"),
            Color.parseColor("#FF43FFF7"),
            Color.parseColor("#FF0016FF"),
            Color.parseColor("#FFEE28EA"),
            Color.parseColor("#FFFF0000"),
            Color.parseColor("#FFFF6B00"),
            Color.parseColor("#FFFFDB00")
    };
    private float density;

    private float[] positions = {0f, 0.0625f, 0.1875f, 0.3125f, 0.4375f, 0.5625f, 0.6875f, 0.8125f, 0.9375f};

    public PaletteButton(Context context) {
        this(context, null);
    }

    public PaletteButton(Context context, @Nullable AttributeSet attrs) {
        this (context, attrs, 0);
    }

    public PaletteButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        density = context.getResources().getDisplayMetrics().density;
        centerPaint.setStrokeWidth(BORDER * density);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        float cx = paddingLeft + (getWidth() - paddingLeft - paddingRight) / 2f;
        float cy = paddingTop + (getHeight() - paddingTop - paddingBottom) / 2f;
        SweepGradient sweepGradient = new SweepGradient(cx, cy, colors, positions);
        sweepPaint.setShader(sweepGradient);
    }


    @Override
    protected void onDraw(Canvas canvas) {

        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        float cx = paddingLeft + (getWidth() - paddingLeft - paddingRight) / 2f;
        float cy = paddingTop + (getHeight() - paddingTop - paddingBottom) / 2f;

        canvas.drawCircle(
                cx,
                cy,
                SWEEP_R * density,
                sweepPaint
        );

        centerPaint.setColor(centerColor);
        centerPaint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(
                cx,
                cy,
                CENTER_R * density,
                centerPaint
        );

        centerPaint.setColor(borderColor);
        centerPaint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(
                cx,
                cy,
                CENTER_R * density,
                centerPaint
        );
    }
}
