package com.aces.it.argus;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.aces.it.argus.model.Transformations;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by O.Zakharov on 10.02.2020
 */
public class Argus {

    public final static String ITEMS = "items";
    public final static String SELECTED_POSITION = "selectedPosition";
    public final static String CAN_RE_PHOTO = "canRePhoto";
    public final static String RE_PHOTO_URI = "rePhotoUri";
    public final static String NEW_PHOTO_URI = "newPhotoUri";
    public final static String SHOW_TUTORIAL = "showTutorial";
    public final static String SHOW_DRAW_TUTORIAL = "showDrawTutorial";
    public final static String TRANSFORMATIONS = "transformations";
    public final static String CROP_TRANSFORMATIONS = "cropTransformations";
    public static final String SOURCE_URI = "sourceUri";
    public static final String MAX_PHOTOS = "maxPhotos";

    static final int TYPE_CAMERA = 1;
    static final int TYPE_GALLERY = 2;
    static final int TYPE_DRAW = 3;
    static final int TYPE_CROP = 4;
    static final int TYPE_QR = 5;
    static final int TYPE_TAKE_PHOTO = 6;
    static final int TYPE_EDIT_PHOTO = 7;
    static final int TYPE_BARCODE = 8;

    static final String SAVE_DIR_URI = "save_uri_dir";
    static final String SAVED_URIS = "saved_uris";
    static final String TYPE = "type";
    static final String QR = "qr";
    static final String BARCODE = "barcode";

    static Locale locale = null;

    public static void initializeLog(Application app, boolean writeLogs) {
        Log.addLogLevel(Log.VERBOSE | Log.DEBUG);
        Log.setApp(app);
        Log.setAppName("argus");
        Log.removeDepricatedDirectory();
        Log.setMaxFileSize(5242880); //5mb
        Log.setMaxFileNumber(8);
        Log.setWriteLog(writeLogs);
        Log.i("===== Application started. Version: " + BuildConfig.VERSION_NAME + "-" + BuildConfig.VERSION_CODE);
    }

    public static void scanQR (
            Fragment fragment,
            int requestCode
    ) {
        Activity activity = fragment.getActivity();
        if (activity != null)
            fragment.startActivityForResult(
                    new Intent(activity, ArgusActivity.class)
                            .putExtra(TYPE, TYPE_QR),
                    requestCode
            );
    }

    public static void scanBarcode (
            Fragment fragment,
            int requestCode
    ) {
        Activity activity = fragment.getActivity();
        if (activity != null)
            fragment.startActivityForResult(
                    new Intent(activity, ArgusActivity.class)
                            .putExtra(TYPE, TYPE_BARCODE),
                    requestCode
            );
    }

    public static void getFromCamera (
            Fragment target,
            int requestCode,
            Uri saveDirUri,
            boolean showTutorial,
            boolean showDrawTutorial
    ) {
        getFromCamera(target, requestCode, saveDirUri, showTutorial, showDrawTutorial, 0);
    }

    public static void getFromCamera (
        Fragment target,
        int requestCode,
        Uri saveDirUri,
        boolean showTutorial,
        boolean showDrawTutorial,
        int maxPhotos
    ) {
        Activity activity = target.getActivity();
        if (activity != null) {
            Intent intent = getFromCameraIntent(
                activity,
                saveDirUri,
                showTutorial,
                showDrawTutorial,
                maxPhotos
            );
            target.startActivityForResult ( intent, requestCode);
        }
    }

    public static void getFromGallery(
            Fragment target,
            int requestCode,
            ArrayList<Uri> sourceUri,
            Uri saveDirUri,
            boolean showTutorial,
            boolean showDrawTutorial
    ) {
        Activity activity = target.getActivity();
        if (activity != null) {
            Intent intent = getFromGalleryIntent(
                    activity,
                    sourceUri,
                    saveDirUri,
                    showTutorial,
                    showDrawTutorial
            );
            target.startActivityForResult( intent, requestCode );
        }
    }

    public static void crop(
            Fragment target,
            int requestCode,
            Transformations transformations,
            Uri saveDirUri,
            Bundle args
    ) {
        Activity activity = target.getActivity();
        if (activity != null) {
            Intent intent = cropIntent (
                    activity,
                    transformations,
                    saveDirUri,
                    args
            );
            target.startActivityForResult(intent, requestCode);
        }
    }

    public static void draw (
            Fragment target,
            int requestCode,
            Transformations transformations,
            Uri saveDirUri,
            Bundle args,
            boolean showDrawTutorial
    ) {
        Activity activity = target.getActivity();
        if (activity != null) {
            Intent intent = drawIntent(
                    activity,
                    transformations,
                    saveDirUri,
                    args,
                    showDrawTutorial
            );
            target.startActivityForResult(intent, requestCode);
        }
    }

    public static void takePhoto (
            Fragment target,
            int requestCode,
            Uri saveDirUri,
            boolean showTutorial,
            boolean showDrawTutorial
    ) {
        Activity activity = target.getActivity();
        if (activity != null) {
            Intent intent = getTakePhotoIntent (
                    activity,
                    saveDirUri,
                    showTutorial,
                    showDrawTutorial
            );
            target.startActivityForResult ( intent, requestCode);
        }
    }

    public static void editPhoto (
            Fragment target,
            int requestCode,
            ArrayList<Uri> sourceUri,
            Uri saveDirUri,
            boolean showTutorial,
            boolean showDrawTutorial
    ) {
        Activity activity = target.getActivity();
        if (activity != null) {
            Intent intent = getEditPhotoIntent (
                    activity,
                    sourceUri,
                    saveDirUri,
                    showTutorial,
                    showDrawTutorial
            );
            target.startActivityForResult ( intent, requestCode);
        }
    }

    public static Intent scanQRIntent (Context context) {
        return new Intent(context, ArgusActivity.class).putExtra(TYPE, TYPE_QR);
    }

    public static Intent scanBarcodeIntent (Context context) {
        return new Intent(context, ArgusActivity.class).putExtra(TYPE, TYPE_BARCODE);
    }

    public static Intent getFromCameraIntent (
            Context context,
            Uri saveDirUri,
            boolean showTutorial,
            boolean showDrawTutorial
    ) {
        return getFromCameraIntent(context, saveDirUri, showTutorial, showDrawTutorial, 0);
    }

    public static Intent getFromCameraIntent (
        Context context,
        Uri saveDirUri,
        boolean showTutorial,
        boolean showDrawTutorial,
        int maxPhotos
    ) {
        return new Intent(context, ArgusActivity.class)
            .putExtra(SAVE_DIR_URI, saveDirUri != null ? saveDirUri : getDefaultDir(context))
            .putExtra(TYPE, TYPE_CAMERA)
            .putExtra(SHOW_TUTORIAL, showTutorial)
            .putExtra(SHOW_DRAW_TUTORIAL, showDrawTutorial)
            .putExtra(MAX_PHOTOS, maxPhotos);
    }

    public static Intent getFromGalleryIntent (
            Context context,
            ArrayList<Uri> sourceUri,
            Uri saveDirUri,
            boolean showTutorial,
            boolean showDrawTutorial
    ) {
        return new Intent(context, ArgusActivity.class)
                .putExtra(SOURCE_URI, sourceUri)
                .putExtra(SAVE_DIR_URI, saveDirUri)
                .putExtra(TYPE, TYPE_GALLERY)
                .putExtra(SHOW_TUTORIAL, showTutorial)
                .putExtra(SHOW_DRAW_TUTORIAL, showDrawTutorial);
    }

    public static Intent cropIntent (
            Context context,
            Transformations transformations,
            Uri saveDirUri,
            Bundle args
    ) {
        Intent intent = new Intent(context, ArgusActivity.class)
                .putExtra(TRANSFORMATIONS, transformations)
                .putExtra(SAVE_DIR_URI, saveDirUri)
                .putExtra(TYPE, TYPE_CROP);

        if (args != null)
            intent.putExtras(args);

        return intent;
    }

    public static Intent drawIntent (
            Context context,
            Transformations transformations,
            Uri saveDirUri,
            Bundle args,
            boolean showDrawTutorial
    ) {
        Intent intent = new Intent(context, ArgusActivity.class)
                .putExtra(TRANSFORMATIONS, transformations)
                .putExtra(SAVE_DIR_URI, saveDirUri)
                .putExtra(TYPE, TYPE_DRAW)
                .putExtra(SHOW_DRAW_TUTORIAL, showDrawTutorial);

        if (args != null)
            intent.putExtras(args);

        return intent;
    }

    public static Intent getTakePhotoIntent (
            Context context,
            Uri saveDirUri,
            boolean showTutorial,
            boolean showDrawTutorial
    ) {
        return new Intent(context, ArgusActivity.class)
                .putExtra(SAVE_DIR_URI, saveDirUri != null ? saveDirUri : getDefaultDir(context))
                .putExtra(TYPE, TYPE_TAKE_PHOTO)
                .putExtra(SHOW_TUTORIAL, showTutorial)
                .putExtra(SHOW_DRAW_TUTORIAL, showDrawTutorial);
    }

    public static Intent getEditPhotoIntent (
            Context context,
            ArrayList<Uri> sourceUri,
            Uri saveDirUri,
            boolean showTutorial,
            boolean showDrawTutorial
    ) {
        return new Intent(context, ArgusActivity.class)
                .putExtra(SOURCE_URI, sourceUri)
                .putExtra(SAVE_DIR_URI, saveDirUri)
                .putExtra(TYPE, TYPE_EDIT_PHOTO)
                .putExtra(SHOW_TUTORIAL, showTutorial)
                .putExtra(SHOW_DRAW_TUTORIAL, showDrawTutorial);
    }

    public static String parseQRIntent(Intent intent) {
        if (intent != null)
            return intent.getStringExtra(QR);
        else
            return null;
    }

    public static String parseBarcodeIntent(Intent intent) {
        if (intent != null)
            return intent.getStringExtra(BARCODE);
        else
            return null;
    }

    public static List<Uri> parseImageIntent(Intent intent) {
        if (intent != null) {
            ArrayList<Uri> uris = intent.getParcelableArrayListExtra(SAVED_URIS);
            if (uris != null)
                return uris;
            else
                return Collections.emptyList();
        } else
            return Collections.emptyList();
    }

    public static Transformations parseTransformationIntent(Intent intent) {
        if (intent != null) {
            return (Transformations) intent.getSerializableExtra(Argus.TRANSFORMATIONS);
        } else
            return null;
    }

    public static Boolean parseShowTutorial(Intent intent) {
        if (intent != null && intent.hasExtra(SHOW_TUTORIAL))
            return intent.getBooleanExtra(SHOW_TUTORIAL, false);
        else
            return null;
    }

    public static Boolean parseShowDrawTutorial(Intent intent) {
        if (intent != null && intent.hasExtra(SHOW_DRAW_TUTORIAL))
            return intent.getBooleanExtra(SHOW_DRAW_TUTORIAL, false);
        else
            return null;
    }

    public static void setLocale(Locale locale) {
        Argus.locale = locale;
    }

    private static Uri getDefaultDir(Context context) {
        boolean hasPermission = ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED;

        try {
            File parent = hasPermission ? context.getExternalCacheDir() : context.getCacheDir();
            File imagePath = new File(parent, "argus");
            if (!imagePath.exists())
                if (!imagePath.mkdirs())
                    return null;
            return Uri.parse(imagePath.getAbsolutePath());
        } catch (Exception e) {
            return null;
        }
    }
}
