package com.aces.it.argus.processor;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

/**
 * Created by O.Zakharov on 01.12.2020
 */
class LinesProcessor {

    private static final float POINTS_DELTA = 0.05f; // 5% of min image dimension
    private static final double THETA_DELTA = 10 * Math.PI / 180.0; //10 degrees - delta of theta in hough space to consider lines close enough
    private static final double R_DELTA = 50; //delta of rho in hough space, to consider lines close enough
    private static final double AREA_THRESHOLD = 0.05; //quadrilaterals area should be at least 20% of image area
    private static final float SIDES_RATIO = 0.4f; //quadrilaterals sides ratio threshold
    private static final double MIN_INTERSECTION_ANGLE = 80.0; //degress
    private static final double MAX_INTERSECTION_ANGLE = 100.0; //degress

    public synchronized static Quadrilateral getQuad(Mat lines, double imageWidth, double imageHeight) {
        HashMap<Integer, HashSet<Point>> linePoints = new HashMap<>();
        HashMap<Point, HashSet<Integer>> pointLines = new HashMap<>();
        initIntersectionsData(lines, imageWidth, imageHeight, linePoints, pointLines);
        return selectCorrectQuad(getQuadrilaterals(linePoints, pointLines), imageWidth, imageHeight);
    }

    private static void initIntersectionsData (
            Mat lines,                                      //matrix contains detected lines
            double imageWidth,                              //image width to detect intersections in lay in image bounds
            double imageHeight,                             //image height to detect intersections in lay in image bounds
            HashMap<Integer, HashSet<Point>> linePoints,    //map of lines to init
            HashMap<Point, HashSet<Integer>> pointLines     //map of points to init
    ) {
        double pointsDelta = POINTS_DELTA * Math.min(imageWidth, imageHeight);

        ArrayList<Point> intersections = new ArrayList<>();
        int linesCount = lines.rows();

        //select "strong" lines
        ArrayList<double[]> strongLines = new ArrayList<>();
        if (linesCount > 0) {
            strongLines.add (lines.get(0, 0));
            for (int i = 1; i < linesCount; i++) {
                double[] testLine = lines.get(i, 0);
                boolean strong = true;
                for (double[] strongLine: strongLines) {
                    if (Math.abs(strongLine[0] - testLine[0]) <= R_DELTA
                            && Math.abs(strongLine[1] - testLine[1]) <= THETA_DELTA) {
                        strong = false;
                        break;
                    }
                }

                if (strong)
                    strongLines.add(testLine);
            }
        }


        linesCount = strongLines.size();
        for (int i = 0; i < linesCount; i++) {
            double[] lineI = strongLines.get(i);
            for (int j = i + 1; j < linesCount; j++) {
                double[] lineJ = strongLines.get(j);
                double angle = getAngleBetweenLines(lineI, lineJ);
                if (Double.isNaN(angle) || (MIN_INTERSECTION_ANGLE < angle && angle < MAX_INTERSECTION_ANGLE)) {
                    Point intersection = getIntersection(lineI, lineJ);
                    if (intersection != null
                            && intersection.x >= 0 && intersection.x <= imageWidth
                            && intersection.y >= 0 && intersection.y <= imageHeight
                    ) {
                        Point added =
                                addIntersection(intersections, intersection, pointsDelta, pointsDelta);

                        HashSet<Point> iPoints = linePoints.get(i);
                        if (iPoints != null) {
                            iPoints.add(added);
                        } else {
                            iPoints = new HashSet<>();
                            iPoints.add(added);
                            linePoints.put(i, iPoints);
                        }

                        HashSet<Point> jPoints = linePoints.get(j);
                        if (jPoints != null) {
                            jPoints.add(added);
                        } else {
                            jPoints = new HashSet<>();
                            jPoints.add(added);
                            linePoints.put(j, jPoints);
                        }

                        HashSet<Integer> pl = pointLines.get(added);
                        if (pl != null) {
                            pl.add(i);
                            pl.add(j);
                        } else {
                            pl = new HashSet<>();
                            pl.add(i);
                            pl.add(j);
                            pointLines.put(added, pl);
                        }
                    }
                }
            }
        }
    }

    private static ArrayList<Quadrilateral> getQuadrilaterals (
            HashMap<Integer, HashSet<Point>> linePoints,    //map of lines and points laying on this line
            HashMap<Point, HashSet<Integer>> pointLines     //map of points and lines which has this point
    ) {
        ArrayList<Quadrilateral> quadrilaterals = new ArrayList<>();
        TreeSet<Integer> lineNumbers = new TreeSet<>(linePoints.keySet());
        for (Integer linenumber: lineNumbers) {
            ArrayList<Point> points1 = new ArrayList<>(linePoints.get(linenumber));
            for (int point1Number = 0; point1Number < points1.size(); point1Number++) {
                Point point1 =  points1.get(point1Number);
                HashSet<Integer> point1Lines = pointLines.get(point1);
                if (point1Lines == null)
                    continue;

                for (Integer point1Line: point1Lines) {
                    if (point1Line <= linenumber)
                        continue;
                    HashSet<Point> points2 = linePoints.get(point1Line);

                    for (Point point2: points2) {
                        HashSet<Integer> point2Lines = pointLines.get(point2);
                        if (point2Lines == null || point2.equals(point1))
                            continue;

                        for (Integer point2Line: point2Lines) {
                            if (point2Line <= linenumber || point2Line.equals(point1Line))
                                continue;
                            HashSet<Point> points3 = linePoints.get(point2Line);

                            for (Point point3: points3) {
                                HashSet<Integer> point3Lines = pointLines.get(point3);
                                if (point3Lines == null || point3.equals(point1)|| point3.equals(point2))
                                    continue;
                                for (Integer point3Line: point3Lines) {
                                    if (point3Line <= linenumber || point3Line.equals(point1Line) || point3Line.equals(point2Line))
                                        continue;
                                    HashSet<Point> points4 = linePoints.get(point3Line);

                                    for (Point point4: points4) {
                                        HashSet<Integer> point4Lines = pointLines.get(point4);
                                        if (point4Lines == null|| point4.equals(point1) || point4.equals(point2) || point4.equals(point3))
                                            continue;
                                        boolean found = false;
                                        for (Integer point4Line: point4Lines) {
                                            if (point4Line.equals(linenumber) && points1.indexOf(point4) > point1Number) {
                                                quadrilaterals.add(
                                                    new Quadrilateral(new Point[] {point1, point2, point3, point4})
                                                );
                                                found = true;
                                                break;
                                            }
                                        }

                                        if (found)
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        Collections.sort(quadrilaterals, COMPARATOR_BY_AREA);
        return quadrilaterals;
    }

    private static Quadrilateral selectCorrectQuad(ArrayList<Quadrilateral> quads, double imageWidth, double imageHeight) {
        double areaThreshold = AREA_THRESHOLD * imageWidth * imageHeight;
        for (Quadrilateral quad: quads) {
            if (quad.area < areaThreshold)
                return null;
            if (quad.ratio > SIDES_RATIO)
                return quad;
        }
        return null;
    }

    private synchronized static Point addIntersection(ArrayList<Point> intersections, Point intersection, double deltaX, double deltaY) {
        for (int i = 0; i < intersections.size(); i++) {
            Point exist = intersections.get(i);
            if (Math.abs(exist.x - intersection.x) <= deltaX && Math.abs(exist.y - intersection.y) <= deltaY ) {
                exist.x = (intersection.x + exist.x) / 2.0;
                exist.y = (intersection.y + exist.y) / 2.0;
                return exist;
            }
        }

        intersections.add(intersection);
        return intersection;
    }

    private synchronized static double getAngleBetweenLines(double[] line1, double[] line2) {
        double theta1 = line1[1];
        double theta2 = line2[1];
        double m1 = -(Math.cos(theta1) / Math.sin(theta1));
        double m2 = -(Math.cos(theta2) / Math.sin(theta2));
        double a = Math.abs(Math.atan(Math.abs(m2-m1) / (1 + m2 * m1))) * (180 / Math.PI);
        return a;
    }

    private synchronized static Point getIntersection (double[] line1, double[] line2) {
        double rho1 = line1[0], theta1 = line1[1];
        double rho2 = line2[0], theta2 = line2[1];
        double ct1 = Math.cos(theta1);
        double st1 = Math.sin(theta1);
        double ct2 = Math.cos(theta2);
        double st2 = Math.sin(theta2);
        double d = ct1 * st2 - st1 * ct2;
        if(d != 0.0) {
            return new Point(
                    (st2 * rho1 - st1 * rho2) / d,
                    (-ct2 * rho1 + ct1 * rho2)/ d
            );
        } else {
            return null;
        }
    }

    private static final Comparator<Quadrilateral> COMPARATOR_BY_AREA =
            (lhs, rhs) -> Double.compare(rhs.area, lhs.area);

    static class Quadrilateral {

        private final MatOfPoint contour;
        private final double area;
        private final Point[] points;
        private final float ratio;

        private Quadrilateral(Point[] points) {
            contour = new MatOfPoint(points);
            area = Imgproc.contourArea(contour);
            this.points = points;

            float sideSumm1 = sideLength(points[0], points[1]) + sideLength(points[2], points[3]);
            float sideSumm2 = sideLength(points[1], points[2]) + sideLength(points[3], points[0]);
            if (sideSumm1 > sideSumm2) {
                ratio = sideSumm2 / sideSumm1;
            } else if (sideSumm2 > sideSumm1) {
                ratio = sideSumm1 / sideSumm2;
            } else {
                ratio = 1f;
            }
        }

        private float sideLength(Point p1, Point p2) {
            return (float) Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
        }

        public MatOfPoint getContour() {
            return contour;
        }

        public Point[] getPoints() {
            return points;
        }

    }
}
