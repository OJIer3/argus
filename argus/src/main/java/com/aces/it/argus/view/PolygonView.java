package com.aces.it.argus.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;

import com.aces.it.argus.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by O.Zakharov on 04.09.2019
 */
public class PolygonView extends FrameLayout {

    private static final float magnifierZoom = 2f;

    protected Context context;
    private Paint polygonPaint;
    private View pointer1;
    private View pointer2;
    private View pointer3;
    private View pointer4;
    private View midPointer13;
    private View midPointer12;
    private View midPointer34;
    private View midPointer24;
    private ImageView imageView;

    private int minPointerX = 0;
    private int maxPointerX = 0;
    private int minPointerY = 0;
    private int maxPointerY = 0;
    private boolean pointersVisible = false;

    private int validColor;
    private int notValidColor;

    private Bitmap bitmap;
    private float bitmapScale = 0f;
    private final Map<Integer, PointF> bitmapPoints = new LinkedHashMap<>();

    private Paint magnifierPaint;
    private Paint magnifierBorderPaint;
    private Matrix magnifierMatrix;
    private int magnifierR;
    private int magnifierBorder;
    private int magnifierCenterDotR;
    private boolean drawMagnifier = false;

    private final int excludeWidth = (int) (200 * getResources().getDisplayMetrics().density);
    private int excludeDelta = getResources().getDimensionPixelSize(R.dimen.argus_drag_dot_r);


    public PolygonView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public PolygonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public PolygonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
    }

    private void init() {

        pointer1 = getPointerView(0, 0);
        pointer2 = getPointerView(getWidth(), 0);
        pointer3 = getPointerView(0, getHeight());
        pointer4 = getPointerView(getWidth(), getHeight());

        midPointer13 = getPointerView(0, getHeight() / 2);
        midPointer13.setOnTouchListener(new MidPointerTouchListener(pointer1, pointer3));

        midPointer12 = getPointerView(0, getWidth() / 2);
        midPointer12.setOnTouchListener(new MidPointerTouchListener(pointer1, pointer2));

        midPointer34 = getPointerView(0, getHeight() / 2);
        midPointer34.setOnTouchListener(new MidPointerTouchListener(pointer3, pointer4));

        midPointer24 = getPointerView(0, getHeight() / 2);
        midPointer24.setOnTouchListener(new MidPointerTouchListener(pointer2, pointer4));

        imageView = getImageView();

        addView(imageView);
        addView(pointer1);
        addView(pointer2);
        addView(midPointer13);
        addView(midPointer12);
        addView(midPointer34);
        addView(midPointer24);
        addView(pointer3);
        addView(pointer4);

        validColor = ContextCompat.getColor(context, R.color.argusColorPolygonStroke);
        notValidColor = ContextCompat.getColor(context, R.color.argusColorPolygonStrokeError);

        polygonPaint = new Paint();
        polygonPaint.setColor(validColor);
        polygonPaint.setStrokeWidth(context.getResources().getDimensionPixelSize(R.dimen.argus_polygon_border_width));
        polygonPaint.setAntiAlias(true);

        magnifierMatrix = new Matrix();

        magnifierPaint = new Paint();

        magnifierBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        magnifierBorderPaint.setStyle(Paint.Style.FILL);
        magnifierBorderPaint.setColor(ContextCompat.getColor(context, R.color.argusColorMagnifierStroke));

        magnifierR = context.getResources().getDimensionPixelSize(R.dimen.argus_magnifier_r);
        magnifierBorder = context.getResources().getDimensionPixelSize(R.dimen.argus_magnifier_border_width);
        magnifierCenterDotR = context.getResources().getDimensionPixelSize(R.dimen.argus_magnifier_center_dot_r);

        excludeDelta = Math.max ((int) (getResources().getDisplayMetrics().density * 16.5), excludeDelta);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldW, int oldH) {
        super.onSizeChanged(w, h, oldW, oldH);
        calculatePointersBounds();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        excludeSystemGestures();
        super.dispatchDraw(canvas);
        if (pointersVisible) {
            canvas.drawLine(pointer1.getX(), pointer1.getY(), pointer3.getX(), pointer3.getY(), polygonPaint);
            canvas.drawLine(pointer1.getX(), pointer1.getY(), pointer2.getX(), pointer2.getY(), polygonPaint);
            canvas.drawLine(pointer2.getX(), pointer2.getY(), pointer4.getX(), pointer4.getY(), polygonPaint);
            canvas.drawLine(pointer3.getX(), pointer3.getY(), pointer4.getX(), pointer4.getY(), polygonPaint);
            midPointer13.setX(pointer3.getX() - ((pointer3.getX() - pointer1.getX()) / 2));
            midPointer13.setY(pointer3.getY() - ((pointer3.getY() - pointer1.getY()) / 2));
            midPointer24.setX(pointer4.getX() - ((pointer4.getX() - pointer2.getX()) / 2));
            midPointer24.setY(pointer4.getY() - ((pointer4.getY() - pointer2.getY()) / 2));
            midPointer34.setX(pointer4.getX() - ((pointer4.getX() - pointer3.getX()) / 2));
            midPointer34.setY(pointer4.getY() - ((pointer4.getY() - pointer3.getY()) / 2));
            midPointer12.setX(pointer2.getX() - ((pointer2.getX() - pointer1.getX()) / 2));
            midPointer12.setY(pointer2.getY() - ((pointer2.getY() - pointer1.getY()) / 2));

            if (drawMagnifier) {
                canvas.drawCircle(getWidth() / 2f, getHeight() / 2f, magnifierBorder + magnifierR, magnifierBorderPaint);
                canvas.drawCircle(getWidth() / 2f, getHeight() / 2f, magnifierR, magnifierPaint);
                canvas.drawCircle(getWidth() / 2f, getHeight() / 2f, magnifierCenterDotR, magnifierBorderPaint);
            }
        }
    }

    private void excludeSystemGestures() {
        ArrayList<Rect> excluded = new ArrayList<>();
        int w = getWidth();

        if (pointer1.getX() < excludeWidth) {
            int y = (int) pointer1.getY();
            excluded.add(new Rect(
                0,
                y - excludeDelta,
                excludeWidth,
                y + excludeDelta
            ));
        } else if (pointer1.getX() > w - excludeWidth) {
            int y = (int) pointer1.getY();
            excluded.add(new Rect(
                w - excludeWidth,
                y - excludeDelta,
                w,
                y + excludeDelta
            ));
        }

        if (pointer2.getX() < excludeWidth) {
            int y = (int) pointer2.getY();
            excluded.add(new Rect(
                0,
                y - excludeDelta,
                excludeWidth,
                y + excludeDelta
            ));
        } else if (pointer2.getX() > w - excludeWidth) {
            int y = (int) pointer2.getY();
            excluded.add(new Rect(
                w - excludeWidth,
                y - excludeDelta,
                w,
                y + excludeDelta
            ));
        }

        if (pointer3.getX() < excludeWidth) {
            int y = (int) pointer3.getY();
            excluded.add(new Rect(
                0,
                y - excludeDelta,
                excludeWidth,
                y + excludeDelta
            ));
        } else if (pointer3.getX() > w - excludeWidth) {
            int y = (int) pointer3.getY();
            excluded.add(new Rect(
                w - excludeWidth,
                y - excludeDelta,
                w,
                y + excludeDelta
            ));
        }

        if (pointer4.getX() < excludeWidth) {
            int y = (int) pointer4.getY();
            excluded.add(new Rect(
                0,
                y - excludeDelta,
                excludeWidth,
                y + excludeDelta
            ));
        } else if (pointer4.getX() > w - excludeWidth) {
            int y = (int) pointer4.getY();
            excluded.add(new Rect(
                w - excludeWidth,
                y - excludeDelta,
                w,
                y + excludeDelta
            ));
        }

        if (midPointer12.getX() < excludeWidth) {
            int y = (int) midPointer12.getY();
            excluded.add(new Rect(
                0,
                y - excludeDelta,
                excludeWidth,
                y + excludeDelta
            ));
        } else if (midPointer12.getX() > w - excludeWidth) {
            int y = (int) midPointer12.getY();
            excluded.add(new Rect(
                w - excludeWidth,
                y - excludeDelta,
                w,
                y + excludeDelta
            ));
        }

        if (midPointer13.getX() < excludeWidth) {
            int y = (int) midPointer13.getY();
            excluded.add(new Rect(
                0,
                y - excludeDelta,
                excludeWidth,
                y + excludeDelta
            ));
        } else if (midPointer13.getX() > w - excludeWidth) {
            int y = (int) midPointer13.getY();
            excluded.add(new Rect(
                w - excludeWidth,
                y - excludeDelta,
                w,
                y + excludeDelta
            ));
        }

        if (midPointer24.getX() < excludeWidth) {
            int y = (int) midPointer24.getY();
            excluded.add(new Rect(
                0,
                y - excludeDelta,
                excludeWidth,
                y + excludeDelta
            ));
        } else if (midPointer24.getX() > w - excludeWidth) {
            int y = (int) midPointer24.getY();
            excluded.add(new Rect(
                w - excludeWidth,
                y - excludeDelta,
                w,
                y + excludeDelta
            ));
        }

        if (midPointer34.getX() < excludeWidth) {
            int y = (int) midPointer34.getY();
            excluded.add(new Rect(
                0,
                y - excludeDelta,
                excludeWidth,
                y + excludeDelta
            ));
        } else if (midPointer34.getX() > w - excludeWidth) {
            int y = (int) midPointer34.getY();
            excluded.add(new Rect(
                w - excludeWidth,
                y - excludeDelta,
                w,
                y + excludeDelta
            ));
        }

        ViewCompat.setSystemGestureExclusionRects(this, excluded);
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
        imageView.setImageBitmap(bitmap);
        bitmapPoints.clear();
        calculatePointersBounds();

        if (this.bitmap != null) {
            BitmapShader shader = new BitmapShader(this.bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            magnifierPaint.setShader(shader);
        }
    }

    public void setBitmapPoints(Map<Integer, PointF> pointFMap) {
        bitmapPoints.clear();
        if (pointFMap.size() == 4)
            bitmapPoints.putAll(getOrderedPoints(pointFMap.values()));
        setPointersCoordinates();
    }

    public Map<Integer, PointF> getBitmapPoints() {
        Map<Integer, PointF> points = getPoints();
        if (bitmapScale != 0) {
            for (PointF point : points.values()) {
                point.x = (point.x - minPointerX) / bitmapScale;
                point.y = (point.y - minPointerY) / bitmapScale;
            }
        }
        return points;
    }

    public void setGrayscale(boolean grayscale) {
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(grayscale ? 0 : 1);
        ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
        imageView.setColorFilter(colorFilter);
        magnifierPaint.setColorFilter(colorFilter);
    }

    private Map<Integer, PointF> getPoints() {

        List<PointF> points = new ArrayList<>();
        points.add(new PointF(pointer1.getX(), pointer1.getY()));
        points.add(new PointF(pointer2.getX(), pointer2.getY()));
        points.add(new PointF(pointer3.getX(), pointer3.getY()));
        points.add(new PointF(pointer4.getX(), pointer4.getY()));

        return getOrderedPoints(points);
    }

    private Map<Integer, PointF> getOrderedPoints(Collection<PointF> points) {

        PointF centerPoint = new PointF();
        int size = points.size();
        for (PointF pointF : points) {
            centerPoint.x += pointF.x / size;
            centerPoint.y += pointF.y / size;
        }
        @SuppressLint("UseSparseArrays")
        Map<Integer, PointF> orderedPoints = new HashMap<>();
        for (PointF pointF : points) {
            int index = -1;
            if (pointF.x < centerPoint.x && pointF.y < centerPoint.y) {
                index = 0;
            } else if (pointF.x > centerPoint.x && pointF.y < centerPoint.y) {
                index = 1;
            } else if (pointF.x < centerPoint.x && pointF.y > centerPoint.y) {
                index = 2;
            } else if (pointF.x > centerPoint.x && pointF.y > centerPoint.y) {
                index = 3;
            }
            orderedPoints.put(index, pointF);
        }
        return orderedPoints;
    }

    private void calculatePointersBounds() {

        minPointerX = 0;
        minPointerY = 0;
        maxPointerX = getWidth();
        maxPointerY = getHeight();
        bitmapScale = 0f;

        if (imageView != null) {
            int width = imageView.getWidth() - imageView.getPaddingLeft() - imageView.getPaddingRight();
            int height = imageView.getHeight() - imageView.getPaddingTop() - imageView.getPaddingBottom();
            if (width > 0 && height > 0) {
                if (bitmap != null && bitmap.getHeight() != 0 && bitmap.getWidth() != 0) {
                    bitmapScale = Math.min(width / (float) bitmap.getWidth(), height / (float) bitmap.getHeight());
                    minPointerX = (int) ((getWidth() - bitmap.getWidth() * bitmapScale) / 2f);
                    maxPointerX = minPointerX + (int) (bitmap.getWidth() * bitmapScale);
                    minPointerY = (int) ((getHeight() - bitmap.getHeight() * bitmapScale) / 2f);
                    maxPointerY = minPointerY + (int) (bitmap.getHeight() * bitmapScale);
                }
            }
        }

        setPointersCoordinates();
    }

    @SuppressWarnings("ConstantConditions")
    private void setPointersCoordinates() {

        if (bitmapPoints.size() == 4) {

            pointer1.setX(bitmapPoints.get(0).x * bitmapScale + minPointerX);
            pointer1.setY(bitmapPoints.get(0).y * bitmapScale + minPointerY);

            pointer2.setX(bitmapPoints.get(1).x * bitmapScale + minPointerX);
            pointer2.setY(bitmapPoints.get(1).y * bitmapScale + minPointerY);

            pointer3.setX(bitmapPoints.get(2).x * bitmapScale + minPointerX);
            pointer3.setY(bitmapPoints.get(2).y * bitmapScale + minPointerY);

            pointer4.setX(bitmapPoints.get(3).x * bitmapScale + minPointerX);
            pointer4.setY(bitmapPoints.get(3).y * bitmapScale + minPointerY);

            pointersVisible(true);
        } else {
            pointersVisible(false);
        }
    }

    private void pointersVisible(boolean visible) {
        pointersVisible = visible;
        if (visible) {
            pointer1.setVisibility(View.VISIBLE);
            pointer3.setVisibility(View.VISIBLE);
            pointer3.setVisibility(View.VISIBLE);
            pointer4.setVisibility(View.VISIBLE);
            midPointer12.setVisibility(View.VISIBLE);
            midPointer13.setVisibility(View.VISIBLE);
            midPointer24.setVisibility(View.VISIBLE);
            midPointer34.setVisibility(View.VISIBLE);
        } else {
            pointer1.setVisibility(View.INVISIBLE);
            pointer3.setVisibility(View.INVISIBLE);
            pointer3.setVisibility(View.INVISIBLE);
            pointer4.setVisibility(View.INVISIBLE);
            midPointer12.setVisibility(View.INVISIBLE);
            midPointer13.setVisibility(View.INVISIBLE);
            midPointer24.setVisibility(View.INVISIBLE);
            midPointer34.setVisibility(View.INVISIBLE);
        }
    }

    private float correctPointerX(float pointerX) {
        if (pointerX > maxPointerX)
            return maxPointerX;
        else if (pointerX < minPointerX)
            return minPointerX;
        else
            return pointerX;
    }

    private float correctPointerY(float pointerY) {
        if (pointerY > maxPointerY)
            return maxPointerY;
        else if (pointerY < minPointerY)
            return minPointerY;
        else
            return pointerY;
    }

    private void drawMag(float x,float y) {
        float bitmapX = (x - minPointerX) / bitmapScale;
        float bitmapY = (y - minPointerY) / bitmapScale;
        magnifierMatrix.reset();
        magnifierMatrix.postScale(magnifierZoom, magnifierZoom, bitmapX, bitmapY);
        magnifierMatrix.postTranslate(getWidth() / 2f - bitmapX, getHeight() / 2f - bitmapY);
        magnifierPaint.getShader().setLocalMatrix(magnifierMatrix);
        drawMagnifier = true;
    }

    private void dismissMag() {
        drawMagnifier = false;
    }

    @SuppressLint("ClickableViewAccessibility")
    private View getPointerView(int x, int y) {
        View pointerView = new DragDotView(context);
        pointerView.setX(x);
        pointerView.setY(y);
        pointerView.setOnTouchListener(new CornerPointerTouchListener());
        return pointerView;
    }

    private ImageView getImageView() {
        ImageView imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.CENTER);
        imageView.setAdjustViewBounds(true);
        FrameLayout.LayoutParams params =
                new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.leftMargin = params.rightMargin =
                params.topMargin = params.bottomMargin =
                        context.getResources().getDimensionPixelSize(R.dimen.argus_drag_dot_r) +
                                context.getResources().getDimensionPixelSize(R.dimen.argus_drag_dot_border_width) / 2;
        imageView.setLayoutParams(params);
        return imageView;
    }

    private boolean isValidShape(Map<Integer, PointF> pointFMap) {
        return pointFMap.size() == 4;
    }

    private class MidPointerTouchListener implements OnTouchListener {

        private float downX;
        private float downY;

        private final View mainPointer1;
        private final View mainPointer2;

        MidPointerTouchListener (View mainPointer1, View mainPointer2) {
            this.mainPointer1 = mainPointer1;
            this.mainPointer2 = mainPointer2;
        }

        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int eid = event.getAction();
            switch (eid) {
                case MotionEvent.ACTION_MOVE:
                    float moveX = event.getX() - downX;
                    float moveY = event.getY() - downY;

                    if (Math.abs(mainPointer1.getX() - mainPointer2.getX()) > Math.abs(mainPointer1.getY() - mainPointer2.getY())) {
                        //vertical drag
                        mainPointer1.setY((int) correctPointerY(mainPointer1.getY() + moveY));
                        mainPointer2.setY((int) correctPointerY(mainPointer2.getY() + moveY));
                    } else {
                        //horizontal drag
                        mainPointer1.setX((int) correctPointerX(mainPointer1.getX() + moveX));
                        mainPointer2.setX((int) correctPointerX(mainPointer2.getX() + moveX));
                    }

                    drawMag(
                            Math.max(mainPointer1.getX(), mainPointer2.getX()) - Math.abs((mainPointer1.getX() - mainPointer2.getX()) / 2f),
                            Math.max(mainPointer1.getY(), mainPointer2.getY()) - Math.abs((mainPointer1.getY() - mainPointer2.getY()) / 2f)
                    );
                    break;

                case MotionEvent.ACTION_DOWN:
                    downX = event.getX();
                    downY = event.getY();
                    break;

                case MotionEvent.ACTION_UP:
                    polygonPaint.setColor(isValidShape(getPoints()) ? validColor : notValidColor);
                    dismissMag();
                    break;

                default:
                    break;
            }
            PolygonView.this.invalidate();
            return true;
        }
    }

    private class CornerPointerTouchListener implements OnTouchListener {

        private float downX;
        private float downY;

        private float viewX;
        private float viewY;

        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_MOVE:
                    viewX = correctPointerX(viewX + event.getX() - downX);
                    viewY = correctPointerY(viewY + event.getY() - downY);
                    v.setX((int) viewX);
                    v.setY((int) viewY);
                    drawMag(viewX, viewY);
                    break;

                case MotionEvent.ACTION_DOWN:
                    downX = event.getX();
                    downY = event.getY();
                    viewX = v.getX();
                    viewY = v.getY();
                    break;

                case MotionEvent.ACTION_UP:
                    polygonPaint.setColor(isValidShape(getPoints()) ? validColor : notValidColor);
                    dismissMag();
                    break;
            }
            PolygonView.this.invalidate();
            return true;
        }

    }

}
