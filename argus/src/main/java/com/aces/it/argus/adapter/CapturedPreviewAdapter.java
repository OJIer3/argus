package com.aces.it.argus.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aces.it.argus.R;
import com.aces.it.argus.model.Transformations;
import com.aces.it.argus.view.GalleryImageView;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by O.Zakharov on 05.11.2020
 */
public class CapturedPreviewAdapter extends RecyclerView.Adapter<CapturedPreviewAdapter.Holder> {

    private ArrayList<Transformations> images = new ArrayList<>();
    private int maxShownImagesCount = 0;
    private boolean hideFirstImage = false;
    private View firstView = null;
    private Listener listener;

    @NonNull
    @Override
    public CapturedPreviewAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.argus_li_captured_view, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CapturedPreviewAdapter.Holder holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return Math.min(images.size(), maxShownImagesCount);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setImages(List<Transformations> uris) {
        images.clear();
        if (uris != null && !uris.isEmpty())
            images.addAll(uris);
    }

    public void hideFirstImage(boolean hideFirstImage) {
        this.hideFirstImage = hideFirstImage;
        if (firstView != null) {
            firstView.setAlpha(hideFirstImage ? 0f : 1f);
            firstView.findViewById(R.id.capturedPreview).setAlpha(hideFirstImage ? 0f : 1f);
        } else {
            if (getItemCount() > 0)
                notifyItemChanged(0);
        }
    }

    public void addImage(Transformations transformations, boolean hideFirstImage) {
        images.add(0, transformations);
        this.hideFirstImage = hideFirstImage;
        notifyItemInserted(0);
        if (images.size() > getItemCount())
            notifyItemChanged(getItemCount() - 1);
    }

    public void deleteLastImage() {
        if (images.size() > 0) {
            images.remove(0);
            notifyItemRemoved(0);
            if (images.size() >= maxShownImagesCount)
                notifyItemChanged(getItemCount() - 2);
        }
    }

    public void setMaxShownImagesCount(int count) {
        maxShownImagesCount = count;
        notifyDataSetChanged();
    }

    class Holder extends RecyclerView.ViewHolder {

        private GalleryImageView imageView;
        private TextView textView;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.capturedPreview);
            imageView.setDrawingEnabled(false);
            imageView.setZoomEnabled(false);
            imageView.setMinimumScaleType(SubsamplingScaleImageView.SCALE_TYPE_CENTER_CROP);

            textView = itemView.findViewById(R.id.moreCount);
            View.OnClickListener onClick = v -> {
                if (listener != null) {
                    final int position = getAdapterPosition();
                    if (position < images.size())
                        listener.onItemSelected(images.get(position));
                }
            };
            imageView.setOnClickListener(onClick);
            itemView.setOnClickListener(onClick);
        }

        public void bind () {
            final int position = getAdapterPosition();
            final int imagesCount = images.size();
            final int itemsCount = getItemCount();

            if (position == 0)
                firstView = itemView;

            if (position < imagesCount) {
                imageView.setImage (ImageSource.uri(images.get(position).getUri()));
                imageView.setTransformations(images.get(position));

                if (imagesCount > itemsCount && position == itemsCount - 1) {
                    textView.setText(String.format(Locale.ENGLISH, "+%d", imagesCount - itemsCount));
                    textView.setVisibility(View.VISIBLE);
                } else {
                    textView.setVisibility(View.GONE);
                }
            }

            itemView.setAlpha(position == 0 && hideFirstImage ? 0f : 1f);
            imageView.setAlpha(position == 0 && hideFirstImage ? 0f : 1f);
        }
    }

    public interface Listener {
        void onItemSelected(Transformations transformations);
    }
}
