package com.aces.it.argus.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.aces.it.argus.ArgusActivity;
import com.aces.it.argus.Callback;

/**
 * Created by O.Zakharov on 19.02.2020
 */
public abstract class BaseFragment extends Fragment {

    protected Callback callback;

    public BaseFragment() {
        setRetainInstance(true);
        setArguments(new Bundle());
    }

    protected boolean hasCameraPermission() {
        Activity activity = getActivity();
        if (activity != null)
            return ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
        else
            return false;
    }

    public void onCameraPermissionGranted() {
    }

    protected void replaceFragment (Fragment fragment, boolean addToBackStack) {
        Activity activity = getActivity();
        if (activity instanceof ArgusActivity && !isStateSaved()) {
            ((ArgusActivity) activity).replaceFragment(fragment, addToBackStack);
        }
    }

    protected void onBackPressed() {
        Activity activity = getActivity();
        if (activity != null)
            activity.onBackPressed();
    }

    public boolean onBackPressedHandled() {
        return false;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof Callback)
            callback = (Callback) context;
    }
}
