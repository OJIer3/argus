package com.aces.it.argus.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

import java.io.InputStream;

/**
 * Created by O.Zakharov on 25.05.2021
 */
public class GifImageView extends AppCompatImageView {

    private Movie movie = null;
    private long movieStart = 0;
    private long movieDuration = 0;

    public GifImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setLayerType(LAYER_TYPE_SOFTWARE, null);
    }

    public GifImageView(Context context) {
        super(context);
        setLayerType(LAYER_TYPE_SOFTWARE, null);
    }

    public GifImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setLayerType(LAYER_TYPE_SOFTWARE, null);
    }

    public void setGif (int gifResourceId) {
        try {
            InputStream inputStream = getContext().getResources().openRawResource(gifResourceId);
            movie = Movie.decodeStream(inputStream);
            inputStream.close();
            movieStart = System.currentTimeMillis();
            movieDuration = movie.duration();
            if (movieDuration == 0)
                movieDuration = 3000;
            setImageDrawable(null);
            invalidate();
        }
        catch (Exception ignore) {}
    }

    public void restart() {
        movieStart = System.currentTimeMillis();
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int w = 0;
        int h = 0;
        if (movie != null) {
            w = movie.width();
            h = movie.height();
        }
        setMeasuredDimension(w, h);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (movie != null) {
            int relTime = (int) ((System.currentTimeMillis() - movieStart) % movieDuration);
            movie.setTime(relTime);
            movie.draw(canvas, 0, 0);
            invalidate();
        }
    }

}
