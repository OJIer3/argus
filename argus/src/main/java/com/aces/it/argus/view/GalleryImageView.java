package com.aces.it.argus.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.aces.it.argus.Log;
import com.aces.it.argus.R;
import com.aces.it.argus.model.Correction;
import com.aces.it.argus.model.Transformations;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by O.Zakharov on 12.11.2020
 */
public class GalleryImageView extends SubsamplingScaleImageView implements View.OnTouchListener {

    private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private PointF sourceStart = null;
    private PointF sourceEnd = null;
    private PointF viewEnd = new PointF();
    private PointF viewStart = new PointF();

    private int correctionColor;
    private ColorFilter grayColorFilter;
    private ColorFilter normalColorFilter;

    private boolean drawingEnabled = false;

    private Matrix rotateCorrectionsMatrix = new Matrix();
    //private ArrayList<Correction> corrections = new ArrayList<>();
    private ArrayList<Correction> undoCorrections = new ArrayList<>();
    private ArrayList<Correction> redoCorrections = new ArrayList<>();

    private MutableLiveData<Boolean> undoPossible = new MutableLiveData<>(false);
    private MutableLiveData<Boolean> redoPossible = new MutableLiveData<>(false);

    public void setCorrectionColor(int color) {
        correctionColor = color;
    }

    public int getCorrectionColor() {
        return correctionColor;
    }

    public void setTransformations(Transformations transformations) {
        //corrections.clear();
        //corrections.addAll(transformations.getCorrections());
        undoCorrections.clear();
        undoCorrections.addAll(transformations.getCorrections());
        undoPossible.setValue(!undoCorrections.isEmpty());
        redoPossible.setValue(!redoCorrections.isEmpty());
        setGrayScale(transformations.isGrayScale());
        if (getOrientation() != transformations.getOrientation())
            setOrientation(transformations.getOrientation());
        initRotateMatrix();
        invalidate();
    }

    public void setDrawingEnabled(boolean enabled) {
        drawingEnabled = enabled;
    }

    public void undo() {
        int cancelableSize = undoCorrections.size();
        if (cancelableSize > 0) {
            redoCorrections.add(0, undoCorrections.remove(cancelableSize - 1));
        }
        undoPossible.setValue(!undoCorrections.isEmpty());
        redoPossible.setValue(!redoCorrections.isEmpty());
        invalidate();
    }

    public void redo() {
        int repeatSize = redoCorrections.size();
        if (repeatSize > 0) {
            undoCorrections.add(redoCorrections.remove(0));
        }
        undoPossible.setValue(!undoCorrections.isEmpty());
        redoPossible.setValue(!redoCorrections.isEmpty());
        invalidate();
    }

    public GalleryImageView(Context context, AttributeSet attr) {
        super(context, attr);
        initialise();
    }

    public GalleryImageView(Context context) {
        this(context, null);
    }

    public LiveData<Boolean> getUndoPossibility() {
        return undoPossible;
    }

    public LiveData<Boolean> getRedoPossibility() {
        return redoPossible;
    }

    public List<Correction> getAppliedCorrections() {
        //ArrayList<Correction> corrections = new ArrayList<>(this.corrections);
        //corrections.addAll(undoCorrections);
        return new ArrayList<>(undoCorrections);
    }

    private void initialise() {
        setOnTouchListener(this);
        paint.setStyle(Paint.Style.FILL);

        correctionColor = ContextCompat.getColor(getContext(), R.color.argusPalette3);

        ColorMatrix grayColor = new ColorMatrix();
        grayColor.setSaturation(0);
        grayColorFilter = new ColorMatrixColorFilter(grayColor);

        ColorMatrix normalColor = new ColorMatrix();
        normalColor.setSaturation(1);
        normalColorFilter = new ColorMatrixColorFilter(normalColor);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        if (!drawingEnabled)
            return super.onTouchEvent(event);

        boolean consumed = false;
        int touchCount = event.getPointerCount();
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                if (event.getActionIndex() == 0) {
                    sourceStart = viewToSourceCoord(event.getX(), event.getY());
                    if (sourceStart != null &&
                            (sourceStart.x < 0 || sourceStart.y < 0 ||
                            sourceStart.x > getSourceWidth() || sourceStart.y > getSourceHeight())
                    )
                        sourceStart = null;
                } else {
                    sourceStart = null;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                sourceEnd = viewToSourceCoord(event.getX(), event.getY());
                if (sourceEnd != null) {
                    sourceEnd.x = Math.max(0, Math.min(sourceEnd.x, getSourceWidth()));
                    sourceEnd.y = Math.max(0, Math.min(sourceEnd.y, getSourceHeight()));
                }

                if (touchCount == 1 && sourceStart != null) {
                    consumed = true;
                    invalidate();
                } else if (touchCount == 1) {
                    consumed = true;
                } else {
                    sourceEnd = null;
                    sourceStart = null;
                    invalidate();
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                addRect();
                invalidate();
        }

        return consumed || super.onTouchEvent(event);
    }

    private void initRotateMatrix() {
        rotateCorrectionsMatrix.reset();
        int width = getSWidth();
        int height = getSHeight();
        switch (getOrientation()) {
            case SubsamplingScaleImageView.ORIENTATION_90: {
                rotateCorrectionsMatrix.setRotate(90f, width / 2f, height / 2f);
                rotateCorrectionsMatrix.postTranslate((height - width) / 2f, (width - height) / 2f);
                break;
            }

            case SubsamplingScaleImageView.ORIENTATION_180: {
                rotateCorrectionsMatrix.setRotate(180f, width / 2f, height / 2f);
                break;
            }

            case SubsamplingScaleImageView.ORIENTATION_270: {
                rotateCorrectionsMatrix.setRotate(270f, width / 2f, height / 2f);
                rotateCorrectionsMatrix.postTranslate((height - width) / 2f, (width - height) / 2f);
                break;
            }

            default: {
                break;
            }
        }
    }

    private void addRect() {
        if (sourceStart != null && sourceEnd != null) {
            float[] coordinates = {sourceStart.x, sourceStart.y, sourceEnd.x, sourceEnd.y};
            Matrix matrix = new Matrix();
            rotateCorrectionsMatrix.invert(matrix);
            matrix.mapPoints(coordinates);
            undoCorrections.add(new Correction (coordinates, paint.getColor()));
        }

        sourceEnd = null;
        sourceStart = null;
        redoCorrections.clear();

        redoPossible.setValue(false);
        undoPossible.setValue(!undoCorrections.isEmpty());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (!isReady()) {
            return;
        }

        initRotateMatrix();

        /*for (Correction correction: corrections) {
            float[] points = {correction.startX, correction.startY, correction.endX, correction.endY};
            rotateCorrectionsMatrix.mapPoints(points);

            sourceToViewCoord(points[0], points[1], viewStart);
            sourceToViewCoord(points[2], points[3], viewEnd);

            paint.setColor(correction.color);
            paint.setColorFilter(correction.isGrayScale ? grayColorFilter : normalColorFilter);

            canvas.drawRect(viewStart.x, viewStart.y, viewEnd.x, viewEnd.y, paint);
        }*/

        for (Correction correction: undoCorrections) {
            float[] points = {correction.startX, correction.startY, correction.endX, correction.endY};
            rotateCorrectionsMatrix.mapPoints(points);

            sourceToViewCoord(points[0], points[1], viewStart);
            sourceToViewCoord(points[2], points[3], viewEnd);

            paint.setColor(correction.color);
            paint.setColorFilter(correction.isGrayScale ? grayColorFilter : normalColorFilter);

            canvas.drawRect(viewStart.x, viewStart.y, viewEnd.x, viewEnd.y, paint);
        }

        if (sourceStart != null && sourceEnd != null) {
            paint.setColor(correctionColor);
            paint.setColorFilter(normalColorFilter);
            sourceToViewCoord(sourceStart.x, sourceStart.y, viewStart);
            sourceToViewCoord(sourceEnd.x, sourceEnd.y, viewEnd);
            canvas.drawRect(viewStart.x, viewStart.y, viewEnd.x, viewEnd.y, paint);
        }
    }

    @Override
    protected void onImageLoaded() {
        super.onImageLoaded();
        initRotateMatrix();
    }

    private int getSourceWidth() {
        if (getOrientation() == ORIENTATION_90 || getOrientation() == ORIENTATION_270)
            return getSHeight();
        else
            return getSWidth();
    }

    private int getSourceHeight() {
        if (getOrientation() == ORIENTATION_90 || getOrientation() == ORIENTATION_270)
            return getSWidth();
        else
            return getSHeight();
    }

    private void setGrayScale(boolean grayScale) {
        try {
            Class<?> superClass = this.getClass().getSuperclass();
            if (superClass != null) {
                Method createPaints = superClass.getDeclaredMethod("createPaints");
                createPaints.setAccessible(true);
                createPaints.invoke(this);
                Field field = superClass.getDeclaredField("bitmapPaint");
                field.setAccessible(true);
                Object paint = field.get(this);
                if (paint != null) {
                    Method setColorFilter = paint.getClass().getDeclaredMethod("setColorFilter", ColorFilter.class);
                    if (grayScale)
                        setColorFilter.invoke(paint, grayColorFilter);
                    else
                        setColorFilter.invoke(paint, normalColorFilter);
                }
            }
        } catch (Exception e) {
            Log.e(e);
        }
    }
}
