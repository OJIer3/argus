package com.aces.it.argus.model;

import android.annotation.SuppressLint;
import android.graphics.Path;
import android.graphics.PointF;

import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;

import java.util.HashMap;
import java.util.Map;

public class Quad {

    public static final long ANIMATE_TIME = 100L;

    private final MatOfPoint2f mContour;
    private final Point[] mPoints;
    private final float mDetectionScale;
    private int mColor;
    private int mBorderColor;
    private Quad mPreviousQuad;
    private long mPreviousQuadTime;

    public Quad(MatOfPoint2f contour, Point[] points) {
        mContour = contour;
        mPoints = points;
        mDetectionScale = 1.0f;
    }

    public Quad(MatOfPoint2f contour, Point[] points, float detectionScale) {
        mContour = contour;
        mPoints = points;
        mDetectionScale = detectionScale;
    }

    public MatOfPoint2f getContour() {
        return mContour;
    }

    public Point[] getPoints() {
        return mPoints;
    }

    public double getDetectionScale() {
        return mDetectionScale;
    }

    public static boolean isValid(Quad quad) {
        return quad != null
                && quad.mContour != null
                && quad.mPoints != null && quad.mPoints.length == 4
                && quad.mPoints[0] != null && quad.mPoints[1] != null
                && quad.mPoints[2] != null && quad.mPoints[3] != null;
    }

    public void setPreviousQuad(Quad quad) {
        mPreviousQuad = quad;
        mPreviousQuadTime = System.currentTimeMillis();
    }

    public void setColor(int mColor) {
        this.mColor = mColor;
    }

    public void setBorderColor(int mBorderColor) {
        this.mBorderColor = mBorderColor;
    }

    public int getColor() {
        return mColor;
    }

    public int getBorderColor() {
        return mBorderColor;
    }

    public boolean addOnPath(Path path, float scale) {
        path.reset();

        if (scale == 0) scale = 1;
        float factor = 1f - (System.currentTimeMillis() - mPreviousQuadTime) / (float) ANIMATE_TIME;

        if (mPreviousQuad == null || factor > 1 || factor < 0) {
            path.moveTo((float) mPoints[0].x * scale, (float) mPoints[0].y * scale);
            path.lineTo((float) mPoints[1].x * scale, (float) mPoints[1].y * scale);
            path.lineTo((float) mPoints[2].x * scale, (float) mPoints[2].y * scale);
            path.lineTo((float) mPoints[3].x * scale, (float) mPoints[3].y * scale);
            path.lineTo((float) mPoints[0].x * scale, (float) mPoints[0].y * scale);
            mPreviousQuad = null;
        } else {
            path.moveTo((float) (mPoints[0].x - (mPoints[0].x - mPreviousQuad.mPoints[0].x) * factor) * scale, (float) (mPoints[0].y - (mPoints[0].y - mPreviousQuad.mPoints[0].y) * factor) * scale);
            path.lineTo((float) (mPoints[1].x - (mPoints[1].x - mPreviousQuad.mPoints[1].x) * factor) * scale, (float) (mPoints[1].y - (mPoints[1].y - mPreviousQuad.mPoints[1].y) * factor) * scale);
            path.lineTo((float) (mPoints[2].x - (mPoints[2].x - mPreviousQuad.mPoints[2].x) * factor) * scale, (float) (mPoints[2].y - (mPoints[2].y - mPreviousQuad.mPoints[2].y) * factor) * scale);
            path.lineTo((float) (mPoints[3].x - (mPoints[3].x - mPreviousQuad.mPoints[3].x) * factor) * scale, (float) (mPoints[3].y - (mPoints[3].y - mPreviousQuad.mPoints[3].y) * factor) * scale);
            path.lineTo((float) (mPoints[0].x - (mPoints[0].x - mPreviousQuad.mPoints[0].x) * factor) * scale, (float) (mPoints[0].y - (mPoints[0].y - mPreviousQuad.mPoints[0].y) * factor) * scale);
        }

        return mPreviousQuad != null;
    }

    public Map<Integer, PointF> toPolygonPoints() {
        PointF centerPoint = new PointF();
        for (Point point : mPoints) {
            centerPoint.x += (float) point.x / mPoints.length;
            centerPoint.y += (float) point.y / mPoints.length;
        }

        @SuppressLint("UseSparseArrays")
        Map<Integer, PointF> polygonPoints = new HashMap<>();
        for (Point point : mPoints) {
            float x = (float) point.x;
            float y = (float) point.y;
            if (x < centerPoint.x && y < centerPoint.y)
                polygonPoints.put(0, new PointF(x / mDetectionScale, y / mDetectionScale));
            else if (x > centerPoint.x && y < centerPoint.y)
                polygonPoints.put(1, new PointF(x / mDetectionScale, y / mDetectionScale));
            else if (x < centerPoint.x && y > centerPoint.y)
                polygonPoints.put(2, new PointF(x / mDetectionScale, y / mDetectionScale));
            else if (x > centerPoint.x && y > centerPoint.y)
                polygonPoints.put(3, new PointF(x / mDetectionScale, y / mDetectionScale));
        }
        return polygonPoints;
    }
}