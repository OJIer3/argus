package com.aces.it.argus.processor;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageProxy;

import com.aces.it.argus.Log;
import com.aces.it.argus.model.Quad;

import org.opencv.core.Point;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by O.Zakharov on 06.11.2020
 */
public class PreviewDocumentAnalyzer implements ImageAnalysis.Analyzer  {

    public PreviewDocumentAnalyzer (Callback callback, int frameColor) {
        this.callback = callback;
        color = frameColor;
    }

    private final Callback callback;
    private final Handler handler = new Handler();
    private Quad previousQuad = null;
    private final Matrix matrix = new Matrix();
    private long lastDetection = 0L;
    private final int color;

    private final AtomicBoolean paused = new AtomicBoolean(false);
    private final AtomicInteger previewWidth = new AtomicInteger(0);
    private final AtomicInteger previewHeight = new AtomicInteger(0);

    public void paused(boolean paused) {
        this.paused.set(paused);
    }

    public void setPreviewSizes(int width, int height) {
        previewWidth.set(width);
        previewHeight.set(height);
    }

    @Override
    public void analyze(@NonNull ImageProxy image) {
        try {
            if (!paused.get() && System.currentTimeMillis() - lastDetection >= Quad.ANIMATE_TIME ) {
                Quad quad = ImageProcessor.DEBUG ? null : ImageProcessor.detectLargestQuad(image);
                if (Quad.isValid(quad)) {
                    Quad previewQuad = mapToScreenCoordinates(quad, image);
                    previewQuad.setColor(color);
                    previewQuad.setBorderColor(color);
                    if (previousQuad != null)
                        previewQuad.setPreviousQuad(previousQuad);
                    previousQuad = previewQuad;
                    handler.post(() -> {
                        if (callback != null)
                            callback.onFrameAnalyzed(previewQuad);
                    });
                    lastDetection = System.currentTimeMillis();
                } else {
                    previousQuad = null;
                    handler.post(() -> {
                        if (callback != null)
                            callback.onFrameAnalyzed((Quad) null);
                    });
                }

                if (ImageProcessor.DEBUG) {
                    Bitmap bm = ImageProcessor.debug(image);
                    handler.post(() -> {
                        if (callback != null)
                            callback.onFrameAnalyzed(bm);
                    });
                }
            }
        } catch (Exception e) {
            Log.e(e);
        } finally {
            image.close();
        }
    }

    private Quad mapToScreenCoordinates(Quad quad, ImageProxy image) {

        int imageWidth;
        int imageHeight;

        Point[] points = new Point[4];
        int[] cornersOrder;

        double detectionScale = quad.getDetectionScale();

        matrix.reset();
        //1: Rotate coordinates
        Point[] originalPoints = quad.getPoints();
        if (image.getImageInfo().getRotationDegrees() == 90) {
            imageWidth = (int) (image.getHeight() * detectionScale);
            imageHeight = (int) (image.getWidth() * detectionScale);
            matrix.postRotate(90);
            matrix.postTranslate(imageWidth, 0);
            cornersOrder = new int[] {3, 0, 1, 2};

        } else if (image.getImageInfo().getRotationDegrees() == 180) {
            imageWidth = (int) (image.getWidth() * detectionScale);
            imageHeight = (int) (image.getHeight() * detectionScale);
            matrix.postRotate(180);
            matrix.postTranslate(imageWidth, imageHeight);
            cornersOrder = new int[] {2, 3, 0, 1};

        } else if (image.getImageInfo().getRotationDegrees() == 270) {
            imageWidth = (int) (image.getHeight() * detectionScale);
            imageHeight = (int) (image.getWidth() * detectionScale);
            matrix.postRotate(270);
            matrix.postTranslate(0, imageHeight);
            cornersOrder = new int[] {1, 2, 3, 0};

        } else {
            imageWidth = (int) (image.getWidth() * detectionScale);
            imageHeight = (int) (image.getHeight() * detectionScale);
            cornersOrder = new int[] {0, 1, 2, 3};

            points[0] = originalPoints[0];
            points[1] = originalPoints[1];
            points[2] = originalPoints[2];
            points[3] = originalPoints[3];
        }

        // 2: convert to the preview coordinates
        int previewWidth = this.previewWidth.get();
        int previewHeight = this.previewHeight.get();
        float scaleX = previewWidth / (float) imageWidth;
        float scaleY = previewHeight / (float) imageHeight;
        float dy = 0;
        float dx = 0;
        if( previewHeight / (float) previewWidth < imageHeight / (float) imageWidth) {
            scaleY = (previewWidth / (float) imageWidth);
            dy = (previewHeight - scaleY * imageHeight) / 2f;
        } else if (previewHeight / (float) previewWidth > imageHeight / (float) imageWidth) {
            scaleX = (previewHeight / (float) imageHeight);
            dx = (previewWidth - scaleX * imageWidth) / 2f;
        }
        matrix.postScale(scaleX, scaleY);
        matrix.postTranslate(dx, dy);


        //3: apply transformation
        for (int i = 0; i < points.length; i++) {
            points[i] = applyMatrix(originalPoints[cornersOrder[i]], matrix);
        }

        return new Quad(quad.getContour(), points);
    }

    private Point applyMatrix (Point point, Matrix mat) {
        float[] pointCoordinates = {(float) point.x, (float) point.y};
        mat.mapPoints(pointCoordinates);
        return new Point(pointCoordinates[0], pointCoordinates[1]);
    }

    public interface Callback {
        void onFrameAnalyzed(@Nullable Quad quad);
        void onFrameAnalyzed(@Nullable Bitmap bitmap);
    }
}
