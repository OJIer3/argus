package com.aces.it.argus;

import android.net.Uri;
import android.view.WindowInsets;

import com.aces.it.argus.model.Transformations;

import java.util.ArrayList;

/**
 * Created by O.Zakharov on 13.11.2020
 */
public interface Callback {
    void onQrRead(String value);
    void onBarcodeRead(String value);
    WindowInsets getInsets();
    Uri getSaveUri ();
    boolean isShowTutorial ();
    void setShowTutorialNextTime(boolean show);
    boolean isShowDrawTutorial ();
    void setShowDrawTutorialNextTime(boolean show);
    void returnResult(ArrayList<Uri> images);
    void returnResultTransformations(Transformations transformations);
}
