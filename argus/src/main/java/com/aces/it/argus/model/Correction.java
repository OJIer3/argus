package com.aces.it.argus.model;

import android.graphics.PointF;

import java.io.Serializable;

/**
 * Created by O.Zakharov on 12.11.2020
 */
public class Correction implements Serializable {

    final public float startX;
    final public float startY;
    final public float endX;
    final public float endY;

    final public int color;

    public boolean isGrayScale = false;

    public Correction(PointF start, PointF end, int color) {
        startX = start.x;
        startY = start.y;
        endX = end.x;
        endY = end.y;
        this.color = color;
    }

    public Correction(float startX, float startY, float endX, float endY, int color) {
        this.startX = startX;
        this.startY = startY;
        this.endX = endX;
        this.endY = endY;
        this.color = color;
    }

    public Correction(float[] coordinates, int color) {
        this.startX = coordinates[0];
        this.startY = coordinates[1];
        this.endX = coordinates[2];
        this.endY = coordinates[3];
        this.color = color;
    }

    public Correction copy() {
        Correction correction = new Correction( startX, startY, endX, endY, color );
        correction.isGrayScale = isGrayScale;
        return correction;
    }
}
