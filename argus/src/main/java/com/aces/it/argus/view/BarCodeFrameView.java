package com.aces.it.argus.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.aces.it.argus.R;

/**
 * Created by O.Zakharov on 24.09.2021
 */
public class BarCodeFrameView extends View {

    private Paint mPaintBg;
    private Paint mPaintTextBg;
    private Paint mPaintFrame;
    private Paint mGradientPaint;
    private TextPaint mPaintText;

    private Path mPathWindow;
    private Path mPathBg;
    private Path mPathFrame;
    private Path mPathTextBg;

    private String mText;
    private float mTextX;
    private float mTextY;
    private float mTextBgHeight;
    private float mTextBgWidth;
    private float mTextBgCorners;

    public BarCodeFrameView(Context context) {
        super(context);
        init(context);
    }

    public BarCodeFrameView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public BarCodeFrameView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        int bgColor = ContextCompat.getColor(context, R.color.argusBlack50);

        mPaintBg = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintBg.setStyle(Paint.Style.FILL);
        mPaintBg.setColor(bgColor);

        mPaintTextBg = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintTextBg.setStyle(Paint.Style.FILL);
        mPaintTextBg.setColor(bgColor);

        mPaintFrame = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintFrame.setStyle(Paint.Style.STROKE);
        mPaintFrame.setStrokeWidth(context.getResources().getDimensionPixelSize(R.dimen.argus_bar_code_frame_width));
        mPaintFrame.setStrokeCap(Paint.Cap.ROUND);
        mPaintFrame.setStrokeJoin(Paint.Join.ROUND);
        mPaintFrame.setColor(Color.WHITE);

        mGradientPaint = new Paint();
        mGradientPaint.setDither(true);


        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        float textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 14, metrics);
        float verticalPadding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, metrics);
        float horizontalPadding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 14, metrics);

        mPaintText = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        mPaintText.setTextSize(textSize);
        mPaintText.setColor(Color.WHITE);
        mPaintText.setTextAlign(Paint.Align.CENTER);

        Rect bounds = new Rect();
        mText = context.getString(R.string.argus_bar_code_hint);
        mPaintText.getTextBounds(mText, 0, mText.length(), bounds);
        mTextBgHeight = bounds.height() + 2 * verticalPadding;
        mTextBgWidth = bounds.width() + 2 * horizontalPadding;
        mTextBgCorners = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, metrics);

        mPathBg = new Path();
        mPathWindow = new Path();
        mPathFrame = new Path();
        mPathTextBg = new Path();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        float qrWidth = 0.9f * Math.min(w, h);
        float qrHeight = 0.75f * qrWidth;
        float left = (w - qrWidth) / 2f;
        float right = left + qrWidth;
        float top = (h - qrHeight) / 2f;
        float bottom = top + qrHeight;

        float length = (qrWidth / 7f) * 0.7f;
        float r = (qrWidth / 7f) * 0.3f;

        mPathWindow.reset();
        mPathWindow.addRoundRect(left, top, right, bottom, r, r, Path.Direction.CW);

        mPathTextBg.reset();
        mPathTextBg.addRoundRect(
                w / 2f - mTextBgWidth / 2f,
                (h - bottom) / 2f + bottom,
                w / 2f + mTextBgWidth / 2f,
                (h - bottom) / 2f + bottom + mTextBgHeight,
                mTextBgCorners, mTextBgCorners, Path.Direction.CW);


        mPathBg.reset();
        mPathBg.moveTo(0, 0);
        mPathBg.rLineTo(0, h/ 2f);
        mPathBg.rLineTo(left, 0);
        mPathBg.rLineTo(0, - qrHeight / 2f + r);
        mPathBg.rQuadTo(0, -r, r, -r);
        mPathBg.rLineTo(qrWidth - 2 * r, 0);
        mPathBg.rQuadTo(r, 0, r, r);
        mPathBg.rLineTo(0, qrHeight / 2f - r);
        mPathBg.rLineTo(w - right, 0);
        mPathBg.rLineTo(0, -h/ 2f);
        mPathBg.rLineTo(0, 0);

        mPathBg.moveTo(w, h);
        mPathBg.rLineTo(0, -h/ 2f);
        mPathBg.rLineTo(-(w - right), 0);
        mPathBg.rLineTo(0, qrHeight / 2f - r);
        mPathBg.rQuadTo(0, r, -r, r);
        mPathBg.rLineTo(-(qrWidth - 2 * r), 0);
        mPathBg.rQuadTo(-r, 0, -r, -r);
        mPathBg.rLineTo(0, - qrHeight / 2f + r);
        mPathBg.rLineTo(-left, 0);
        mPathBg.rLineTo(0, h/ 2f);
        mPathBg.rLineTo(w, 0);

        mPathFrame.reset();
        mPathFrame.moveTo(left, top + length + r);
        mPathFrame.rLineTo(0, -length);
        mPathFrame.rQuadTo(0, -r, r, -r);
        mPathFrame.rLineTo(length, 0);

        mPathFrame.moveTo(right - length - r, top);
        mPathFrame.rLineTo(length, 0);
        mPathFrame.rQuadTo(r, 0, r, r);
        mPathFrame.rLineTo(0, length);

        mPathFrame.moveTo(right, bottom - length - r);
        mPathFrame.rLineTo(0, length);
        mPathFrame.rQuadTo(0, r, -r, r);
        mPathFrame.rLineTo(-length, 0);

        mPathFrame.moveTo(left + length + r, bottom);
        mPathFrame.rLineTo(-length, 0);
        mPathFrame.rQuadTo(-r, 0, -r, -r);
        mPathFrame.rLineTo(0, -length);

        mTextX = w / 2f;
        mTextY = (h - bottom) / 2f + bottom + 0.65f * mTextBgHeight;

        LinearGradient gradient = new LinearGradient (
            left, top, left, bottom,
            new int[] {
                    Color.parseColor("#00ffffff"),
                    Color.parseColor("#00ffffff"),
                    Color.parseColor("#80ffffff"),
                    Color.parseColor("#00ffffff")
            },
            new float[] {
                    0f,
                    0.4999f,
                    0.5f,
                    1f
            },
            Shader.TileMode.CLAMP
        );
        mGradientPaint.setShader(gradient);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawPath(mPathBg, mPaintBg);
        canvas.drawPath(mPathWindow, mGradientPaint);
        canvas.drawPath(mPathFrame, mPaintFrame);
        //canvas.drawPath(mPathTextBg, mPaintTextBg);
        canvas.drawText(mText, mTextX, mTextY, mPaintText);
    }
}
