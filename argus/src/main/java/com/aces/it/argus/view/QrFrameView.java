package com.aces.it.argus.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.aces.it.argus.R;

/**
 * Created by O.Zakharov on 26.02.2020
 */
public class QrFrameView extends View {

    private Paint mPaintBg;
    private Paint mPaintFrame;
    private TextPaint mPaintText;

    private Path mPathBg;
    private Path mPathFrame;
    private Path mPathTextBg;

    private String mText;
    private float mTextX;
    private float mTextY;
    private float mTextBgHeight;
    private float mTextBgWidth;
    private float mTextBgCorners;
    private float mTextBgPaddingTop;

    public QrFrameView(Context context) {
        super(context);
        init(context);
    }

    public QrFrameView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public QrFrameView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        mPaintBg = new Paint();
        mPaintBg.setStyle(Paint.Style.FILL);
        mPaintBg.setColor(ContextCompat.getColor(context, R.color.argusBlack50));

        mPaintFrame = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintFrame.setStyle(Paint.Style.STROKE);
        mPaintFrame.setStrokeWidth(context.getResources().getDimensionPixelSize(R.dimen.argus_qr_frame_width));
        mPaintFrame.setStrokeCap(Paint.Cap.ROUND);
        mPaintFrame.setStrokeJoin(Paint.Join.ROUND);
        mPaintFrame.setColor(Color.WHITE);

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        float textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 14, metrics);
        float verticalPadding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, metrics);
        float horizontalPadding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 14, metrics);

        mPaintText = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        mPaintText.setTextSize(textSize);
        mPaintText.setColor(Color.WHITE);
        mPaintText.setTextAlign(Paint.Align.CENTER);

        Rect bounds = new Rect();
        mText = context.getString(R.string.argus_qr_hint);
        mPaintText.getTextBounds(mText, 0, mText.length(), bounds);
        mTextBgHeight = bounds.height() + 2 * verticalPadding;
        mTextBgWidth = bounds.width() + 2 * horizontalPadding;
        mTextBgCorners = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, metrics);
        mTextBgPaddingTop = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, metrics);

        mPathBg = new Path();
        mPathFrame = new Path();
        mPathTextBg = new Path();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        float qrWidth = 0.7f * Math.min(w, h);
        float left = (w - qrWidth) / 2f;
        float right = left + qrWidth;
        float top = (h - qrWidth) / 2f;
        float bottom = top + qrWidth;

        float length = qrWidth / 5f;
        float strokePad = mPaintFrame.getStrokeWidth() / 2f;

        mPathBg.reset();
        mPathBg.addRect(0f, 0f, w, top, Path.Direction.CW);
        mPathBg.addRect(right, top, w, bottom, Path.Direction.CW);
        mPathBg.addRect(0, bottom, w, h, Path.Direction.CW);
        mPathBg.addRect(0, top, left, bottom, Path.Direction.CW);

        mPathTextBg.reset();
        mPathTextBg.addRoundRect(
                w / 2f - mTextBgWidth / 2f,
                bottom + mTextBgPaddingTop,
                w / 2f + mTextBgWidth / 2f,
                bottom + mTextBgPaddingTop + mTextBgHeight,
                mTextBgCorners, mTextBgCorners, Path.Direction.CW);

        mPathFrame.reset();
        mPathFrame.moveTo(left - strokePad, top + length);
        mPathFrame.rLineTo(0, -length - strokePad);
        mPathFrame.rLineTo(length + strokePad, 0);

        mPathFrame.moveTo(right - length, top - strokePad);
        mPathFrame.rLineTo(length + strokePad, 0);
        mPathFrame.rLineTo(0, length + strokePad);

        mPathFrame.moveTo(right + strokePad, bottom - length);
        mPathFrame.rLineTo(0, length + strokePad);
        mPathFrame.rLineTo(-length - strokePad, 0);

        mPathFrame.moveTo(left + length, bottom + strokePad);
        mPathFrame.rLineTo(-length - strokePad, 0);
        mPathFrame.rLineTo(0, -length - strokePad);

        mTextX = w / 2f;
        mTextY = bottom + mTextBgPaddingTop + 0.65f * mTextBgHeight;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawPath(mPathBg, mPaintBg);
        canvas.drawPath(mPathFrame, mPaintFrame);
        canvas.drawPath(mPathTextBg, mPaintBg);
        canvas.drawText(mText, mTextX, mTextY, mPaintText);
    }
}
