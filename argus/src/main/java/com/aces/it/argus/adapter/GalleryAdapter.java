package com.aces.it.argus.adapter;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aces.it.argus.R;
import com.aces.it.argus.model.Transformations;
import com.aces.it.argus.view.GalleryImageView;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.ImageViewState;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Created by O.Zakharov on 09.11.2020
 */
public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.Holder> {

    private HashMap<Uri, ImageViewState> states = new HashMap<>();
    private ArrayList<Transformations> items = new ArrayList<>();
    private FullScreenListener fullScreenListener;

    public void setFullScreenListener(FullScreenListener listener) {
        fullScreenListener = listener;
    }

    public void setItems (Collection<Transformations> uris) {
        items.clear();
        if (uris != null && !uris.isEmpty())
            items.addAll(uris);
        notifyDataSetChanged();
    }

    public void deleteItem (int position) {
        if (position >= 0 && position < getItemCount()) {
            items.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void updateItem (Transformations transformations) {
        int index = -1;
        for (int i = 0; i < getItemCount(); i++) {
            if (items.get(i).getUri() == transformations.getUri()) {
                index = i;
                break;
            }
        }

        if (index >= 0) {
            items.remove(index);
            items.add(index, transformations);
            notifyItemChanged(index);
        } else {
            items.add(transformations);
            notifyItemInserted(items.size() - 1);
        }
    }

    public void insertItem (Transformations transformations) {
        items.add(transformations);
        notifyItemInserted(items.size() - 1);
    }

    public void replaceItem (Transformations old, Transformations newTransformation) {
        int index = -1;
        for (int i = 0; i < getItemCount(); i++) {
            if (items.get(i).getUri() == old.getUri()) {
                index = i;
                break;
            }
        }

        if (index >= 0) {
            items.remove(index);
            items.add(index, newTransformation);
            notifyItemChanged(index);
        } else {
            items.add(newTransformation);
            notifyItemInserted(items.size() - 1);
        }
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.argus_li_gallery_item, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        if (items.size() > position)
            holder.bind(items.get(position), false);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position, @NonNull List<Object> payloads) {
        if (!payloads.isEmpty()) {
            Object o = payloads.get(0);
            if (o instanceof Boolean) {
                holder.bind(items.get(position), (Boolean) o);
                return;
            }
        }
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class Holder extends RecyclerView.ViewHolder {

        private GalleryImageView image;
        private Uri previousUri = null;

        public Holder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);

            View.OnClickListener onClick = v -> {
                if (fullScreenListener != null)
                    fullScreenListener.toggleFullscreen();
            };

            itemView.setOnClickListener(onClick);
            image.setOnClickListener(onClick);
            image.setDrawingEnabled(false);
        }

        void bind (Transformations transformations, boolean onlyTransform) {
            if (!onlyTransform) {
                if (previousUri != null)
                    states.put(previousUri, image.getState());
                previousUri = transformations.getUri();
                image.setImage(ImageSource.uri(previousUri), states.get(previousUri));
            }

            image.setTransformations(transformations);
        }
    }

    public interface FullScreenListener {
        void toggleFullscreen();
    }
}
