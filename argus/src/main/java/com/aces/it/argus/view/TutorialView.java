package com.aces.it.argus.view;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.util.Pair;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.aces.it.argus.R;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.List;

/**
 * Created by O.Zakharov on 10.11.2020
 */
public class TutorialView extends FrameLayout {

    private static final int BALLOON_HORIZONTAL_PADDING = 16;
    private static final int BALLOON_VERTICAL_PADDING = 16;
    private static final int BALLOON_CORNERS_R = 6;
    private static final int BALLOON_ARROW_SIZE = 10;

    private Paint circlePaint = new Paint (Paint.ANTI_ALIAS_FLAG);
    private Paint backgroundPaint = new Paint (Paint.ANTI_ALIAS_FLAG);
    private Paint balloonPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private float highlightX = 0f;
    private float highlightY = 0f;
    private float highlightR = 0f;
    private float dp1;

    @SuppressWarnings("FieldCanBeLocal")
    private int animateTime = 150;

    private List<Pair<View, String>> anchorViews = null;
    private View anchorView;
    private boolean invalidated = true;
    private ValueAnimator animator = null;

    private Path balloon = new Path();
    private int balloonColor;
    private int balloonShadowColor;
    private BlurMaskFilter balloonShadowFilter;

    private LinearLayout hintLayout;
    private ViewPager2 viewPager;
    private HintsAdapter adapter = new HintsAdapter();
    private int hintLayoutHeight = 0;

    private Listener listener;

    public TutorialView(Context context) {
        this(context, null);
    }

    public TutorialView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TutorialView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setLayerType(LAYER_TYPE_HARDWARE, null);
        setWillNotDraw(false);

        circlePaint.setColor(ContextCompat.getColor(context, R.color.argusWhite30));
        circlePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

        backgroundPaint.setColor(ContextCompat.getColor(context, R.color.argusBlack60));

        dp1 = context.getResources().getDisplayMetrics().density;

        balloonShadowColor = ContextCompat.getColor(context, R.color.argusBlack50);
        balloonColor = ContextCompat.getColor(context, R.color.argusBalloonBg);
        balloonShadowFilter = new BlurMaskFilter(10 * dp1, BlurMaskFilter.Blur.NORMAL);

        createHintLayout();

        setOnClickListener(v -> {
            if (viewPager.getCurrentItem() < adapter.getItemCount() - 1)
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
            else {
                if (listener != null)
                    listener.onTutorialFinished();
                setVisibility(View.GONE);
            }
        });
    }

    private void createHintLayout() {
        hintLayout = new LinearLayout(getContext());
        hintLayout.setOrientation(LinearLayout.VERTICAL);
        LayoutParams hintLayoutParams = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                Gravity.BOTTOM
        );
        hintLayoutParams.setMarginStart((int) (BALLOON_HORIZONTAL_PADDING * dp1));
        hintLayoutParams.setMarginEnd((int) (BALLOON_HORIZONTAL_PADDING * dp1));
        hintLayout.setLayoutParams(hintLayoutParams);

        viewPager = new ViewPager2(getContext());
        LinearLayout.LayoutParams viewPagerParam = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        viewPagerParam.topMargin = (int) (16 * dp1);
        viewPagerParam.bottomMargin = (int) (8 * dp1);
        viewPager.setLayoutParams(viewPagerParam);
        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                if (anchorViews != null && anchorViews.size() > position)
                    setAnchorView(anchorViews.get(position).first);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }
        });
        viewPager.setAdapter(adapter);

        TabLayout dots = (TabLayout) LayoutInflater.from(getContext())
                .inflate(R.layout.argus_layout_tutorial_dots, hintLayout, false);
        new TabLayoutMediator(dots, viewPager, (tab, position) -> {}).attach();

        hintLayout.addView(viewPager);
        hintLayout.addView(dots);
        addView(hintLayout);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setAnchorViews(List<Pair<View, String>> views) {
        hintLayoutHeight = 0;
        anchorViews = views;
        adapter.notifyDataSetChanged();
        viewPager.setCurrentItem(0, true);
        setAnchorView(views != null && !views.isEmpty() ? views.get(0).first : null);
    }

    private void setAnchorView(View view) {
        anchorView = view;
        calculateHighlightPosition();
    }

    private void calculateHighlightPosition() {
        if (hintLayoutHeight == 0)
            calculateHintLayoutHeight();

        if (anchorView != null && hintLayoutHeight != 0) {
            int anchorWidth = anchorView.getWidth();
            int anchorHeight = anchorView.getHeight();
            if (anchorWidth == 0 || anchorHeight ==0) {
                highlightX = 0f;
                highlightY = 0f;
                highlightR = 0f;
                invalidated = true;
                calculateBalloon();
                positioningHintLayout();
                invalidate();
                return;
            }

            invalidated = false;
            int parentX = 0;
            int parentY = 0;
            ViewParent overlayParent = getParent();
            ViewParent parent = anchorView.getParent();

            while (overlayParent != null && parent != null && parent != overlayParent) {
                parentX += ((View) parent).getX();
                parentY += ((View) parent).getY();
                parent = parent.getParent();
            }

            final float anchorX = parentX + anchorView.getX() + anchorView.getWidth() / 2f;
            final float anchorY = parentY + anchorView.getY() + anchorView.getHeight() / 2f;
            final float anchorR = Math.max(anchorWidth / 2f, anchorHeight / 2f);
            if (animator != null)
                animator.cancel();

            if (highlightR == 0) {
                highlightR = anchorR;
                highlightX = anchorX;
                highlightY = anchorY;
                calculateBalloon();
                positioningHintLayout();
            } else {
                final float deltaR = anchorR - highlightR;
                final float deltaX = anchorX - highlightX;
                final float deltaY = anchorY - highlightY;
                animator = ValueAnimator.ofFloat(1f, 0f).setDuration(animateTime);
                animator.addUpdateListener(animation -> {
                    float fraction = (float) animation.getAnimatedValue();
                    highlightX = anchorX - fraction * deltaX;
                    highlightY = anchorY - fraction * deltaY;
                    highlightR = anchorR - fraction * deltaR;
                    calculateBalloon();
                    positioningHintLayout();
                    invalidate();
                });
                animator.start();
            }
        } else {
            invalidated = true;
        }

        invalidate();
    }

    private void calculateBalloon() {
        balloon.reset();
        if (highlightR == 0)
            return;

        float balloonBottom = highlightY - highlightR - BALLOON_VERTICAL_PADDING * dp1;
        float balloonArrowSize = BALLOON_ARROW_SIZE * dp1;

        balloon.addRoundRect(
                BALLOON_HORIZONTAL_PADDING * dp1,
                balloonBottom - hintLayoutHeight,
                getWidth() - BALLOON_HORIZONTAL_PADDING * dp1,
                balloonBottom,
                BALLOON_CORNERS_R * dp1,
                BALLOON_CORNERS_R *dp1,
            Path.Direction.CW
        );

        balloon.moveTo(highlightX, balloonBottom + balloonArrowSize);
        balloon.lineTo(highlightX + balloonArrowSize, balloonBottom);
        balloon.lineTo(highlightX - balloonArrowSize, balloonBottom);
        balloon.close();
    }

    private void positioningHintLayout() {
        int balloonBottom = (int)(highlightY - highlightR - BALLOON_VERTICAL_PADDING * dp1);
        if (hintLayout.getY() + hintLayout.getHeight() != balloonBottom)
            hintLayout.setY(balloonBottom - hintLayout.getHeight());
    }

    private void calculateHintLayoutHeight() {
        int hintLayoutWidth = hintLayout.getWidth();
        if (hintLayoutWidth != 0 && anchorViews != null && !anchorViews.isEmpty()) {
            TextView textView = getHintTextView(true);
            int wSpec = MeasureSpec.makeMeasureSpec(hintLayoutWidth, MeasureSpec.EXACTLY);
            int hSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
            int maxHeight = 0;
            for (Pair<View, String> pair: anchorViews) {
                textView.setText(pair.second);
                textView.measure(wSpec, hSpec);
                int measuredHeight = textView.getMeasuredHeight();
                maxHeight = Math.max(maxHeight, measuredHeight);
            }
            viewPager.getLayoutParams().height = maxHeight;

            hintLayout.measure(wSpec, hSpec);
            hintLayoutHeight = hintLayout.getMeasuredHeight();
            hintLayout.getLayoutParams().height = hintLayoutHeight;

            layout(0, 0, getWidth(), getHeight());
        } else {
            hintLayoutHeight = 0;
        }
    }

    private TextView getHintTextView(boolean forMeasure) {
        TextView textView = new TextView(getContext());
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f);
        ViewGroup.LayoutParams params = forMeasure ?
                new ViewGroup.LayoutParams (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT) :
                new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        textView.setLayoutParams(params);
        textView.setPadding((int) (16 * dp1), 0, (int) (16 * dp1), 0);
        return textView;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(0f, 0f, getWidth(), getHeight(), backgroundPaint);

        if (invalidated)
            calculateHighlightPosition();
        else {
            canvas.drawCircle(highlightX, highlightY, highlightR, circlePaint);
            drawBalloon(canvas);
        }
    }

    private void drawBalloon(Canvas canvas) {
        // Create paint for shadow
        balloonPaint.setColor(balloonShadowColor);
        balloonPaint.setMaskFilter(balloonShadowFilter);

        // Draw shadow before drawing object
        canvas.save();
        canvas.translate(0f, 10f);
        canvas.drawPath(balloon, balloonPaint);
        canvas.restore();

        // Create paint for main object
        balloonPaint.setColor(balloonColor);
        balloonPaint.setMaskFilter(null);

        // Draw main object
        canvas.drawPath(balloon, balloonPaint);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        calculateHighlightPosition();
    }

    private class HintsAdapter extends RecyclerView.Adapter<HintsAdapter.Holder> {

        @NonNull
        @Override
        public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new Holder( getHintTextView(false) );
        }

        @Override
        public void onBindViewHolder(@NonNull Holder holder, int position) {
            if (anchorViews != null && anchorViews.size() > position) {
                holder.bind(anchorViews.get(position).second);
            }
        }

        @Override
        public int getItemCount() {
            return anchorViews != null ? anchorViews.size() : 0;
        }

        class Holder extends RecyclerView.ViewHolder {

            private TextView textView;

            public Holder(@NonNull View itemView) {
                super(itemView);
                textView = (TextView) itemView;
            }

            private void bind (String string) {
                textView.setText(string);
            }
        }
    }

    public interface Listener {
        void onTutorialFinished();
    }
}
