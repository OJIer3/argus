package com.aces.it.argus.fragment;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.core.graphics.Insets;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.WindowInsetsControllerCompat;
import androidx.viewpager2.widget.ViewPager2;

import com.aces.it.argus.Argus;
import com.aces.it.argus.R;
import com.aces.it.argus.adapter.GalleryAdapter;
import com.aces.it.argus.fragment.dialog.ConfirmationDialog;
import com.aces.it.argus.model.Transformations;
import com.aces.it.argus.processor.ImageProcessor;
import com.aces.it.argus.utils.FileUtils;
import com.aces.it.argus.view.TutorialView;

import java.util.ArrayList;
import java.util.Arrays;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by O.Zakharov on 19.02.2020
 */
public class GalleryFragment extends BaseFragment implements GalleryAdapter.FullScreenListener {

    public static final int REQUEST_DELETE = 9001;
    private static final int REQUEST_RE_PHOTO = 9000;
    private static final int REQUEST_CORRECT = 9002;
    private static final int REQUEST_CROP = 9003;

    public static GalleryFragment newInstance(
        ArrayList<Transformations> transformations, int selectedPos, boolean canRePhoto, int maxPhotos
    ) {
        Bundle args = new Bundle();
        args.putSerializable(Argus.ITEMS, transformations);
        args.putInt(Argus.SELECTED_POSITION, selectedPos);
        args.putBoolean(Argus.CAN_RE_PHOTO, canRePhoto);
        args.putInt(Argus.MAX_PHOTOS, maxPhotos);
        GalleryFragment fragment = new GalleryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private MotionLayout content;
    private FrameLayout topLayout;
    private LinearLayout bottomLayout;
    private ViewPager2 viewPager;
    private TextView titleView;
    private TutorialView tutorialView;
    private View buttonDraw;
    private View buttonCrop;
    private View buttonBW;
    private View buttonRotate;
    private View buttonDone;
    private View progress;

    private boolean canRePhoto = false;
    private int selectedPos = 0;
    private ArrayList<Transformations> images = new ArrayList<>();
    private int maxPhotos = 0;

    private Disposable saving;
    private final GalleryAdapter adapter = new GalleryAdapter();

    private WindowInsetsControllerCompat insetsController;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            //noinspection unchecked
            ArrayList<Transformations> transformations = (ArrayList<Transformations>) args.getSerializable(Argus.ITEMS);
            if (transformations != null && !transformations.isEmpty())
                images.addAll(transformations);
            selectedPos = args.getInt(Argus.SELECTED_POSITION, 0);
            canRePhoto = args.getBoolean(Argus.CAN_RE_PHOTO, false);
            maxPhotos = args.getInt(Argus.MAX_PHOTOS, 0);
        }

        if (savedInstanceState != null) {
            //noinspection unchecked
            images = (ArrayList<Transformations>) savedInstanceState.getSerializable(Argus.ITEMS);
            selectedPos = savedInstanceState.getInt(Argus.SELECTED_POSITION, 0);
        }

        adapter.setFullScreenListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.argus_fmt_gallery, container, false);
        content = (MotionLayout) view;
        topLayout = view.findViewById(R.id.topLayout);
        bottomLayout = view.findViewById(R.id.bottomLayout);
        viewPager = view.findViewById(R.id.viewPager);
        titleView = view.findViewById(R.id.titleView);
        tutorialView = view.findViewById(R.id.tutorialView);
        buttonDraw = view.findViewById(R.id.btnDraw);
        buttonCrop = view.findViewById(R.id.btnCrop);
        buttonBW = view.findViewById(R.id.btnBW);
        buttonRotate = view.findViewById(R.id.btnRotate);
        buttonDone = view.findViewById(R.id.btn_done);
        progress = view.findViewById(R.id.progress);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Window window = requireActivity().getWindow();
        insetsController = new WindowInsetsControllerCompat(window, window.getDecorView());
        insetsController.setSystemBarsBehavior(
            WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        );

        if (content.getCurrentState()  == R.id.exit)
            showSystemUI();
        else
            hideSystemUI();

        view.setOnApplyWindowInsetsListener((v, insets) -> {
            handleInsets(insets);
            return insets;
        });

        handleInsets(callback.getInsets());

        bindTitle();
        bindViewPager();
        bindDoneButton();

        if (canRePhoto) {
            View rePhotoButton = view.findViewById(R.id.btn_re_photo);
            rePhotoButton.setVisibility(View.VISIBLE);
            rePhotoButton.setOnClickListener(v -> {
                if (selectedPos < images.size()) {
                    Uri rePhotoUri = images.get(selectedPos).getUri();
                    DocumentScanFragment fragment = DocumentScanFragment.rePhoto(rePhotoUri, maxPhotos);
                    fragment.setTargetFragment(this, REQUEST_RE_PHOTO);
                    replaceFragment(fragment, true);
                }
            });
        }
        view.findViewById(R.id.btnClose).setOnClickListener(v -> onBackPressed());
        view.findViewById(R.id.btnDelete).setOnClickListener(v ->
            ConfirmationDialog.show(this, REQUEST_DELETE, selectedPos)
        );

        if (callback.isShowTutorial()) {
            tutorialView.setListener(() -> callback.setShowTutorialNextTime(false));
            tutorialView.setAnchorViews(Arrays.asList (
                    Pair.create(buttonDraw, getString(R.string.argus_tutorial_draw_button)),
                    Pair.create(buttonCrop, getString(R.string.argus_tutorial_crop_button)),
                    Pair.create(buttonBW, getString(R.string.argus_tutorial_bw_button)),
                    Pair.create(buttonRotate, getString(R.string.argus_tutorial_rotate_button))
            ));
            tutorialView.setVisibility(View.VISIBLE);
        } else {
            tutorialView.setVisibility(View.GONE);
        }

        buttonRotate.setOnClickListener(v -> {
            if (selectedPos >= 0 && selectedPos < images.size()) {
                images.get(selectedPos).rotate();
                adapter.notifyItemChanged(selectedPos, Boolean.TRUE);
            }
        });

        buttonBW.setOnClickListener(v -> {
            if (selectedPos >= 0 && selectedPos < images.size()) {
                images.get(selectedPos).toggleBW();
                adapter.notifyItemChanged(selectedPos, Boolean.TRUE);
            }
        });

        buttonDraw.setOnClickListener(v -> {
            if (selectedPos >= 0 && selectedPos < images.size()) {
                DocumentDrawFragment fragment =
                        DocumentDrawFragment.newInstance(images.get(selectedPos));
                fragment.setTargetFragment(this, REQUEST_CORRECT);
                replaceFragment(fragment, true);
            }
        });

        buttonCrop.setOnClickListener(v -> {
            if (selectedPos >= 0 && selectedPos < images.size()) {
                DocumentCropFragment fragment =
                        DocumentCropFragment.newInstance(images.get(selectedPos));
                fragment.setTargetFragment(this, REQUEST_CROP);
                replaceFragment(fragment, true);
            }
        });

        progress.setOnClickListener(v -> {});
    }

    private void handleInsets(WindowInsets windowInsets) {
        if (windowInsets != null) {
            Insets insets = WindowInsetsCompat.toWindowInsetsCompat(windowInsets)
                .getInsets(WindowInsetsCompat.Type.systemBars());

            content.setPadding(0, 0, 0, 0);
            topLayout.setPadding(
                0, Math.max(topLayout.getPaddingTop(), insets.top), 0, 0
            );
            bottomLayout.setPadding(
                0, 0, 0, Math.max(bottomLayout.getPaddingBottom(), insets.bottom)
            );

            topLayout.requestLayout();
            bottomLayout.requestLayout();
            content.requestLayout();
        }
    }

    private void bindViewPager() {
        adapter.setItems(images);
        viewPager.setOffscreenPageLimit(1);
        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                bindTitle();
                selectedPos = position;
            }
        });
        viewPager.setAdapter(adapter);
    }

    private void bindTitle() {
        titleView.setText( images.isEmpty() ? "-" :
            getString (
                R.string.argus_i_from_n_pattern,
                viewPager.getCurrentItem() + 1,
                images.size()
            )
        );
    }

    private void bindDoneButton() {
        buttonDone.setOnClickListener(v -> {
            if (saving != null && !saving.isDisposed())
                return;

            progress.setVisibility(View.VISIBLE);

            final ContentResolver resolver = getActivity() != null ? getActivity().getContentResolver() : null;
            final Uri savePath = callback.getSaveUri();

            saving = Single.fromCallable(() -> {
                ArrayList<Uri> savedImages = new ArrayList<>();
                if (resolver != null) {
                    for(Transformations transformations: images) {
                        Uri uri = transformations.getUri();
                        if (transformations.hasChanges()) {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(resolver, uri);
                            Bitmap save = ImageProcessor.applyTransformations(bitmap, transformations);

                            if (!bitmap.isRecycled())
                                bitmap.recycle();

                            savedImages.add(FileUtils.saveBitmap(save, savePath));
                            save.recycle();

                            FileUtils.deleteFileIfInDirectory(uri, savePath);
                        } else {
                            savedImages.add (uri);
                        }
                    }
                }
                return savedImages;
            })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            uris -> {
                                progress.setVisibility(View.GONE);
                                callback.returnResult(uris);
                            },
                            error -> {
                                error.printStackTrace();
                                progress.setVisibility(View.GONE);
                            });
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        if (selectedPos < adapter.getItemCount())
            viewPager.setCurrentItem(selectedPos, false);
        else if (adapter.getItemCount() > 0)
            viewPager.setCurrentItem(adapter.getItemCount() - 1, false);
    }

    @Override
    public void onStop() {
        selectedPos = viewPager.getCurrentItem();
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable(Argus.ITEMS, images);
        outState.putInt(Argus.SELECTED_POSITION, selectedPos);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_RE_PHOTO: {
                    if (data != null) {
                        Uri rePhoto = data.getParcelableExtra(Argus.RE_PHOTO_URI);
                        Uri newPhoto = data.getParcelableExtra(Argus.NEW_PHOTO_URI);
                        int index = images.size() - 1;
                        for (int i = 0; i < images.size(); i++) {
                            if (images.get(i).getUri() == rePhoto) {
                                index = i;
                                break;
                            }
                        }

                        if (index >= 0) {
                            images.get(index).reset();
                            images.get(index).setUri(newPhoto);
                            adapter.notifyItemChanged(index);
                        } else {
                            Transformations transformations = new Transformations(newPhoto);
                            images.add(transformations);
                            adapter.insertItem(transformations);
                        }

                        FileUtils.deleteFile(rePhoto);
                    }
                    break;
                }

                case REQUEST_DELETE: {
                    if (data != null) {
                        int deletePos = data.getIntExtra(Argus.SELECTED_POSITION, -1);
                        if (deletePos >= 0 && deletePos < images.size()) {
                            adapter.deleteItem(deletePos);
                            Uri uri = images.remove(deletePos).getUri();
                            Uri parent = callback.getSaveUri();
                            if (parent != null && parent.getPath() != null) {
                                if (uri.getPath() != null && uri.getPath().startsWith(parent.getPath()))
                                    FileUtils.deleteFile(uri);
                            }

                            if (images.isEmpty())
                                onBackPressed();

                            bindTitle();
                        }
                    }
                    break;
                }

                case REQUEST_CORRECT: {
                    if (data != null) {
                        Transformations transformations =
                                (Transformations) data.getSerializableExtra(Argus.TRANSFORMATIONS);
                        if (transformations == null)
                            return;

                        adapter.updateItem(transformations);

                        int index = -1;
                        for (int i = 0; i < images.size(); i++) {
                            if (images.get(i).getUri() == transformations.getUri()) {
                                index = i;
                                break;
                            }
                        }

                        if (index >= 0) {
                            images.remove(index);
                            images.add(index, transformations);
                        } else {
                            images.add(transformations);
                        }
                    }
                    break;
                }

                case REQUEST_CROP: {
                    if (data != null) {
                        Transformations transformations =
                                (Transformations) data.getSerializableExtra(Argus.TRANSFORMATIONS);
                        Transformations newTransformations =
                                (Transformations) data.getSerializableExtra(Argus.CROP_TRANSFORMATIONS);
                        if (transformations == null || newTransformations == null)
                            return;

                        adapter.replaceItem(transformations, newTransformations);

                        int index = -1;
                        for (int i = 0; i < images.size(); i++) {
                            if (images.get(i).getUri() == transformations.getUri()) {
                                index = i;
                                break;
                            }
                        }

                        if (index >= 0) {
                            images.remove(index);
                            images.add(index, newTransformations);
                        } else {
                            images.add(newTransformations);
                        }
                    }
                    break;
                }
            }
        }
    }

    @Override
    public void toggleFullscreen() {
        if (content.getCurrentState() == R.id.exit) {
            hideSystemUI();
            content.transitionToState(R.id.enter);
        } else {
            showSystemUI();
            content.transitionToState(R.id.exit);
        }
    }

    @Override
    public boolean onBackPressedHandled() {
        if (progress.getVisibility() == View.VISIBLE || tutorialView.getVisibility() == View.VISIBLE)
            return true;

        if (canRePhoto && (maxPhotos == 0 || maxPhotos > images.size())) {
            replaceFragment(DocumentScanFragment.newInstance(images, maxPhotos), false);
            return true;
        }

        return false;
    }

    private void hideSystemUI() {
        insetsController.hide(WindowInsetsCompat.Type.systemBars());

        // Fix bug on old android sdks, when getting white screen on pop backstack
        // after enter and exit from fullscreen mode (androidx.core:core-ktx:1.7.0)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R)
            requireActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void showSystemUI() {
        insetsController.show(WindowInsetsCompat.Type.systemBars());
    }
}
