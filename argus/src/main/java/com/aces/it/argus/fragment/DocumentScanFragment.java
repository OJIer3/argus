package com.aces.it.argus.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.display.DisplayManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowInsets;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.camera.core.AspectRatio;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.Preview;
import androidx.camera.core.TorchState;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.Insets;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.Fade;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;

import com.aces.it.argus.Argus;
import com.aces.it.argus.Log;
import com.aces.it.argus.R;
import com.aces.it.argus.adapter.CapturedPreviewAdapter;
import com.aces.it.argus.model.Quad;
import com.aces.it.argus.model.Transformations;
import com.aces.it.argus.processor.ImageProcessor;
import com.aces.it.argus.processor.PreviewDocumentAnalyzer;
import com.aces.it.argus.utils.FileUtils;
import com.aces.it.argus.view.CameraScanOverlayView;
import com.aces.it.argus.view.HorizontalMarginItemDecoration;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.google.common.util.concurrent.ListenableFuture;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by O.Zakharov on 19.02.2020
 */
public class DocumentScanFragment extends BaseFragment implements CapturedPreviewAdapter.Listener {

    public static DocumentScanFragment newInstance(
        ArrayList<Transformations> transformations, int maxPhotos
    ) {
        Bundle args = new Bundle();
        args.putInt(Argus.MAX_PHOTOS, maxPhotos);
        if (transformations != null)
            args.putSerializable(Argus.ITEMS, transformations);
        DocumentScanFragment fragment = new DocumentScanFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static DocumentScanFragment rePhoto(
        Uri uri, int maxPhotos
    ) {
        Bundle args = new Bundle();
        args.putInt(Argus.MAX_PHOTOS, maxPhotos);
        if (uri != null)
            args.putParcelable (Argus.RE_PHOTO_URI, uri);
        DocumentScanFragment fragment = new DocumentScanFragment();
        fragment.setArguments(args);
        return fragment;
    }


    final private static float PREVIEW_ITEM_SIZE = 36f;

    private PreviewView mPreviewView;
    private CameraScanOverlayView mOverlay;
    private ImageView mFlashButton;
    private View mShutterButton;
    private View mCloseButton;
    private View mUndoButton;
    private View mProgress;
    private View mDoneButton;
    private ConstraintLayout mTopLayout;
    private ConstraintLayout mBottomLayout;
    private ImageView mPlaceHolder;
    private ImageView mCapturedView;
    private RecyclerView mCapturedViews;
    private TextView mHintView;
    private ConstraintLayout mBarsLayout;
    private ImageView mDebug;
    private SeekBar mParameter1;
    private SeekBar mParameter2;
    private SeekBar mParameter3;
    private TextView mParameter1Label;
    private TextView mParameter2Label;
    private TextView mParameter3Label;

    private Disposable mDisposable;

    private Preview cameraPreview;
    private ImageAnalysis edgeAnalyzer;
    private ImageCapture imageCapture;
    private ExecutorService cameraExecutor;
    private LiveData<Integer> torchState = null;

    final private Handler handler = new Handler();
    final private Runnable hideHint = () -> showHint(0);

    private PreviewDocumentAnalyzer previewAnalyzer;

    final private CapturedPreviewAdapter adapter = new CapturedPreviewAdapter();

    final private DisplayManager.DisplayListener displayListener = new DisplayManager.DisplayListener() {
        @Override
        public void onDisplayAdded(int displayId) { }

        @Override
        public void onDisplayRemoved(int displayId) { }

        @Override
        public void onDisplayChanged(int displayId) {
            if (cameraPreview != null && displayId == mPreviewView.getDisplay().getDisplayId()) {
                int rotation = mPreviewView.getDisplay().getRotation();
                Log.d("Rotation changed: " + rotation);
                if (edgeAnalyzer != null)
                    edgeAnalyzer.setTargetRotation(rotation);
                if (imageCapture != null)
                    imageCapture.setTargetRotation(rotation);
            }
        }
    };

    private boolean mProgressVisible = false;

    private ArrayList<Transformations> images = new ArrayList<>();
    private Uri rePhotoUri = null;
    private int maxPhotos = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            //noinspection unchecked
            ArrayList<Transformations> transformations = (ArrayList<Transformations>) args.getSerializable(Argus.ITEMS);
            if (transformations != null && !transformations.isEmpty())
                images.addAll(transformations);
            rePhotoUri = args.getParcelable(Argus.RE_PHOTO_URI);

            maxPhotos = args.getInt(Argus.MAX_PHOTOS, 0);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.argus_fmt_document_scan, container, false);
        mPreviewView = view.findViewById(R.id.preview_view);
        mCloseButton = view.findViewById(R.id.btn_close);
        mShutterButton = view.findViewById(R.id.btn_take_picture);
        mFlashButton = view.findViewById(R.id.btn_flash);
        mTopLayout = view.findViewById(R.id.top_layout);
        mBottomLayout = view.findViewById(R.id.bottom_layout);
        mProgress = view.findViewById(R.id.progress);
        mOverlay = view.findViewById(R.id.overlayQuad);
        mCapturedView = view.findViewById(R.id.captured);
        mCapturedViews = view.findViewById(R.id.recycler);
        mPlaceHolder = view.findViewById(R.id.placeholder);
        mUndoButton = view.findViewById(R.id.btn_undo);
        mHintView = view.findViewById(R.id.scan_hint);
        mBarsLayout = view.findViewById(R.id.bars_layout);
        mDoneButton = view.findViewById(R.id.btn_done);
        mDebug = view.findViewById(R.id.debug);
        mParameter1 = view.findViewById(R.id.parameter1);
        mParameter2 = view.findViewById(R.id.parameter2);
        mParameter3 = view.findViewById(R.id.parameter3);
        mParameter1Label = view.findViewById(R.id.parameter1Label);
        mParameter2Label = view.findViewById(R.id.parameter2Label);
        mParameter3Label = view.findViewById(R.id.parameter3Label);

        previewAnalyzer = new PreviewDocumentAnalyzer(
                new PreviewDocumentAnalyzer.Callback() {
                    @Override
                    public void onFrameAnalyzed(@Nullable Quad quad) {
                        mOverlay.setQuad(quad);
                    }

                    @Override
                    public void onFrameAnalyzed(@Nullable Bitmap bitmap) {
                        mDebug.setImageBitmap(bitmap);
                        mDebug.setPivotX(mDebug.getWidth() / 2f);
                        mDebug.setPivotY(mDebug.getHeight() / 2f);
                    }
                },
                ContextCompat.getColor(view.getContext(), R.color.argusPreviewAnalyzerFrame)
        );

        bindDebugParameters();

        view.setOnApplyWindowInsetsListener((v, insets) -> {
            handleInsets(insets);
            return insets;
        });

        Activity activity = getActivity();
        if (activity != null ) {
            DisplayManager manager = (DisplayManager) activity.getSystemService(Context.DISPLAY_SERVICE);
            if (manager != null)
                manager.registerDisplayListener(displayListener, null);
        }

        handleInsets(callback.getInsets());

        if (savedInstanceState != null)
            //noinspection unchecked
            images = (ArrayList<Transformations>) savedInstanceState.getSerializable(Argus.ITEMS);

        while (maxPhotos > 0 && images.size() >= maxPhotos) {
            images.remove(images.size() - 1);
        }

        return view;
    }

    private void bindDebugParameters() {
        if (ImageProcessor.SHOW_SEEKBARS) {
            mDebug.setVisibility(View.VISIBLE);
            mParameter1.setVisibility(View.VISIBLE);
            mParameter2.setVisibility(View.VISIBLE);
            mParameter3.setVisibility(View.VISIBLE);
            mParameter1Label.setVisibility(View.VISIBLE);
            mParameter2Label.setVisibility(View.VISIBLE);
            mParameter3Label.setVisibility(View.VISIBLE);
            mParameter1.setMax(ImageProcessor.PARAMETER1MAX);
            mParameter1.setProgress(ImageProcessor.PARAMETER1);
            mParameter2.setMax(ImageProcessor.PARAMETER2MAX);
            mParameter2.setProgress(ImageProcessor.PARAMETER2);
            mParameter3.setMax(ImageProcessor.PARAMETER3MAX);
            mParameter3.setProgress(ImageProcessor.PARAMETER3);
            mParameter1Label.setText(String.valueOf(ImageProcessor.PARAMETER1));
            mParameter2Label.setText(String.valueOf(ImageProcessor.PARAMETER2));
            mParameter3Label.setText(String.valueOf(ImageProcessor.PARAMETER3));

            SeekBar.OnSeekBarChangeListener listener = new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (seekBar.getId() == mParameter1.getId()) {
                        ImageProcessor.PARAMETER1 = progress;
                        mParameter1Label.setText(String.valueOf(progress));
                    }
                    else if (seekBar.getId() == mParameter2.getId()) {
                        ImageProcessor.PARAMETER2 = progress;
                        mParameter2Label.setText(String.valueOf(progress));
                    }
                    else if (seekBar.getId() == mParameter3.getId()) {
                        ImageProcessor.PARAMETER3 = progress;
                        mParameter3Label.setText(String.valueOf(progress));
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {}

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {}
            };
            mParameter1.setOnSeekBarChangeListener(listener);
            mParameter2.setOnSeekBarChangeListener(listener);
            mParameter3.setOnSeekBarChangeListener(listener);

        } else {
            mDebug.setVisibility(View.GONE);
            mParameter1.setVisibility(View.GONE);
            mParameter2.setVisibility(View.GONE);
            mParameter3.setVisibility(View.GONE);
            mParameter1Label.setVisibility(View.GONE);
            mParameter2Label.setVisibility(View.GONE);
            mParameter3Label.setVisibility(View.GONE);
        }
    }

    private void handleInsets(WindowInsets windowInsets) {
        if (windowInsets != null) {
            Insets insets = WindowInsetsCompat.toWindowInsetsCompat(windowInsets)
                .getInsets(WindowInsetsCompat.Type.systemBars());

            mTopLayout.setPadding(
                0, Math.max(mTopLayout.getPaddingTop(), insets.top), 0, 0
            );
            mBottomLayout.setPadding(
                0, mBottomLayout.getPaddingTop(),
                0, Math.max(mBottomLayout.getPaddingBottom(), insets.bottom)
            );

            mTopLayout.requestLayout();
            mBottomLayout.requestLayout();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (hasCameraPermission())
            initCamera(view.getContext());

        bindTopLayout();
        bindUndoAndDoneButton();

        bindProgress(mProgressVisible);

        showHint(images.isEmpty() ? R.string.argus_hint_move_camera_on_document : R.string.argus_hint_move_camera_on_next_page);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable(Argus.ITEMS, images);
        super.onSaveInstanceState(outState);
    }

    private void initCamera(Context context) {
        final ListenableFuture<ProcessCameraProvider> cameraProviderFuture
                = ProcessCameraProvider.getInstance(context);
        cameraProviderFuture.addListener(() ->
            mPreviewView.post(() -> {
                try {
                    bindCamera(cameraProviderFuture.get());
                    previewAnalyzer.setPreviewSizes(mPreviewView.getWidth(), mPreviewView.getHeight());
                } catch (InterruptedException | ExecutionException e) {
                    Log.e(e);
                }
            }),
            ContextCompat.getMainExecutor(context)
        );
    }

    private void bindCamera(ProcessCameraProvider cameraProvider) {
        cameraProvider.unbindAll();

        if (cameraExecutor == null || cameraExecutor.isShutdown())
            cameraExecutor = Executors.newSingleThreadExecutor();

        initPreview();
        initEdgeAnalyzer();
        initImageCapture();

        CameraSelector cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build();

        Camera camera = cameraProvider.bindToLifecycle(
                getViewLifecycleOwner(),
                cameraSelector,
                cameraPreview, edgeAnalyzer, imageCapture);

        cameraPreview.setSurfaceProvider( mPreviewView.getSurfaceProvider());

        if (camera.getCameraInfo().hasFlashUnit()) {
            torchState = camera.getCameraInfo().getTorchState();
            torchState.removeObservers(DocumentScanFragment.this);
            torchState.observe(
                    getViewLifecycleOwner(),
                    integer -> mFlashButton.setImageResource(TorchState.ON == integer ?
                            R.drawable.ic_argus_flash_on :
                            R.drawable.ic_argus_flash_off
                    )
            );

            mFlashButton.setVisibility(View.VISIBLE);
            mFlashButton.setOnClickListener(v -> {
                if (torchState != null && torchState.getValue() != null)
                    camera.getCameraControl().enableTorch(TorchState.ON != torchState.getValue());
            });
            if (torchState != null && torchState.getValue() != null)
                mFlashButton.setImageResource(TorchState.ON == torchState.getValue() ?
                        R.drawable.ic_argus_flash_on :
                        R.drawable.ic_argus_flash_off);
        } else {
            mFlashButton.setVisibility(View.GONE);
        }
    }

    private void initPreview() {
        cameraPreview = new Preview.Builder()
                .setTargetAspectRatio(AspectRatio.RATIO_16_9)
                .setTargetRotation(Surface.ROTATION_0)
                .build();
    }

    private void initEdgeAnalyzer() {
        if (edgeAnalyzer != null) {
            try {
                edgeAnalyzer.clearAnalyzer();
            } catch (Exception ignore) {}
        }

        edgeAnalyzer = new ImageAnalysis.Builder()
                .setTargetAspectRatio(AspectRatio.RATIO_16_9)
                .setTargetRotation(Surface.ROTATION_0)
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .build();

        edgeAnalyzer.setAnalyzer(cameraExecutor, previewAnalyzer);
    }

    private void initImageCapture() {
        imageCapture = new ImageCapture.Builder()
                .setCaptureMode(ImageCapture.CAPTURE_MODE_MAXIMIZE_QUALITY)
                .setTargetAspectRatio(AspectRatio.RATIO_16_9)
                .setTargetRotation(Surface.ROTATION_0)
                .build();

        mShutterButton.setOnClickListener(v -> {

            //pause edge detection on preview
            previewAnalyzer.paused (true);
            mOverlay.pause(true);

            //show progress and disable buttons while saving picture
            bindProgress(true);

            //create placeholder above camera preview view to "freeze" picture
            final Bitmap placeholderBitmap = mPreviewView.getBitmap();
            if (placeholderBitmap != null) {
                mPlaceHolder.setImageBitmap(placeholderBitmap);
                mPlaceHolder.setVisibility(View.VISIBLE);
            }

            final Uri saveUri = callback.getSaveUri();
            final float density = getResources().getDisplayMetrics().density;

            imageCapture.takePicture(cameraExecutor, new ImageCapture.OnImageCapturedCallback() {
                @Override
                public void onCaptureSuccess(@NonNull ImageProxy image) {

                    mDisposable = Single.fromCallable(() -> {
                        Bitmap temp = ImageProcessor.autoCropAndRotate(image);
                        image.close();
                        Uri uri = FileUtils.saveBitmap(temp, saveUri);
                        temp.recycle();
                        return uri;
                    })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                            res -> {
                                if (rePhotoUri != null) {
                                    returnResult(res);
                                    return;
                                }
                                Transformations transformations = new Transformations(res);
                                images.add(transformations);

                                if (maxPhotos > 0 && images.size() >= maxPhotos) {
                                    mOverlay.pause(false);
                                    mOverlay.clear();
                                    replaceFragment(
                                        GalleryFragment.newInstance(images, 0, true, maxPhotos),
                                        false
                                    );
                                    return;
                                }

                                mOverlay.clear();
                                bindProgress(false);


                                mCapturedView.setVisibility(View.VISIBLE);
                                int w = mCapturedView.getWidth();
                                int h = mCapturedView.getHeight();
                                if (w > 0 && h > 0)
                                    mCapturedView.setImageBitmap(ImageProcessor.getScaledBitmap(res, w, h));
                                else
                                    mCapturedView.setImageURI(res);


                                mPlaceHolder.setVisibility(View.GONE);
                                mPlaceHolder.setImageBitmap(null);
                                if (placeholderBitmap != null)
                                    placeholderBitmap.recycle();


                                adapter.addImage(transformations, true);

                                mCapturedViews.post(() -> {

                                    int capturedListWidth = mCapturedViews.getWidth();
                                    float centerX = mCapturedViews.getX() + capturedListWidth / 2f;
                                    float offset = (adapter.getItemCount() * (36f + 8f) - 8f) * density / 2f;

                                    float trX = centerX - offset - mCapturedView.getX();
                                    float trY = mCapturedViews.getY() - mCapturedView.getY();

                                    int width = mCapturedView.getWidth();
                                    int height = mCapturedView.getHeight();
                                    float scaleX = 0.1f;
                                    float scaleY = 0.1f;
                                    if (width != 0 && height != 0) {
                                        scaleX = 36 * density / width;
                                        scaleY = 36 * density / height;
                                    }


                                    mCapturedView.animate()
                                            .withEndAction(() -> {
                                                adapter.hideFirstImage(false);
                                                mCapturedView.setVisibility(View.GONE);
                                                mCapturedView.clearAnimation();
                                                mCapturedView.setScaleX(1f);
                                                mCapturedView.setScaleY(1f);
                                                mCapturedView.setTranslationX(0);
                                                mCapturedView.setTranslationY(0);

                                                previewAnalyzer.paused(false);
                                                mOverlay.pause(false);
                                                bindUndoAndDoneButton();
                                                showHint(R.string.argus_hint_move_camera_on_next_page);
                                            })
                                            .setInterpolator(new AccelerateInterpolator())
                                            .scaleX(scaleX)
                                            .scaleY(scaleY)
                                            .translationXBy(trX)
                                            .translationYBy(trY)
                                            .setStartDelay(500)
                                            .setDuration(250)
                                            .start();
                                });
                            },

                            error -> {
                                Log.e(error);
                                bindProgress(false);

                                mPlaceHolder.setVisibility(View.GONE);
                                mPlaceHolder.setImageBitmap(null);
                                if (placeholderBitmap != null)
                                    placeholderBitmap.recycle();

                                previewAnalyzer.paused(false);
                                mOverlay.pause(false);
                            }
                        );
                }

                @Override
                public void onError(@NonNull ImageCaptureException exception) {
                    Log.e(exception);
                    previewAnalyzer.paused (false);
                    mOverlay.pause(false);
                    mPlaceHolder.setVisibility(View.GONE);
                    mPlaceHolder.setImageBitmap(null);
                    if (placeholderBitmap != null)
                        placeholderBitmap.recycle();
                    bindProgress(false);
                }
            });
        });
    }

    private void bindTopLayout() {
        mCloseButton.setOnClickListener(v -> onBackPressed());

        bindCapturedViews();
    }

    private void bindUndoAndDoneButton() {
        if (images.isEmpty()) {
            mUndoButton.setVisibility(View.GONE);
            mDoneButton.setVisibility(View.GONE);
        } else {
            mUndoButton.setVisibility(View.VISIBLE);
            mDoneButton.setVisibility(View.VISIBLE);
        }

        if (!mUndoButton.hasOnClickListeners()) {
            mUndoButton.setOnClickListener(v -> {
                adapter.deleteLastImage();
                if (images.size() > 0) {
                    Transformations t = images.remove(images.size() - 1);
                    Uri uri = t.getUri();
                    if (uri != null)
                        FileUtils.deleteFile(uri);
                }
                bindUndoAndDoneButton();
            });
        }

        if (!mDoneButton.hasOnClickListeners()) {
            mDoneButton.setOnClickListener(v -> {
                mOverlay.pause(false);
                mOverlay.clear();
                replaceFragment(
                    GalleryFragment.newInstance(images, 0, true, maxPhotos),
                    false
                );
            });
        }
    }

    private void bindProgress(boolean show) {
        if (!mProgress.hasOnClickListeners())
            mProgress.setOnClickListener(v -> {});
        mProgressVisible = show;
        mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void bindCapturedViews() {
        float density = getResources().getDisplayMetrics().density;

        mTopLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mTopLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                float itemWidth = (PREVIEW_ITEM_SIZE + 4f + 4f) * density;
                int itemCount = (int)((mFlashButton.getX() - (mCloseButton.getX() + mCloseButton.getWidth())) / itemWidth);
                Log.d("AdapterItemWidth: " + itemWidth + "; itemsCount: " + itemCount);
                adapter.setMaxShownImagesCount(itemCount);
            }
        });

        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getContext());
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.CENTER);
        mCapturedViews.setLayoutManager(layoutManager);
        mCapturedViews.addItemDecoration(new HorizontalMarginItemDecoration((int) (4f * density)));

        ArrayList<Transformations> transformations = new ArrayList<>(images);
        Collections.reverse(transformations);
        adapter.setImages(transformations);
        adapter.setListener(this);
        mCapturedViews.setAdapter(adapter);
    }

    private void returnResult(Uri uri) {
        FragmentManager manager = getFragmentManager();
        Fragment target = getTargetFragment();
        int requestCode = getTargetRequestCode();

        if (target != null && manager != null) {
            manager.popBackStack();
            target.onActivityResult(
                    requestCode,
                    Activity.RESULT_OK,
                    new Intent()
                            .putExtra(Argus.RE_PHOTO_URI, rePhotoUri)
                            .putExtra(Argus.NEW_PHOTO_URI, uri)
            );
        }
    }

    @Override
    public void onDestroyView() {
        if (cameraExecutor != null)
            cameraExecutor.shutdown();
        if (getActivity() != null) {
            DisplayManager manager = (DisplayManager)getActivity().getSystemService(Context.DISPLAY_SERVICE);
            if (manager != null)
                manager.unregisterDisplayListener(displayListener);
        }
        if (mDisposable != null)
            mDisposable.dispose();
        super.onDestroyView();
    }

    @Override
    public void onCameraPermissionGranted() {
        Activity activity = getActivity();
        if (activity != null)
            initCamera(activity);
    }

    private void showHint (@StringRes int resId) {
        if (hideHint != null)
            handler.removeCallbacks(hideHint);

        Transition transition = new Fade();
        transition.setDuration(400);
        transition.addTarget(mHintView);

        TransitionManager.beginDelayedTransition(mBarsLayout, transition);
        if (resId != 0) {
            mHintView.setText(resId);
            mHintView.setVisibility(View.VISIBLE);
            handler.postDelayed(hideHint, 3000);
        } else {
            mHintView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemSelected(Transformations transformations) {
        int pos = 0;
        for (int i = 0; i < images.size(); i++) {
            if (images.get(i).getUri() == transformations.getUri()) {
                pos = i;
                break;
            }
        }
        mOverlay.pause(false);
        mOverlay.clear();
        replaceFragment(
            GalleryFragment.newInstance(images, pos, true, maxPhotos),
            false
        );
    }
}
