package com.aces.it.argus.fragment;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.core.graphics.Insets;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.WindowInsetsControllerCompat;

import com.aces.it.argus.Argus;
import com.aces.it.argus.R;
import com.aces.it.argus.model.Transformations;
import com.aces.it.argus.processor.ImageProcessor;
import com.aces.it.argus.utils.FileUtils;
import com.aces.it.argus.view.GalleryImageView;
import com.aces.it.argus.view.TutorialView;
import com.davemorrissey.labs.subscaleview.ImageSource;

import java.util.ArrayList;
import java.util.Arrays;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import kotlin.Suppress;

/**
 * Created by O.Zakharov on 19.02.2020
 */
public class EditPhotoFragment extends BaseFragment {

    private static final int REQUEST_RE_PHOTO = 8000;
    private static final int REQUEST_CORRECT = 8001;
    private static final int REQUEST_CROP = 8002;

    public static EditPhotoFragment newInstance(Transformations transformations, boolean canRePhoto) {
        Bundle args = new Bundle();
        args.putSerializable(Argus.TRANSFORMATIONS, transformations);
        args.putBoolean(Argus.CAN_RE_PHOTO, canRePhoto);
        EditPhotoFragment fragment = new EditPhotoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private MotionLayout content;
    private GalleryImageView image;
    private FrameLayout topLayout;
    private LinearLayout bottomLayout;
    private TutorialView tutorialView;
    private View buttonDraw;
    private View buttonCrop;
    private View buttonBW;
    private View buttonRotate;
    private View buttonDone;
    private View progress;

    private boolean canRePhoto = false;
    private Transformations transformations;

    private Disposable saving;

    private WindowInsetsControllerCompat insetsController;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            transformations = (Transformations) args.getSerializable(Argus.TRANSFORMATIONS);
            canRePhoto = args.getBoolean(Argus.CAN_RE_PHOTO, false);
        }

        if (savedInstanceState != null) {
            transformations = (Transformations) savedInstanceState.getSerializable(Argus.TRANSFORMATIONS);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.argus_fmt_edit_photo, container, false);
        content = (MotionLayout) view;
        topLayout = view.findViewById(R.id.topLayout);
        bottomLayout = view.findViewById(R.id.bottomLayout);
        image = view.findViewById(R.id.image);
        tutorialView = view.findViewById(R.id.tutorialView);
        buttonDraw = view.findViewById(R.id.btnDraw);
        buttonCrop = view.findViewById(R.id.btnCrop);
        buttonBW = view.findViewById(R.id.btnBW);
        buttonRotate = view.findViewById(R.id.btnRotate);
        buttonDone = view.findViewById(R.id.btn_done);
        progress = view.findViewById(R.id.progress);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Window window = requireActivity().getWindow();
        insetsController = new WindowInsetsControllerCompat(window, window.getDecorView());
        insetsController.setSystemBarsBehavior(
            WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        );

        if (content.getCurrentState()  == R.id.exit)
            showSystemUI();
        else
            hideSystemUI();

        view.setOnApplyWindowInsetsListener((v, insets) -> {
            handleInsets(insets);
            return insets;
        });

        handleInsets(callback.getInsets());

        bindDoneButton();

        if (canRePhoto) {
            View rePhotoButton = view.findViewById(R.id.btn_re_photo);
            rePhotoButton.setVisibility(View.VISIBLE);
            rePhotoButton.setOnClickListener(v -> {
                Uri rePhotoUri = transformations.getUri();
                TakePhotoFragment fragment = TakePhotoFragment.newInstance(rePhotoUri);
                fragment.setTargetFragment(this, REQUEST_RE_PHOTO);
                replaceFragment(fragment, true);
            });
        }
        view.findViewById(R.id.btnClose).setOnClickListener(v -> onBackPressed());

        if (callback.isShowTutorial()) {
            tutorialView.setListener(() -> callback.setShowTutorialNextTime(false));
            tutorialView.setAnchorViews(Arrays.asList (
                    Pair.create(buttonDraw, getString(R.string.argus_tutorial_draw_button)),
                    Pair.create(buttonCrop, getString(R.string.argus_tutorial_crop_button)),
                    Pair.create(buttonBW, getString(R.string.argus_tutorial_bw_button)),
                    Pair.create(buttonRotate, getString(R.string.argus_tutorial_rotate_button))
            ));
            tutorialView.setVisibility(View.VISIBLE);
        } else {
            tutorialView.setVisibility(View.GONE);
        }

        buttonRotate.setOnClickListener(v -> {
            transformations.rotate();
            image.setTransformations(transformations);
        });

        buttonBW.setOnClickListener(v -> {
            transformations.toggleBW();
            image.setTransformations(transformations);
        });

        buttonDraw.setOnClickListener(v -> {
            DocumentDrawFragment fragment =
                    DocumentDrawFragment.newInstance(transformations);
            fragment.setTargetFragment(this, REQUEST_CORRECT);
            replaceFragment(fragment, true);
        });

        buttonCrop.setOnClickListener(v -> {
            DocumentCropFragment fragment =
                    DocumentCropFragment.newInstance(transformations);
            fragment.setTargetFragment(this, REQUEST_CROP);
            replaceFragment(fragment, true);
        });

        progress.setOnClickListener(v -> {});

        image.setOnClickListener(v -> toggleFullscreen());
        image.setImage(ImageSource.uri(transformations.getUri()));
        image.setTransformations(transformations);
    }

    private void handleInsets(WindowInsets windowInsets) {
        if (windowInsets != null) {
            Insets insets = WindowInsetsCompat.toWindowInsetsCompat(windowInsets)
                .getInsets(WindowInsetsCompat.Type.systemBars());

            content.setPadding(0, 0, 0, 0);
            topLayout.setPadding(
                0, Math.max(topLayout.getPaddingTop(), insets.top), 0, 0
            );
            bottomLayout.setPadding(
                0, 0, 0, Math.max(bottomLayout.getPaddingBottom(), insets.bottom)
            );

            topLayout.requestLayout();
            bottomLayout.requestLayout();
            content.requestLayout();
        }
    }

    private void bindDoneButton() {
        buttonDone.setOnClickListener(v -> {
            if (saving != null && !saving.isDisposed())
                return;

            progress.setVisibility(View.VISIBLE);

            final ContentResolver resolver = getActivity() != null ? getActivity().getContentResolver() : null;
            final Uri savePath = callback.getSaveUri();

            saving = Single.fromCallable(() -> {
                ArrayList<Uri> savedImages = new ArrayList<>();
                if (resolver != null) {
                    Uri uri = transformations.getUri();
                    if (transformations.hasChanges()) {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(resolver, uri);
                        Bitmap save = ImageProcessor.applyTransformations(bitmap, transformations);

                        if (!bitmap.isRecycled())
                            bitmap.recycle();

                        savedImages.add(FileUtils.saveBitmap(save, savePath));
                        save.recycle();

                        FileUtils.deleteFileIfInDirectory(uri, savePath);
                    } else {
                        savedImages.add (uri);
                    }
                }
                return savedImages;
            })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            uris -> {
                                progress.setVisibility(View.GONE);
                                callback.returnResult(uris);
                            },
                            error -> {
                                error.printStackTrace();
                                progress.setVisibility(View.GONE);
                            });
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable(Argus.TRANSFORMATIONS, transformations);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_RE_PHOTO: {
                    if (data != null) {
                        Uri rePhoto = data.getParcelableExtra(Argus.RE_PHOTO_URI);
                        Uri newPhoto = data.getParcelableExtra(Argus.NEW_PHOTO_URI);
                        transformations.reset();
                        transformations.setUri(newPhoto);
                        image.setImage(ImageSource.uri(transformations.getUri()));
                        image.setTransformations(transformations);
                        FileUtils.deleteFile(rePhoto);
                    }
                    break;
                }

                case REQUEST_CORRECT: {
                    if (data != null) {
                        Transformations transformations =
                                (Transformations) data.getSerializableExtra(Argus.TRANSFORMATIONS);
                        if (transformations == null)
                            return;

                        this.transformations = transformations;
                        image.setTransformations(transformations);
                    }
                    break;
                }

                case REQUEST_CROP: {
                    if (data != null) {
                        Transformations transformations =
                                (Transformations) data.getSerializableExtra(Argus.TRANSFORMATIONS);
                        Transformations newTransformations =
                                (Transformations) data.getSerializableExtra(Argus.CROP_TRANSFORMATIONS);
                        if (transformations == null || newTransformations == null)
                            return;

                        this.transformations = newTransformations;
                        image.setImage(ImageSource.uri(transformations.getUri()));
                        image.setTransformations(transformations);
                    }
                    break;
                }
            }
        }
    }

    private void toggleFullscreen() {
        if (content.getCurrentState() == R.id.exit) {
            hideSystemUI();
            content.transitionToState(R.id.enter);
        } else {
            showSystemUI();
            content.transitionToState(R.id.exit);
        }
    }

    @Override
    public boolean onBackPressedHandled() {
        if (progress.getVisibility() == View.VISIBLE || tutorialView.getVisibility() == View.VISIBLE)
            return true;

        if (canRePhoto) {
            replaceFragment(TakePhotoFragment.newInstance(null), false);
            return true;
        }

        return false;
    }

    private void hideSystemUI() {
        insetsController.hide(WindowInsetsCompat.Type.systemBars());

        // Fix bug on old android sdks, when getting white screen on pop backstack
        // after enter and exit from fullscreen mode (androidx.core:core-ktx:1.7.0)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R)
            requireActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void showSystemUI() {
        insetsController.show(WindowInsetsCompat.Type.systemBars());
    }
}
