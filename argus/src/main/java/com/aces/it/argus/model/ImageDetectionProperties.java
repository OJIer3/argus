package com.aces.it.argus.model;

import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;


public class ImageDetectionProperties {

    private final double mPreviewWidth;
    private final double mPreviewHeight;
    private final double mResultWidth;
    private final double mResultHeight;
    private final Point mTopLeftPoint;
    private final Point mBottomLeftPoint;
    private final Point mBottomRightPoint;
    private final Point mTopRightPoint;
    private final double mPreviewArea;
    private final double mResultArea;

    public ImageDetectionProperties(double previewWidth, double previewHeight, double resultWidth,
                                    double resultHeight, double previewArea, double resultArea,
                                    Point topLeftPoint, Point bottomLeftPoint, Point bottomRightPoint,
                                    Point topRightPoint) {
        mPreviewWidth = previewWidth;
        mPreviewHeight = previewHeight;
        mPreviewArea = previewArea;
        mResultWidth = resultWidth;
        mResultHeight = resultHeight;
        mResultArea = resultArea;
        mBottomLeftPoint = bottomLeftPoint;
        mBottomRightPoint = bottomRightPoint;
        mTopLeftPoint = topLeftPoint;
        mTopRightPoint = topRightPoint;
    }

    public boolean isDetectedAreaBeyondLimits() {
        return mResultArea > mPreviewArea * 0.95  || mResultArea < mPreviewArea * 0.20;
    }

    public boolean isDetectedWidthAboveLimit() {
        return mResultWidth / mPreviewWidth > 0.95;
    }

    public boolean isDetectedHeightAboveLimit() {
        return mResultHeight / mPreviewHeight > 0.95;
    }

    public boolean isDetectedAreaAboveLimit() {
        return mResultArea > mPreviewArea * 0.75;
    }

    public boolean isDetectedAreaBelowLimits() {
        return mResultArea < mPreviewArea * 0.25;
    }

    public boolean isAngleNotCorrect(MatOfPoint2f approx) {
        return getMaxCosine(approx) || isLeftEdgeDistorted() || isRightEdgeDistorted();
    }

    private boolean isRightEdgeDistorted() {
        return Math.abs(mTopRightPoint.y - mBottomRightPoint.y) > 100;
    }

    private boolean isLeftEdgeDistorted() {
        return Math.abs(mTopLeftPoint.y - mBottomLeftPoint.y) > 100;
    }

    private boolean getMaxCosine(MatOfPoint2f approx) {
        double maxCosine = 0;
        Point[] approxPoints = approx.toArray();
        maxCosine = getMaxCosine(maxCosine, approxPoints);
        return maxCosine >= 0.085; //(smallest angle is below 87 deg)
    }

    public boolean isEdgeTouching() {
        return isTopEdgeTouching() || isBottomEdgeTouching() || isLeftEdgeTouching() || isRightEdgeTouching();
    }

    private boolean isBottomEdgeTouching() {
        return (mBottomLeftPoint.x >= mPreviewHeight - 50 || mBottomRightPoint.x >= mPreviewHeight - 50);
    }

    private boolean isTopEdgeTouching() {
        return (mTopLeftPoint.x <= 50 || mTopRightPoint.x <= 50);
    }

    private boolean isRightEdgeTouching() {
        return (mTopRightPoint.y >= mPreviewWidth - 50 || mBottomRightPoint.y >= mPreviewWidth - 50);
    }

    private boolean isLeftEdgeTouching() {
        return (mTopLeftPoint.y <= 50 || mBottomLeftPoint.y <= 50);
    }

    private double getMaxCosine(double maxCosine, Point[] approxPoints) {
        for (int i = 2; i < 5; i++) {
            double cosine = Math.abs(angle(approxPoints[i % 4], approxPoints[i - 2], approxPoints[i - 1]));
            maxCosine = Math.max(cosine, maxCosine);
        }
        return maxCosine;
    }

    private double angle(Point p1, Point p2, Point p0) {
        double dx1 = p1.x - p0.x;
        double dy1 = p1.y - p0.y;
        double dx2 = p2.x - p0.x;
        double dy2 = p2.y - p0.y;
        return (dx1 * dx2 + dy1 * dy2) / Math.sqrt((dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2) + 1e-10);
    }
}
