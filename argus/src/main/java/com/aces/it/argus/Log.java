package com.aces.it.argus;

import android.app.Application;
import android.media.MediaScannerConnection;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public final class Log {

    private static final String LOGS_DIRECTORY_NAME = "logs";

    private static final String PREFIX = "debug-argus-";
    private static final String EXTENSION = "txt";

    private static final int DELEMITER = 0;
    public static final int DEBUG = 1;
    public static final int ERROR = 2;
    public static final int WARN = 4;
    public static final int VERBOSE = 8;
    public static final int INFO = 16;

    private static boolean sWriteLog = false;
    private static int sFileDayOfMonth = 0;
    //private static String sFilePath = null;
    private static String sFileName = null;
    private static File sFile = null;
    private static File sDir = null;
    private static int sMaxFileSize = 10485760; // 10Mb
    private static String sAppName = "argus";
    private static ReentrantLock sLock = new ReentrantLock();
    private static MyFileFilter sFileFilter = null;
    private static MyArrayComparator sArrayComparator = null;
    private static int sLogLevel = Log.ERROR | Log.WARN | Log.INFO;
    private static int sKeepNumber = 5;
    private static Application sApp;

    private Log() { }


    /**
     * Removes logs files from old path that was in the root of SD card.
     */
    public static void removeDepricatedDirectory() {
        try {
            String state = Environment.getExternalStorageState();
            if (state.equals(Environment.MEDIA_MOUNTED)) {
                String pathLogs = Environment.getExternalStorageDirectory().getAbsolutePath() +
                        File.separator + "Logs" + File.separator + Log.sAppName;
                File dir = new File (pathLogs);
                if (dir.exists()) {
                    // Delete directory content
                    File[] files = dir.listFiles();
                    for (File f : files) {
                        f.delete();
                    }
                    dir.delete(); // Delete directory itself
                }
                // Try to delete parent Logs directory. It will be succeeded if it is empty.
                dir.getParentFile().delete();
            }
        } catch (Exception ignored) { }
    }

    /* lock/unlock log file */
    public static void lock() {
        Log.sLock.lock();
    }

    public static void unlock() {
        Log.sLock.unlock();
    }

    /* Markers */
    public static void entered() {
        Log.d("entered");
    }

    public static void finished() {
        Log.d("finished");
    }

    public static void finished(String str) {
        Log.d("finished: " + str);
    }

    public static void fired() {
        Log.d("fired");
    }

    public static void called() {
        Log.d("called");
    }

    public static void failed() {
        Log.d("failed");
    }

    public static void succeeded() {
        Log.d("succeeded");
    }

    public static void delemiter() {
        Log.log(Log.DELEMITER, "***");
    }

    /* Debug */
    public static void d(String msg) {
        if ((Log.sLogLevel & Log.DEBUG) > 0) {
            Log.log(Log.DEBUG, msg);
        }
    }

    /* Error */
    public static void e(String msg) {
        if ((Log.sLogLevel & Log.ERROR) > 0) {
            Log.log(Log.ERROR, msg);
        }
    }

    public static void e(Throwable tr) {
        if (tr != null)
            Log.e(null, tr);
    }

    public static void e(String msg, Throwable tr) {
        if ((Log.sLogLevel & Log.ERROR) > 0) {
            if (msg != null)
                Log.log(Log.ERROR, msg);

            if (tr.getMessage() != null)
                Log.log(Log.ERROR, tr.getMessage());

            Log.log(Log.ERROR, android.util.Log.getStackTraceString(tr));
        }
    }

    /* Information */
    public static void i(String msg) {
        if ((Log.sLogLevel & Log.INFO) > 0) {
            Log.log(Log.INFO, msg);
        }
    }

    /* Verbose */
    public static void v(String msg) {
        if ((Log.sLogLevel & Log.VERBOSE) > 0) {
            Log.log(Log.VERBOSE, msg);
        }
    }

    /* Warning */
    public static void w(String msg) {
        if ((Log.sLogLevel & Log.WARN) > 0) {
            Log.log(Log.WARN, msg);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Configurations
    public static synchronized void setApp(Application app) {
        Log.sApp = app;
    }

    public static synchronized void setWriteLog(boolean value) {
        Log.sWriteLog = value;
    }

    public static synchronized void setAppName(String value) {
        Log.sAppName = value;
    }

    public static synchronized void setMaxFileSize(int size) {
        Log.sMaxFileSize = size;
    }

    /* Change log level */
    public static synchronized void addLogLevel(int level) {
        Log.sLogLevel |= level;
    }

    public static synchronized void setLogLevel(int level) {
        Log.sLogLevel = level;
    }

    public static synchronized void setMaxFileNumber(int num) {
        Log.sKeepNumber = num;
    }

    public static String getLogFilePath() {
        //return Log.sFilePath;
        return Log.sDir.getAbsolutePath();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // XXX

    /* Logging */
    private static void log(int type, String msg) {
        String typeName;
        String normMsg = Log.getScope() + Log.normalizeMessage(msg);

        Log.sLock.lock();

        // log in android logger
        if (type == Log.DELEMITER) {
            android.util.Log.d(Log.sAppName, msg);
            typeName = "D";
            normMsg = msg;
        }
        else if (type == Log.DEBUG) {
            android.util.Log.d(Log.sAppName, normMsg);
            typeName = "D";
        }
        else if (type == Log.ERROR) {
            android.util.Log.e(Log.sAppName, normMsg);
            typeName = "E";
        }
        else if (type == Log.WARN) {
            android.util.Log.w(Log.sAppName, normMsg);
            typeName = "W";
        }
        else if (type == Log.VERBOSE) {
            android.util.Log.v(Log.sAppName, normMsg);
            typeName = "V";
        }
        else {
            android.util.Log.i(Log.sAppName, normMsg);
            typeName = "I";
        }

        if (Log.sWriteLog)
            Log.write(typeName, Log.sAppName, normMsg);

        Log.sLock.unlock();
    }

    private static synchronized void write(String type, String tag, String msg) {
        if (sApp == null)
            return;
        try {
            String state = Environment.getExternalStorageState();
            if (state.equals(Environment.MEDIA_MOUNTED)) {
                if (Log.sDir == null) {
                    Log.sDir = sApp.getExternalFilesDir(Log.LOGS_DIRECTORY_NAME);
                    if (Log.sDir == null) return;
                    //Log.sFilePath = f.getAbsolutePath();
                }

                boolean rotate = false;
                int curDayOfMonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
                if (Log.sFileName != null) {
                    // Check if need to rotate file by size
                    if (Log.sFile.length() >= Log.sMaxFileSize) {
                        rotate = true;
                    }

                    // Check if we need to rotate file by new day
                    if (curDayOfMonth != Log.sFileDayOfMonth) {
                        rotate = true;
                    }
                }

                // Rotate files or first write
                if (rotate || Log.sFileName == null) {
                    if (Log.sArrayComparator == null)
                        Log.sArrayComparator = new MyArrayComparator();

                    if (Log.sFileFilter == null)
                        Log.sFileFilter = new MyFileFilter(Log.sAppName + "-" + Log.PREFIX);

                    //File dir = new File(Log.sFilePath);
                    Log.sFileDayOfMonth = curDayOfMonth;

                    // Create new file name
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                    Log.sFileName = Log.sAppName + "-" + Log.PREFIX + sdf.format(new Date()) + "." + Log.EXTENSION;
                    Log.sFile = new File(Log.sDir.getAbsolutePath() + File.separator + Log.sFileName);

                    // Rotate files here if new file is from the same day and size of existing
                    // file is greater than sMaxFileSize
                    if (Log.sFile.exists() && Log.sFile.length() >= Log.sMaxFileSize) {
                        String name = Log.getFileNameWithoutExtension(Log.sFile);
                        File[] files = Log.sDir.listFiles(new MyFileFilter(name));
                        Arrays.sort(files, Log.sArrayComparator); // on top is most earle file
                        for (int i = (files.length - 1); i >= 0; i--) {
                            File f = files[i];
                            String ext = Log.getFileExtension(f);
                            File newFile = new File(Log.sDir.getAbsolutePath() + File.separator + name + "." + (i) + "." + ext);
                            f.renameTo(newFile);
                            scanFile(f);
                        }
                    }

                    // Check if number of stored files is not greater that specified
                    File[] files = Log.sDir.listFiles(Log.sFileFilter);
                    if (files != null && files.length >= Log.sKeepNumber) {
                        Arrays.sort(files, Log.sArrayComparator);
                        int i = Log.sKeepNumber;
                        if (!Log.sFile.exists()) i--;
                        while (i < files.length) files[i++].delete(); // delete old files

                        // check that all not used files are archived
                        ArchivatorRunnable ar = new ArchivatorRunnable();
                        for (File f : files) {
                            if (f.exists()) {
                                String ext = Log.getFileExtension(f);
                                // If current file extension is equal to Log.EXTENSION we need
                                // to archive it. And we need to skip currently active file.
                                if (ext.equals(Log.EXTENSION) && !f.equals(Log.sFile)) {
//android.util.Log.d("LOGGER", "archive: " + f.getName());
                                    ar.addFile(f);
                                }
                            }
                        }

                        if (ar.hasFiles()) new Thread(ar).start();
                    }
                }

                try {
                    if (!Log.sDir.exists()) Log.sDir.mkdirs();

                    File file = new File(Log.sDir.getAbsoluteFile() + File.separator + Log.sFileName);
                    boolean isNew = !file.exists();

                    FileWriter fw = new FileWriter(file, true);

                    if (isNew) {
                        fw.write("Application id: " + BuildConfig.LIBRARY_PACKAGE_NAME + "\r\n");
                        fw.write("Version name: " + BuildConfig.VERSION_NAME + "\r\n");
                        fw.write("Version code: " + BuildConfig.VERSION_CODE + "\r\n");
                        //fw.write("Build flavor: " + BuildConfig.FLAVOR + "\r\n");
                        fw.write("Build type: " + BuildConfig.BUILD_TYPE + "\r\n");
                        fw.write("Device brand: " + Build.BRAND + "\r\n");
                        fw.write("Device name: " + Build.DEVICE + "\r\n");
                        fw.write("Device board: " + Build.BOARD + "\r\n");
                        fw.write("Device hardware: " + Build.HARDWARE + "\r\n");
                        fw.write("Device product: " + Build.PRODUCT + "\r\n");
                        fw.write("Device model: " + Build.MODEL + "\r\n");
                        fw.write("Android version: " + Build.VERSION.RELEASE + "\r\n");
                        fw.write("Android SDK: " + Build.VERSION.SDK_INT + "\r\n");
                        fw.write("Android build type: " + Build.TYPE + "\r\n");
                        fw.write("\r\n");
                    }

                    fw.write(type + "/" + tag + ": " + msg + "\r\n");
                    fw.flush();
                    fw.close();

                    if (isNew)
                        scanFile(file);

                    fw = null;
                    file = null;
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* Utils */
    private static String normalizeMessage(String msg) {
        if (msg == null) msg = "";
        return msg;
    }

    private static String getScope() {
        StackTraceElement trace[] = Thread.currentThread().getStackTrace();
        StackTraceElement e = null;// = Thread.currentThread().getStackTrace()[5];
        for (int i = trace.length - 1; i >= 0; --i) {
            if (trace[i].getClassName().startsWith(Log.class.getName())) break;
            e = trace[i];
        }

        String threadName = Thread.currentThread().getName();
        if (threadName == null || threadName.length() == 0) threadName = "";
        else threadName += " ";
        threadName += Long.toString(Thread.currentThread().getId());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HHmmss.SSSZ:");
        return sdf.format(new Date()) + Log.getSettingCode(Settings.Global.AUTO_TIME_ZONE) +
                Log.getSettingCode(Settings.Global.AUTO_TIME) + ":" +
                e.getClassName() + "." + e.getMethodName() + "[" + android.os.Process.myPid() + "-" +
                android.os.Process.myTid() + "-" + threadName + "]:";
    }

    // Settings.Global.AUTO_TIME_ZONE
    // Settings.Global.AUTO_TIME
    private static String getSettingCode(String name) {
        if (sApp == null)
            return "n";
        int val = -1;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            try {
                val = Settings.Global.getInt(sApp.getContentResolver(), name, -1);
            }
            catch (Exception ignored) { }
        }
        return (val == -1) ? "n" : ((val == 0) ? "m" : "a");
    }


    private static class MyFileFilter implements FileFilter {

        private String startsWithString;

        public MyFileFilter(String str) {
            this.startsWithString = str;
        }

        public boolean accept(File file) {
            return !file.isDirectory() && file.getName().startsWith(this.startsWithString);
        }

    }

    private static class MyArrayComparator implements Comparator<File> {

        public int compare(File lhs, File rhs) {
            //return (int) (rhs.lastModified() - lhs.lastModified());
            String lf = Log.getFileNameWithoutIndex(lhs);
            String rf = Log.getFileNameWithoutIndex(rhs);

            int cmp = rf.compareTo(lf);
            if (cmp == 0) {
                int li = Log.getFileIndex(lhs) + 1;
                int ri = Log.getFileIndex(rhs) + 1;
                return (li - ri);
            }

            return cmp;
        }

    }

    private static String getFileNameWithoutExtension(File f) {
        String name = f.getName();
        int pos = name.lastIndexOf(".");
        if (pos > 0)
            return name.substring(0, pos);
        return name;
    }

    private static String getFileNameWithoutIndex(File f) {
        String name = f.getName();
        int pos = name.indexOf(".");
        if (pos > 0)
            return name.substring(0, pos);
        return name;
    }

    private static String getFileExtension(File f) {
        String name = f.getName();
        int pos = name.lastIndexOf(".");
        if (pos > 0)
            return name.substring(pos + 1, name.length());
        return "";
    }

    private static int getFileIndex(File f) {
        int idx = -1;
        String name = Log.getFileNameWithoutExtension(f);
        int pos = name.lastIndexOf(".");
        if (pos > 0) {
            String idxS = name.substring(pos + 1, name.length());
            try { idx = Integer.parseInt(idxS); }
            catch (Exception ignored) { }
        }
        return idx;
    }


    private static class ArchivatorRunnable implements Runnable {

        private ArrayList<File> files = new ArrayList<>();

        public void addFile(File file) {
            this.files.add(file);
        }

        public boolean hasFiles() {
            return (this.files.size() > 0);
        }

        @Override
        public void run() {
            for (File f : this.files) {
                try {
                    String name = Log.getFileNameWithoutExtension(f);
                    File fz = new File(f.getParent() + File.separator + name + ".zip");
                    if (fz.exists()) fz.delete();

                    android.util.Log.d("MTSCLOG", "archive: " + f.getParent());
                    android.util.Log.d("MTSCLOG", "archive: " + fz.getAbsolutePath());

                    ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(fz));
                    byte[] buf = new byte[1024];
                    try {
                        FileInputStream fis = new FileInputStream(f);
                        zos.putNextEntry(new ZipEntry(f.getName()));
                        int len;
                        while ((len = fis.read(buf)) > 0)
                            zos.write(buf, 0, len);
                        zos.closeEntry();
                        fis.close();
                    } catch (Exception ignored) { }
                    zos.close();
                    f.delete();
                } catch (Exception ignored) { }
            }
            scanFile(Log.sDir);
        }
    }

    static void scanFile(File file) {
        try {
            if (sApp != null && file != null && file.exists()) {
                if (file.isDirectory() && file.list().length > 0) {
                    MediaScannerConnection.scanFile(sApp, file.list(), null, null);
                } else if (!file.isDirectory()) {
                    MediaScannerConnection.scanFile(sApp, new String[]{file.getAbsolutePath()}, null, null);
                }
            }
        } catch (Exception ignore) { }
    }
}
