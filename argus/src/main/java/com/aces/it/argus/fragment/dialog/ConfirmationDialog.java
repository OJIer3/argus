package com.aces.it.argus.fragment.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.aces.it.argus.Argus;
import com.aces.it.argus.R;
import com.aces.it.argus.fragment.GalleryFragment;
import com.google.android.material.button.MaterialButton;

/**
 * Created by O.Zakharov on 09.11.2020
 */
public class ConfirmationDialog extends DialogFragment {

    public static void show (Fragment target, int requestCode, int position) {
        FragmentManager manager = target.getFragmentManager();
        if (manager != null) {
            Fragment fragment = manager.findFragmentByTag(ConfirmationDialog.class.getCanonicalName());
            if (fragment instanceof ConfirmationDialog)
                ((ConfirmationDialog) fragment).dismissAllowingStateLoss();

            ConfirmationDialog dialog = new ConfirmationDialog();
            dialog.setRetainInstance(true);
            dialog.setTargetFragment(target, requestCode);
            Bundle args = new Bundle();
            args.putInt(Argus.SELECTED_POSITION, position);
            dialog.setArguments(args);

            dialog.show(manager, ConfirmationDialog.class.getCanonicalName());
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(getContext()).inflate(R.layout.argus_dlg_confirmation, null);

        MaterialButton cancelButton = view.findViewById(R.id.btnCancel);
        cancelButton.setOnClickListener(v -> dismissAllowingStateLoss());

        MaterialButton okButton = view.findViewById(R.id.btnOk);
        okButton.setOnClickListener (v -> {
            dismissAllowingStateLoss();
            Fragment target = getTargetFragment();
            int requestCode = getTargetRequestCode();
            if (target != null) {
                Bundle args = getArguments();
                target.onActivityResult (
                        requestCode,
                        Activity.RESULT_OK,
                        new Intent()
                        .putExtra(Argus.SELECTED_POSITION, args != null ? args.getInt(Argus.SELECTED_POSITION, -1) : -1)
                );
            }
        });
        if (getTargetRequestCode() == GalleryFragment.REQUEST_DELETE) {
            okButton.setTextColor(ContextCompat.getColor(view.getContext(), R.color.argusDialogDeleteButton));
            okButton.setText(R.string.argus_btn_delete);
        }

        //noinspection ConstantConditions
        Dialog dialog = new AlertDialog.Builder(getContext(), R.style.ArgusDialog).setView(view).create();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        return dialog;
    }
}
