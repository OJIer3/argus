package com.aces.it.argus.view;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.aces.it.argus.R;

/**
 * Created by O.Zakharov on 02.03.2020
 */
public class PaletteColorView extends View implements Checkable {

    private boolean checked;
    private int color;
    private ValueAnimator animator;
    private Drawable checkMark;


    private float checkScale;
    private float checkSize;
    private float checkLeft;
    private float checkTop;
    private float checkRight;
    private float checkBottom;
    private float checkBgR;

    private Paint checkBackgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public PaletteColorView(Context context) {
        super(context);
        init(context, null);
    }

    public PaletteColorView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public PaletteColorView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init (Context context, AttributeSet attrs) {

        color = ContextCompat.getColor(context, R.color.argusPalette3);

        if (attrs != null) {
            int[] attrsArray = new int[] {android.R.attr.checked, android.R.attr.backgroundTint};
            TypedArray array = context.obtainStyledAttributes(attrs, attrsArray);
            try {
                setCheckedInternal (array.getBoolean(0, false), false);
                color = array.getColor(1, color);
            } finally {
                array.recycle();
            }
        } else {
            setCheckedInternal(false, false);
        }

        checkMark = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_argus_done, null);
        checkBackgroundPaint.setColor(ContextCompat.getColor(context, R.color.argusBlack50));
        checkBackgroundPaint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        float density = getResources().getDisplayMetrics().density;
        checkBgR = 16f * density;
        checkSize = 18 * density;
        checkLeft = (w - checkSize) / 2f;
        checkTop = (h - checkSize) / 2f;
        checkRight = checkLeft + checkSize;
        checkBottom = checkTop + checkSize;
    }

    @Override
    public void setChecked (boolean checked) {
        setCheckedInternal (checked, true);
    }

    @Override
    public boolean isChecked () {
        return checked;
    }

    @Override
    public void toggle () {
        setCheckedInternal (!checked, true);
    }

    public int getColor() {
        return color;
    }

    private void setCheckedInternal(boolean checked, boolean animate) {
        this.checked = checked;

        if (animator != null)
            animator.cancel();

        if (!animate) {
            checkScale = checked ? 1f : 0f;
            invalidate();
        } else {
            animator = ValueAnimator.ofFloat(checkScale, checked ? 1f : 0f);
            animator.setDuration(checked ? (long) ((1f - checkScale) * 255f) : (long) (checkScale * 255f));
            animator.addUpdateListener(animation -> {
                checkScale = (float) animation.getAnimatedValue();
                invalidate();
            });
            animator.start();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float centerX = getWidth() / 2f;
        float centerY = getHeight() / 2f;

        checkBackgroundPaint.setAlpha(Math.max(0, Math.min(127, (int) (127 * checkScale))));
        canvas.drawCircle(centerX, centerY, checkScale * checkBgR, checkBackgroundPaint);

        checkMark.setBounds(
                (int)(centerX - checkScale * (centerX - checkLeft)),
                (int)(centerY - checkScale * (centerY - checkTop)),
                (int)(centerX + checkScale * (checkRight - centerX)),
                (int)(centerY + checkScale * (checkBottom - centerY))
        );
        checkMark.draw(canvas);
    }
}
