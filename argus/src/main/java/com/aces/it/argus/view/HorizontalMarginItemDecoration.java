package com.aces.it.argus.view;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by O.Zakharov on 05.11.2020
 */
public class HorizontalMarginItemDecoration extends RecyclerView.ItemDecoration {

    private int offset;

    public HorizontalMarginItemDecoration(int offset) {
        this.offset = offset;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        outRect.right = offset;
        outRect.left = offset;
    }
}
