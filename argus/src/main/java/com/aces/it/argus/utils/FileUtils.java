package com.aces.it.argus.utils;

import android.graphics.Bitmap;
import android.net.Uri;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 * Created by O.Zakharov on 06.11.2020
 */
public class FileUtils {

    public static void deleteFile(Uri uri) {
        if (uri == null)
            return;
        String path = uri.getPath();
        if (path != null && !path.isEmpty())
            try {
                File file = new File(path);
                if (file.exists())
                    //noinspection ResultOfMethodCallIgnored
                    file.delete();
            } catch (Exception ignore) {
            }
    }

    public static void deleteFileIfInDirectory(Uri uri, Uri directory) {
        if (uri == null || directory == null)
            return;
        String path = uri.getPath();
        String directoryPath = directory.getPath();
        if (path != null && !path.isEmpty()
                && directoryPath != null && !directoryPath.isEmpty()
                && path.startsWith(directoryPath)
        )
            try {
                File file = new File(path);
                if (file.exists())
                    //noinspection ResultOfMethodCallIgnored
                    file.delete();
            } catch (Exception ignore) {
            }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static Uri saveBitmap(Bitmap bitmap, Uri directory) {
        OutputStream fos = null;
        try {
            File file = new File(directory.getPath(), UUID.randomUUID() + ".jpg");
            if (file.exists())
                file.delete();
            file.createNewFile();

            fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 75, fos);
            return Uri.fromFile(file);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (fos != null)
                    fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
