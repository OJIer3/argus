package com.aces.it.argus.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.aces.it.argus.model.Quad;

/**
 * Created by O.Zakharov on 24.04.2020
 */
public class CameraScanOverlayView extends View {

    public CameraScanOverlayView(@NonNull Context context) {
        this(context, null);
    }

    public CameraScanOverlayView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CameraScanOverlayView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public CameraScanOverlayView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr,
                                 int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();

        mHintPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mHintPaint.setStyle(Paint.Style.FILL);

        mHintStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mHintStrokePaint.setStyle(Paint.Style.STROKE);
        mHintStrokePaint.setStrokeWidth(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, metrics));

        mHintPath = new Path();
    }

    private Quad mLargestQuad = null;
    private Paint mHintPaint;
    private Paint mHintStrokePaint;
    private Path mHintPath;
    private boolean paused = false;

    @Override
    protected void onDraw(Canvas canvas) {
        if (mLargestQuad != null) {
            boolean invalidate = mLargestQuad.addOnPath(mHintPath, 1);
            canvas.drawPath(mHintPath, mHintPaint);
            canvas.drawPath(mHintPath, mHintStrokePaint);
            if (invalidate && !paused)
                invalidate();
        }
    }

    public void setQuad(Quad quad) {
        if (!paused) {
            if (mLargestQuad == null && quad == null)
                return;
            mLargestQuad = quad;
            if (mLargestQuad != null) {
                mHintPaint.setColor(mLargestQuad.getColor());
                mHintStrokePaint.setColor(mLargestQuad.getBorderColor());
            }
            invalidate();
        }
    }

    public void pause(boolean pause) {
        this.paused = pause;
        if (!paused)
            invalidate();
    }

    public void clear() {
        mLargestQuad = null;
        invalidate();
    }
}
