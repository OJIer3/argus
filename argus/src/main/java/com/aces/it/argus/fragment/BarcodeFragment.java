package com.aces.it.argus.fragment;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.camera.core.AspectRatio;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.Preview;
import androidx.camera.core.TorchState;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.Insets;
import androidx.core.view.WindowInsetsCompat;
import androidx.lifecycle.LiveData;

import com.aces.it.argus.Callback;
import com.aces.it.argus.Log;
import com.aces.it.argus.R;
import com.aces.it.argus.processor.ImageProcessor;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.oned.MultiFormatOneDReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by O.Zakharov on 19.02.2020
 */
public class BarcodeFragment extends BaseFragment {

    public static BarcodeFragment newInstance() {
        return new BarcodeFragment();
    }

    private ImageView mFlashButton;
    private View mCloseButton;
    private PreviewView mPreviewView;
    private Callback mBarcodeListener = null;

    private Preview cameraPreview;
    private ImageAnalysis barcodeAnalyzer;
    private ExecutorService barcodeAnalyzerExecutor;

    private LiveData<Integer> torchState = null;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof Callback)
            mBarcodeListener = (Callback) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.argus_fmt_barcode_scan, container, false);
        mPreviewView = view.findViewById(R.id.previewView);
        mFlashButton = view.findViewById(R.id.btn_flash);
        mCloseButton = view.findViewById(R.id.btn_close);
        mFlashButton.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (hasCameraPermission())
            initCamera(view.getContext());
        bindCloseButton();

        view.setOnApplyWindowInsetsListener((v, insets) -> {
            handleInsets(insets);
            return insets;
        });

        handleInsets(callback.getInsets());
    }

    private void handleInsets(WindowInsets windowInsets) {
        if (windowInsets != null) {
            Insets insets = WindowInsetsCompat.toWindowInsetsCompat(windowInsets)
                .getInsets(WindowInsetsCompat.Type.systemBars());

            ConstraintLayout.LayoutParams flashParams =
                (ConstraintLayout.LayoutParams) mFlashButton.getLayoutParams();
            flashParams.topMargin = Math.max(flashParams.topMargin, insets.top);

            ConstraintLayout.LayoutParams closeParams =
                (ConstraintLayout.LayoutParams) mCloseButton.getLayoutParams();
            closeParams.topMargin = Math.max(closeParams.topMargin, insets.top);

            mFlashButton.requestLayout();
            mCloseButton.requestLayout();
        }
    }

    private void initCamera(Context context) {
        final ListenableFuture<ProcessCameraProvider> cameraProviderFuture
                = ProcessCameraProvider.getInstance(context);
        cameraProviderFuture.addListener(
                () -> mPreviewView.post(() -> {
                    try {
                        bindCamera(cameraProviderFuture.get());
                    } catch (InterruptedException | ExecutionException e) {
                        Log.e(e);
                    }
                }),
                ContextCompat.getMainExecutor(context));
    }

    private void bindCamera(ProcessCameraProvider cameraProvider) {
        cameraProvider.unbindAll();

        initPreview();
        initQrAnalyzer();

        CameraSelector cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build();


        Camera camera = cameraProvider.bindToLifecycle(
                getViewLifecycleOwner(),
                cameraSelector,
                cameraPreview, barcodeAnalyzer);

        cameraPreview.setSurfaceProvider( mPreviewView.getSurfaceProvider());

        if (camera.getCameraInfo().hasFlashUnit()) {
            torchState = camera.getCameraInfo().getTorchState();
            torchState.removeObservers(getViewLifecycleOwner());
            torchState.observe(
                    getViewLifecycleOwner(),
                    integer -> mFlashButton.setImageResource(TorchState.ON == integer ?
                            R.drawable.ic_argus_flash_on :
                            R.drawable.ic_argus_flash_off
                    )
            );

            mFlashButton.setVisibility(View.VISIBLE);
            mFlashButton.setOnClickListener(v -> {
                if (torchState != null && torchState.getValue() != null)
                    camera.getCameraControl().enableTorch(TorchState.ON != torchState.getValue());
            });
            if (torchState != null && torchState.getValue() != null)
                mFlashButton.setImageResource(TorchState.ON == torchState.getValue() ?
                    R.drawable.ic_argus_flash_on :
                    R.drawable.ic_argus_flash_off);
        } else {
            mFlashButton.setVisibility(View.GONE);
        }
    }

    private void initPreview() {
        cameraPreview = new Preview.Builder()
                .setTargetAspectRatio(AspectRatio.RATIO_16_9)
                .setTargetRotation(Surface.ROTATION_0)
                .build();
    }

    private void initQrAnalyzer() {
        if (barcodeAnalyzer != null) {
            try {
                barcodeAnalyzer.clearAnalyzer();
            } catch (Exception ignore) {}
        }

        if (barcodeAnalyzerExecutor != null && !barcodeAnalyzerExecutor.isShutdown()) {
            barcodeAnalyzerExecutor.shutdown();
        }

        barcodeAnalyzer = new ImageAnalysis.Builder()
                .setTargetAspectRatio(AspectRatio.RATIO_16_9)
                .setTargetRotation(Surface.ROTATION_0)
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .build();

        barcodeAnalyzerExecutor = Executors.newSingleThreadExecutor();

        final ArrayList<BarcodeFormat> formats = new ArrayList<>();
        formats.add(BarcodeFormat.EAN_13);
        formats.add(BarcodeFormat.EAN_8);
        formats.add(BarcodeFormat.UPC_E);
        formats.add(BarcodeFormat.UPC_A);
        formats.add(BarcodeFormat.CODE_39);
        formats.add(BarcodeFormat.CODE_93);
        formats.add(BarcodeFormat.CODE_128);
        formats.add(BarcodeFormat.ITF);
        formats.add(BarcodeFormat.CODABAR);
        formats.add(BarcodeFormat.RSS_14);
        formats.add(BarcodeFormat.RSS_EXPANDED);

        final HashMap<DecodeHintType, Object> hints = new HashMap<DecodeHintType, Object>();
        hints.put(DecodeHintType.POSSIBLE_FORMATS, formats);
        barcodeAnalyzer.setAnalyzer(barcodeAnalyzerExecutor, new ImageAnalysis.Analyzer() {

            private final Rect cropRect = new Rect();
            private boolean logImageSize = true;
            private final MultiFormatOneDReader reader = new MultiFormatOneDReader(hints);


            @Override
            public void analyze(@NonNull ImageProxy image) {

                int barcodeHeight = (int) (0.9 * image.getHeight());
                int barcodeWidth = (int) (0.1f * barcodeHeight);
                while (barcodeWidth > 0 && barcodeWidth % 2 != 0 && barcodeWidth % 3 != 0) {
                    barcodeWidth--;
                }

                while (barcodeHeight > 0 && barcodeHeight % 2 != 0 && barcodeHeight % 3 != 0) {
                    barcodeHeight--;
                }

                int hBorder = (image.getWidth() - barcodeWidth) / 2;
                int vBorder = (image.getHeight() - barcodeHeight) / 2;

                if (logImageSize) {
                    switch (image.getFormat()) {
                        case ImageFormat.JPEG: { Log.d("Image format: JPEG"); break;}
                        case ImageFormat.YUV_420_888: { Log.d("Image format: YUV_420_888"); break;}
                        case ImageFormat.YUV_422_888: { Log.d("Image format: YUV_422_888"); break;}
                        case ImageFormat.YUV_444_888: { Log.d("Image format: YUV_444_888"); break;}
                        case ImageFormat.FLEX_RGB_888: { Log.d("Image format: FLEX_RGB_888"); break;}
                        case ImageFormat.FLEX_RGBA_8888: { Log.d("Image format: FLEX_RGBA_8888"); break;}
                        case ImageFormat.RAW_SENSOR: { Log.d("Image format: RAW_SENSOR"); break;}
                        case ImageFormat.RAW_PRIVATE: { Log.d("Image format: RAW_PRIVATE"); break;}
                        case ImageFormat.HEIC: { Log.d("Image format: HEIC"); break;}
                    }
                    Log.d("Image size: " + image.getWidth() + " x " + image.getHeight() + "; Rotation: " + image.getImageInfo().getRotationDegrees());
                    Log.d("Analyze size: " + barcodeWidth + " x " + barcodeHeight);
                    logImageSize = false;
                }

                cropRect.set(hBorder, vBorder, hBorder + barcodeWidth, vBorder + barcodeHeight);
                image.setCropRect(cropRect);


                try {
                    Bitmap temp = ImageProcessor.YUV420toBitmap(image);
                    Bitmap bm = ImageProcessor.getRotatedBitmap(temp, 90);
                    temp.recycle();

                    int[] intArray = new int[bm.getWidth()*bm.getHeight()];
                    bm.getPixels(intArray, 0, bm.getWidth(), 0, 0, bm.getWidth(), bm.getHeight());
                    LuminanceSource source = new RGBLuminanceSource(bm.getWidth(), bm.getHeight(),intArray);
                    BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
                    try {
                        Result result = reader.decode(bitmap);
                        String qr = result.getText();
                        if (!TextUtils.isEmpty(qr)) {
                            Log.d("Barcode " + qr);
                            if (mBarcodeListener != null)
                                mBarcodeListener.onBarcodeRead(qr);
                        }
                    } catch (NotFoundException ignore) {
                        //Log.e(e);
                    }
                    bm.recycle();
                } catch (Exception ex) {
                    Log.e(ex);
                } finally {
                    image.close();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        if (barcodeAnalyzerExecutor != null)
            barcodeAnalyzerExecutor.shutdown();
        super.onDestroyView();
    }

    @Override
    public void onCameraPermissionGranted() {
        Activity activity = getActivity();
        if (activity != null)
            initCamera(activity);
    }

    private void bindCloseButton() {
        mCloseButton.setOnClickListener(v -> {
            Activity activity = getActivity();
            if (activity != null)
                activity.finish();
        });
    }
}
