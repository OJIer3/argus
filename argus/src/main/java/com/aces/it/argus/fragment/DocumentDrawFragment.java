package com.aces.it.argus.fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.graphics.Insets;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.transition.Fade;
import androidx.transition.Slide;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;
import androidx.viewpager2.widget.ViewPager2;

import com.aces.it.argus.Argus;
import com.aces.it.argus.R;
import com.aces.it.argus.adapter.DrawTutorialAdapter;
import com.aces.it.argus.model.Transformations;
import com.aces.it.argus.view.GalleryImageView;
import com.aces.it.argus.view.PaletteColorView;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import io.reactivex.disposables.Disposable;

/**
 * Created by O.Zakharov on 21.02.2020
 */
public class DocumentDrawFragment extends BaseFragment {

    public static DocumentDrawFragment newInstance(Transformations transformations) {
        Bundle args = new Bundle();
        args.putSerializable(Argus.TRANSFORMATIONS, transformations);

        DocumentDrawFragment fragment = new DocumentDrawFragment();
        fragment.setArguments(args);

        return fragment;
    }

    private GalleryImageView mEditImage;
    private View mButtonDone;
    private View mButtonCancel;
    private View mBtnUndo;
    private View mBtnRedo;
    private View mBtnPalette;
    private View mProgress;
    private View mPalette;
    private FrameLayout mTutorial;

    private ConstraintLayout mContent;
    private View mNavBarBg;

    private boolean mProgressVisible = false;

    private Transformations transformations;
    private Disposable working;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null)
            transformations = (Transformations) args.getSerializable(Argus.TRANSFORMATIONS);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContent = (ConstraintLayout) inflater.inflate(R.layout.argus_fmt_document_draw, container, false);
        mEditImage = mContent.findViewById(R.id.editedImage);
        mProgress = mContent.findViewById(R.id.progress);
        mButtonDone = mContent.findViewById(R.id.btn_done);
        mButtonCancel = mContent.findViewById(R.id.btn_cancel);
        mBtnUndo = mContent.findViewById(R.id.btn_undo);
        mBtnRedo = mContent.findViewById(R.id.btn_redo);
        mNavBarBg = mContent.findViewById(R.id.navBarBg);
        mBtnPalette = mContent.findViewById(R.id.btn_palette);
        mPalette = mContent.findViewById(R.id.paletteView);
        mTutorial = mContent.findViewById(R.id.tutorial);

        mContent.setOnApplyWindowInsetsListener((v, insets) -> {
            handleInsets(insets);
            return insets;
        });

        bindImageEdit();
        bindDoneButton();
        bindUndoButton();
        bindPaletteButton();
        bindRedoButton();
        bindCancelButton();
        bindProgress();
        bindColors();
        bindTutorial();

        handleInsets(callback.getInsets());

        return mContent;
    }

    private void handleInsets(WindowInsets windowInsets) {
        if (windowInsets != null) {
            Insets insets = WindowInsetsCompat.toWindowInsetsCompat(windowInsets)
                .getInsets(WindowInsetsCompat.Type.systemBars());

            mContent.setPadding (
                0, Math.max(mContent.getPaddingTop(), insets.top),
                0, 0
            );

            ViewGroup.LayoutParams params = mNavBarBg.getLayoutParams();
            params.height = Math.max(params.height, insets.bottom);

            Insets gestures = WindowInsetsCompat.toWindowInsetsCompat(windowInsets)
                .getInsets(WindowInsetsCompat.Type.systemGestures());
            ViewGroup.MarginLayoutParams margins =
                (ViewGroup.MarginLayoutParams) mEditImage.getLayoutParams();
            margins.leftMargin = gestures.left;
            margins.rightMargin = gestures.right;

            mContent.requestLayout();
            mNavBarBg.requestLayout();

            mEditImage.requestLayout();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Activity activity = getActivity();
        if (activity != null)
            activity.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            );
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onBackPressedHandled() {
        return mProgressVisible || mTutorial.getVisibility() == View.VISIBLE;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void bindColors() {
        final PaletteColorView[] colors = {
                mContent.findViewById(R.id.paletteColor1),
                mContent.findViewById(R.id.paletteColor2),
                mContent.findViewById(R.id.paletteColor3),
                mContent.findViewById(R.id.paletteColor4),
                mContent.findViewById(R.id.paletteColor5),
                mContent.findViewById(R.id.paletteColor6),
                mContent.findViewById(R.id.paletteColor7),
                mContent.findViewById(R.id.paletteColor8),
                mContent.findViewById(R.id.paletteColor9),
                mContent.findViewById(R.id.paletteColor10)
        };

        final View.OnClickListener onColorClick = v -> {
            for (PaletteColorView color: colors) {
                color.setChecked(color.getId() == v.getId());
                if (color.isChecked())
                    mEditImage.setCorrectionColor(color.getColor());
            }
            showPalette(false);
        };

        for (PaletteColorView color: colors) {
            color.setOnClickListener(onColorClick);
            color.setChecked(mEditImage.getCorrectionColor() == color.getColor());
        }

        mPalette.setOnTouchListener((v, event) -> true);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void bindImageEdit() {
        mEditImage.setDrawingEnabled(true);
        mEditImage.setTransformations(transformations);
        mEditImage.setImage(ImageSource.uri(transformations.getUri()));
        mEditImage.setOnTouchListener((v, event) -> {
            showPalette(false);
            return false;
        });
    }

    private void bindPaletteButton() {
        mBtnPalette.setOnClickListener(v -> showPalette(mPalette.getVisibility() != View.VISIBLE));
    }

    private void bindUndoButton() {
        mBtnUndo.setOnClickListener(v -> {
            showPalette(false);
            mEditImage.undo();
        });
        mEditImage.getUndoPossibility().observe(getViewLifecycleOwner(), possible -> {
            mBtnUndo.setAlpha(possible ? 1f : 0.35f);
            mBtnUndo.setClickable(possible);
        });
    }

    private void bindRedoButton() {
        mBtnRedo.setOnClickListener(v -> {
            showPalette(false);
            mEditImage.redo();
        });
        mEditImage.getRedoPossibility().observe(getViewLifecycleOwner(), possible -> {
            mBtnRedo.setAlpha(possible ? 1f : 0.35f);
            mBtnRedo.setClickable(possible);
        });
    }

    private void bindDoneButton() {
        mButtonDone.setOnClickListener(v -> {
            if (working != null && !working.isDisposed())
                return;
            showPalette(false);
            Fragment target = getTargetFragment();
            int requestCode = getTargetRequestCode();

            transformations.setCorrections(mEditImage.getAppliedCorrections());
            if (target != null) {
                FragmentManager manager = getFragmentManager();
                if (manager != null)
                    manager.popBackStack();
                target.onActivityResult(
                        requestCode,
                        Activity.RESULT_OK,
                        new Intent().putExtra(Argus.TRANSFORMATIONS, transformations)
                );
            } else {
                callback.returnResultTransformations(transformations);
                /*if (transformations.hasChanges()) {
                    mProgressVisible = true;
                    bindProgress();

                    final ContentResolver resolver = getActivity() != null ? getActivity().getContentResolver() : null;
                    final Uri savePath = callback.getSaveUri();

                    working = Single.fromCallable(() -> {
                        if (resolver != null) {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(resolver, transformations.getUri());
                            Bitmap save = ImageProcessor.applyTransformations(bitmap, transformations);

                            if (!bitmap.isRecycled())
                                bitmap.recycle();

                            Uri uri = FileUtils.saveBitmap(save, savePath);
                            save.recycle();

                            return uri;
                        }
                        return null;
                    })
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    uri -> {
                                        mProgressVisible = false;
                                        bindProgress();
                                        if (uri != null) {
                                            ArrayList<Uri> uris = new ArrayList<>();
                                            uris.add(uri);
                                            callback.returnResult(uris);
                                        }
                                    },
                                    error -> {
                                        error.printStackTrace();
                                        mProgressVisible = false;
                                        bindProgress();
                                    });
                } else {
                    Activity activity = getActivity();
                    if (activity != null)
                        activity.onBackPressed();
                }*/
            }
        });
    }

    private void bindCancelButton() {
        mButtonCancel.setOnClickListener(v -> {
            if (getActivity() != null)
                getActivity().onBackPressed();
        });
    }

    private void bindProgress() {
        if (!mProgress.hasOnClickListeners())
            mProgress.setOnClickListener(v -> {});
        mProgress.setVisibility(mProgressVisible ? View.VISIBLE : View.GONE);

        mButtonDone.setEnabled(!mProgressVisible);
        mButtonDone.setClickable(!mProgressVisible);

        mButtonCancel.setEnabled(!mProgressVisible);
        mButtonCancel.setClickable(!mProgressVisible);

        mBtnUndo.setEnabled(!mProgressVisible);
        mBtnUndo.setClickable(!mProgressVisible);

        mBtnRedo.setEnabled(!mProgressVisible);
        mBtnRedo.setClickable(!mProgressVisible);

        mBtnPalette.setEnabled(!mProgressVisible);
        mBtnPalette.setClickable(!mProgressVisible);
    }

    private void showPalette(boolean show) {
        if (show && mPalette.getVisibility() == View.GONE) {
            Transition transition = new Slide(Gravity.BOTTOM);
            transition.setDuration(250);
            transition.addTarget(mPalette);

            TransitionManager.beginDelayedTransition(mContent, transition);
            mPalette.setVisibility(View.VISIBLE);
        } else if (!show && mPalette.getVisibility() == View.VISIBLE) {
            Transition transition = new Slide(Gravity.BOTTOM);
            transition.setDuration(250);
            transition.addTarget(mPalette);

            TransitionManager.beginDelayedTransition(mContent, transition);
            mPalette.setVisibility(View.GONE);
        }
    }

    private void bindTutorial() {
        if (callback.isShowDrawTutorial()) {
            mTutorial.setOnClickListener(v -> {});
            mEditImage.post(() -> {
                ViewPager2 viewPager = mTutorial.findViewById(R.id.tutorialPager);
                TabLayout dots = mTutorial.findViewById(R.id.tutorialDots);
                DrawTutorialAdapter adapter = new DrawTutorialAdapter();
                adapter.setListener(() -> {
                    if (viewPager.getCurrentItem() < adapter.getItemCount() - 1)
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
                    else {
                        Transition transition = new Fade();
                        transition.setDuration(250);
                        transition.addTarget(mTutorial);
                        TransitionManager.beginDelayedTransition(mContent, transition);
                        mTutorial.setVisibility(View.GONE);
                        callback.setShowDrawTutorialNextTime(false);
                        adapter.restartAnimation(null);
                    }
                });

                viewPager.setAdapter(adapter);
                viewPager.setOffscreenPageLimit(3);
                viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
                    @Override
                    public void onPageSelected(int position) {
                        super.onPageSelected(position);
                        adapter.restartAnimation(position);
                    }
                });
                new TabLayoutMediator(dots, viewPager, (tab, position) -> {}).attach();
                viewPager.post(() -> {
                    int height = adapter.measureHeight(mTutorial);
                    if (height > 0) {
                        viewPager.getLayoutParams().height = height;
                        viewPager.requestLayout();
                    }
                });

                Transition transition = new Fade();
                transition.setDuration(250);
                transition.addTarget(mTutorial);
                TransitionManager.beginDelayedTransition(mContent, transition);
                mTutorial.setVisibility(View.VISIBLE);
            });
        }
    }
}
